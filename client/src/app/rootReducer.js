import _ from 'lodash';
import { combineReducers } from 'redux-immutable';

export default function(modules){

  let reducers = combineReducers(
    _.assign({},
            modules.admin.reducers,
            modules.auth.reducers,
            modules.core.reducers,
            modules.dashboard.reducers,
            modules.editshortcode.reducers,
            modules.addshortcode.reducers,
            modules.operator.reducers,
            modules.profile.reducers,
            modules.shortcode.reducers,
            modules.user.reducers
    )
  );
  return reducers;
}
