import Checkit from 'checkit';

export default class {

    constructor( payload ) {
        this.payload = payload;
    }

    execute() {

        let rules = new Checkit( {
            location: ['minLength:3', 'maxLength:64' ],
            firstName: [ 'required', 'minLength:3', 'maxLength:64' ],
            lastName: [ 'required', 'minLength:3', 'maxLength:64' ]
        } );

        return rules.run( this.payload );
    }
}
