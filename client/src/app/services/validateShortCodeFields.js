import Checkit from 'checkit';

export default class {

    constructor( payload ) {
        this.payload = payload;
    }

    execute() {
        let rules = new Checkit( {
            name: [ 'required', 'minLength:1', 'maxLength:10' ],
            description: [ 'minLength:3', 'maxLength:164' ],
            deviceNumber: [ 'minLength:3', 'maxLength:30' ],
            email: [ 'required', 'minLength:3', 'maxLength:64' ]
        } );

        return rules.run( this.payload );
    }
}