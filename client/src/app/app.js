/* global require */
/* eslint global-require: 0*/
require('assets/stylus/main');
require('jquery-sparkline/jquery.sparkline.min.js');
require('easy-pie-chart/dist/jquery.easypiechart.min.js');

import React from 'react';
import ReactDOM from 'react-dom';
import {IntlProvider} from 'react-intl';
import firebase from 'firebase';
import Rest from './utils/rest';
import FireBaseRest from './utils/firebaseRest';
import configureStore from './configureStore';

import AuthModule from './parts/auth/authModule';
import CoreModule from './parts/core/coreModule';
import ProfileModule from './parts/profile/profileModule';
import DashboardModule from './parts/dashboard/dashboardModule';
import ShortCodeModule from './parts/shortcode/shortCodeModule';
import OperatorModule from './parts/operator/operatorModule';
import EditShortCodeModule from './parts/editshortcode/editShortCodeModule';
import AddShortCodeModule from './parts/addshortcode/addShortCodeModule';
import UserModule from './parts/user/userModule';
import NotFoundModule from './parts/notfound/notFoundModule';
import AdminModule from './parts/admin/adminModule';
import Debug from 'debug';

import rootView from './redux/rootView';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

Debug.enable("*,-engine*,-sockjs-client*,-socket*");

let debug = new Debug("app");

async function loadToken(store, parts){
  let token = localStorage.getItem("JWT");
  if(token){
    store.dispatch(parts.auth.actions.setToken(token))
  }
  return token;
}

function App() {
    debug("App begins");

    firebase.initializeApp({
        apiKey: "AIzaSyA6nfru2V9ILabQw8c3GQuB3SVzyR9pZtw",
        authDomain: "shaqodoonlivemonitor.firebaseapp.com",
        databaseURL: "https://shaqodoonlivemonitor.firebaseio.com",
        storageBucket: "shaqodoonlivemonitor.appspot.com"
    });

    const rest = Rest();
    const fireBaseRest = FireBaseRest();
    let auth = AuthModule(rest);
    const parts = {
        auth,
        core: CoreModule(),
        dashboard:DashboardModule(rest, fireBaseRest),
        notfound:NotFoundModule(rest),
        shortcode:ShortCodeModule(rest, fireBaseRest),
        operator:OperatorModule(rest),
        addshortcode:AddShortCodeModule(rest),
        editshortcode:EditShortCodeModule(rest),
        user:UserModule(rest),
        profile: ProfileModule(rest),
        admin: AdminModule(rest)
    };

    const store = configureStore(parts);

  let jwtSelector = (store) => {
    return () => store.getState().get('auth').get('token')
  };

  rest.setJwtSelector(jwtSelector(store));
  return {
        start () {
            debug("start");
            return render()
            .then(authFromLocalStorage);
        }
  };
  function authFromLocalStorage(){
    loadToken(store, parts)
  }
  function render() {
      debug("render");
      let mountEl = document.getElementById('application');
      ReactDOM.render(
              <IntlProvider locale='en'>
                  {rootView(store, parts)}
              </IntlProvider>
              , mountEl);
  }
}

let app = App();
app.start();