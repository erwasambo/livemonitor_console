import React from 'react';
import {Route, IndexRoute} from 'react-router';

import MainLanding from 'parts/core/views/mainLanding';

export default (store, parts) => (
    <Route component={parts.auth.containers.app()} name="home" path="/">
        <IndexRoute component={MainLanding}/>
        {parts.auth.routes}

        <Route component={parts.auth.containers.authentication()}>
            {parts.admin.routes}
            <Route path="/dashboard">
                <IndexRoute component={parts.dashboard.containers.dashboard()}/>
                <Route name="account" path="my">
                    {parts.profile.routes}
                    {parts.operator.routes}
                    {parts.user.routes}
                    {parts.shortcode.routes}
                    {parts.editshortcode.routes}
                </Route>
            </Route>
        </Route>
        {parts.addshortcode.routes}
        <Route path="*" component={parts.notfound.containers.notfound()} />
    </Route>
);