import Axios from 'axios';
import Qs from 'qs';
import Debug from 'debug';

let debug = new Debug("rest");

export default function(options = {}){

    let _jwtSelector;

    function ajax( url, method, data, params) {
        debug("ajax url: %s, method: %s, options %s, params: %s", url, method, JSON.stringify(data), JSON.stringify(params));
        if(_jwtSelector){
            let jwt = _jwtSelector();
            if(jwt){
                console.log("adding jwt: ", jwt);
                headers.Authorization = 'Bearer ' + jwt;
            }
        }

        return Axios({
                method: method,
                url: "http://livemonitorconsole.com/api/v1/"+ url,
                params: params,
                data: data,
                withCredentials: true,
                headers: {'Content-Type': 'application/json'},
                timeout: 9e3,
                paramsSerializer: function(params) {
                    return Qs.stringify(params, {arrayFormat: 'brackets'});
                }
            }).then(res => {
                return res.data;
    }).catch(error => {
            console.log("ajax error: ", error);
        throw error
    });
    }

    return {
        setJwtSelector(jwtSelector){
            _jwtSelector = jwtSelector
        },
        get( url, options = {} ) {
            return ajax(url, 'GET', null,options);
        },
        post( url, options = {} ) {
            return ajax(url, 'POST',options,options);
        },
        put( url, options = {} ) {
            return ajax(url, 'PUT',options,options);
        },
        del( url, options = {} ) {
            return ajax(url, 'DELETE',options,options);
        },
        patch( url, options = {} ) {
            return ajax(url, 'PATCH', options,options);
        }
    }
}