import _ from 'lodash';

/* global process */
let env = process.env;

let config = {

    general: {
        title: 'Shaqodoon LiveMonitor',
        description: 'This app checks the operational availability of shaqodoon shortcodes',
        apiUrl: 'api/v1/',
        firebaseUrl: 'https://shaqodoonlivemonitor.firebaseio.com/',
        analytics: {
            google: ""
        },

        firebaseConfig : {
            apiKey: "AIzaSyA6nfru2V9ILabQw8c3GQuB3SVzyR9pZtw",
            authDomain: "shaqodoonlivemonitor.firebaseapp.com",
            databaseURL: "https://shaqodoonlivemonitor.firebaseio.com",
            storageBucket: "shaqodoonlivemonitor.appspot.com"
        }
},

    development: {
        env: "development"
    },

    production: {
        env: "production"

    }
};

export default _.extend( {}, config.general, config[ env.NODE_ENV ] );