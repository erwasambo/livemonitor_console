import _ from 'lodash';
import React from 'react';
import AppBar from 'material-ui/lib/app-bar';
import LeftNav from 'material-ui/lib/left-nav';
import MenuItem from 'material-ui/lib/menus/menu-item';


import IconButton from 'material-ui/lib/icon-button';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';


import config from 'config';
import Debug from 'debug';
let debug = new Debug("component:navbar");

function navLinks(authenticated) {
    if (authenticated) {
        return [
            {
                route: '/',
                text: 'HOME'
            },
            {
                route: '/admin',
                text: 'ADMIN'
            }, {
                route: '/dashboard',
                text: 'DASHBOARD'
            }, {
                route: '/dashboard/my/profile',
                text: 'PROFILE'
            }, {
                route: '/logout',
                text: 'LOGOUT'
            }
        ];
    } else {
        return [
            {
                route: '/',
                text: 'HOME'
            },
            {
                route: '/login',
                text: 'LOGIN'
            }, {
                route: '/register',
                text: 'REGISTER'
            }
        ];
    }
}

function shortCutNavLinks(authenticated) {
    if (authenticated) {
        return [
            {
                route: '/dashboard/my/profile',
                text: 'PROFILE'
            }, {
                route: '/logout',
                text: 'LOGOUT'
            }
        ];
    } else {
        return [
            {
                route: '/login',
                text: 'LOGIN'
            }, {
                route: '/register',
                text: 'REGISTER'
            }
        ];
    }
}

export default React.createClass({
    contextTypes: {
        muiTheme: React.PropTypes.object,
        router: React.PropTypes.object.isRequired
    },
    propTypes: {
        authenticated: React.PropTypes.bool.isRequired
    },
    getInitialState () {
        return {open: false, muiTheme: this.context.muiTheme};
    },
    componentWillMount () {
        let newMuiTheme = this.state.muiTheme;
        this.setState({muiTheme: newMuiTheme});
    },
    toggleNav () {
        debug('toggleNav');
        this.setState({
            open: !this.state.open
        });
    },
    handleNavChange (menuItem) {
        debug('handleNavChange ', menuItem.route);
        this.context.router.push(menuItem.route);
        this.setState({open: false});
    },
    handleTouchTap() {
        debug('onTouchTap triggered on the title component');
        this.context.router.push("/dashboard");
    },
    renderShortCutMenuItem () {
        return _.map(shortCutNavLinks(this.props.authenticated), (menu, key) => {
            return (
                <MenuItem key={key} onTouchTap={_.partial(this.handleNavChange, menu)}>
                    {menu.text}
                </MenuItem>
            );
        });
    },
    renderMenuItem () {
        //debug('handleNavChange ', this.props);
        return _.map(navLinks(this.props.authenticated), (menu, key) => {
            return (
                <MenuItem key={key} onTouchTap={_.partial(this.handleNavChange, menu)}>
                    {menu.text}
                </MenuItem>
            );
        });
    },
    render () {
        return (
            <div >
                <AppBar
                    style={{
                    }}
                    id='app-bar' title={config.title}
                    onTitleTouchTap={this.handleTouchTap}
                    onLeftIconButtonTouchTap={this.toggleNav}
                    iconElementRight={
      <IconMenu
        iconButtonElement={
          <IconButton><MoreVertIcon/></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      >
                    {this.renderShortCutMenuItem()}
      </IconMenu>
    }
                />
                <LeftNav id='left-nav' docked={false} open={this.state.open}
                         onRequestChange={open => this.setState({open})}>
                    {this.renderMenuItem()}
                </LeftNav>
            </div>
        );
    }
});
