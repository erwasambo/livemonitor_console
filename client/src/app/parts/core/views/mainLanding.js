import React from 'react';
import DocMeta from 'react-doc-meta';
import RaisedButton from 'material-ui/lib/raised-button';
import FontIcon from 'material-ui/lib/font-icon';
import DocTitle from 'components/docTitle';
import config from 'config';
import Debug from 'debug';
let debug = new Debug("views:main");

import Card from 'material-ui/lib/card/card';
import CardMedia from 'material-ui/lib/card/card-media';
import CardTitle from 'material-ui/lib/card/card-title';
import CardText from 'material-ui/lib/card/card-text';


let CardIcon = React.createClass({
    renderIcon(props){
        return (
            <CardMedia>
                <FontIcon
                    className={props.icon}
                    style={{
                        fontSize:'64'
                    }}/>
            </CardMedia>
        );
    },
    renderImg(props){
        return (
            <div>
                <img src={props.img} alt={props.title} height={props.height} width={props.width}/>
            </div>
        );
    },
    render(){
        let {props} = this;
        return (
            <Card
                className='flex-items'
                style={{
                    }}>
                <CardTitle
                    title={props.title}
                />
                <CardText>
                    {props.text}
                </CardText>
                {props.icon && this.renderIcon(props)}
                {props.img && this.renderImg(props)}
            </Card>
        );
    }
});

export default React.createClass({
    render() {
        debug("render ");

        return (
            <section id="main">
                <section>
                    <div className="container">
                        <DocTitle
                            title="Home"
                        />
                        <DocMeta tags={ this.tags() }/>
                        <div className="header-container">
                            <div className="header clearfix">
                                <div className="text-vertical-center col-md-12 text-center">
                                    <h1>Shaqodoon</h1>
                                    <h2>Livemonitor</h2>
                                    <h3>Monitor the operational availability of your short codes!</h3>
                                    <br />

                                    <RaisedButton
                                        label="Get Started"
                                        linkButton={true}
                                        href="/login"
                                        icon={<FontIcon className="icon-shield-circled-alt2"/>}
                                    />
                                </div>
                            </div>
                        </div>

                        <section id="start">
                            <div className="row">
                                <div className="col-md-12 text-center">
                                    <h2>
                                        <strong>Features</strong>
                                    </h2>
                                    <CardIcon
                                        icon='icon-stackexchange'
                                        title='Short Code Testing'
                                        text='With the help of the mobile tests devices, this application works hand in hand to ensure nothing is missed'/>
                                    <CardIcon
                                        icon='icon-user'
                                        title='Live Updates and Alerts'
                                        text='View and get real time updates on the any incidences reported for the Short Codes'/>

                                    <CardIcon
                                        icon='icon-database'
                                        title='Robust Reporting'
                                        text='Get human friendly reports that are even exportable to Excel!'/>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </section>);
    },

    tags() {
        let description = config.description;

        return [
            {name: 'description', content: description},
            {name: 'twitter:card', content: description},
            {name: 'twitter:title', content: description},
            {property: 'og:title', content: description}
        ];
    }
});