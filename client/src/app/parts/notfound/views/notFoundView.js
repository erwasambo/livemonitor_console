import React from 'react';
import DocTitle from 'components/docTitle';

export default React.createClass({
    render () {
        return (
            <div className="four-zero">
                <DocTitle title="Not Found! | Shaqodoon"/>
                <div className="fz-block">
                    <h2>Not Found!</h2>
                    <small>Nah.. it's 404</small>
                    <div className="fzb-links">
                        <a href="index.html"><i className="zmdi zmdi-arrow-back"/></a>
                        <a href="index.html"><i className="zmdi zmdi-home"/></a>
                    </div>
                </div>
            </div>);
    }
});