import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import NotFoundView from './views/notFoundView';

function Actions() {
    return {}
}

function Reducers() {
    return {}
}

function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        notfound(){
            const mapStateToProps = (state) => ({});
            return connect(mapStateToProps, mapDispatchToProps)(NotFoundView);
        }
    }
}

function Routes(containers) {
    return (
        <Route component={containers.notfound()} path="notfound"/>
    )
}

export default function (rest) {
    let actions = Actions();
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(),
        containers,
        routes
    }
}
