import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import LaddaButton from 'react-ladda';
import Dialog from 'material-ui/lib/dialog';
import CircularProgress from 'material-ui/lib/circular-progress';
import FlatButton from 'material-ui/lib/flat-button';
import ValidateShortCodeFields from '../../../services/validateShortCodeFields';
import Debug from 'debug';
let debug = new Debug("components:editShortCodeForm");

export default React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    propTypes: {
        editshortcode: React.PropTypes.object
    },

    getDefaultProps(){
        return {
            loading: false,
            editshortcode: {}
        }
    },
    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            open: false,
            language: 'US',
            updating: false,
            loading: false,
            count: 0,
            operators: {},
            shortCodeData: [],
            operatorData: [],
            errors: {},
            completed: 0
        }
    },

    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.editshortcodeget.data || {});
        this.setState(nextProps.operatorsget.data || {});
    },

    handleDialogOpen(evt) {
        evt.preventDefault();
        this.setState({open: true});
    },

    handleClose(){
        this.setState({open: false});
        setTimeout(function () {
            this.context.router.push('/admin');
        }, 3000);
    },

    handleDeleteClose(){
        this.setState({open: false});
        this.deleteShortCode();
    },

    renderUpdateError(){
        let {error} = this.props.editshortcodeupdate;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    renderDeleteError(){
        let {error} = this.props.editshortcodedelete;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    renderDialog(){
        if (error) {
            return (
                <Dialog
                    title="Delete Short Code"
                    actions={[
            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Delete"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleDeleteClose}
            />
        ]}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    Are you sure you want to delete Short Code {" "+state.name + " "}  of {" "+state.operator + " "}?
                </Dialog>
            )
        }
    },

    handleDescriptionChange: function (event) {
        this.setState({description: event.target.value});
    },

    handleDeviceNumberChange: function (event) {
        this.setState({deviceNumber: event.target.value});
    },
    handleOwnerEmailChange: function (event) {
        this.setState({email: event.target.value});
    },
    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;

        if (props.operatorsget.loading  ||  props.editshortcodeget.loading ) {
            return <CircularProgress size={2}/>
        }

        return (
            <div className="local-login-form">
                {_this.renderDialog}
                <form className="form-horizontal">
                    <div className="signup-options text-center form">
                        {_this.renderUpdateError}
                        {_this.renderDeleteError}
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="name"
                                        hintText="Name"
                                        floatingLabelText="Name"
                                        value={state.name}
                                        disabled={true}
                                        errorText={errors.name && errors.name[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    ref="description"
                                    hintText="Description"
                                    floatingLabelText="Description"
                                    value={state.description}
                                    onChange={_this.handleDescriptionChange}
                                    errorText={errors.description && errors.description[0]}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="deviceNumber"
                                        hintText="Device Number"
                                        floatingLabelText="Device Number"
                                        value={state.deviceNumber}
                                        onChange={_this.handleDeviceNumberChange}
                                        errorText={errors.deviceNumber && errors.deviceNumber[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    ref="id"
                                    hintText="Operator"
                                    floatingLabelText="Operator"
                                    value={state.operator}
                                    disabled={true}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="email"
                                        hintText="Owner Email"
                                        floatingLabelText="Owner Email"
                                        value={state.email}
                                        onChange={_this.handleOwnerEmailChange}
                                        errorText={errors.email && errors.email[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    ref="id"
                                    hintText="Short Code Id"
                                    floatingLabelText="Short Code Id"
                                    value={state.id}
                                    disabled={true}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <LaddaButton
                                        className='btn btn-lg btn-primary'
                                        id='btn-login'
                                        buttonColor='blue'
                                        loading={props.editshortcodeupdate.loading}
                                        progress={.5}
                                        buttonStyle="slide-up"
                                        onClick={_this.updateShortCode}>Update</LaddaButton>
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <LaddaButton
                                    className='btn btn-lg btn-primary'
                                    id='btn-login'
                                    buttonColor='red'
                                    loading={props.editshortcodedelete.loading}
                                    progress={.5}
                                    buttonStyle="slide-up"
                                    onClick={_this.deleteShortCode}>Delete</LaddaButton>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    },

    onLanguage(language){
        debug("onLanguage: ", language);
        this.setState({language: language});
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    },

    updateShortCode(evt) {
        evt.preventDefault();
        debug('updateShortCode ', this.state);
        this.setState({
            errors: {},
            updating: true
        });

        validateForm.call(this)
            .with(this)
            .then(save)
            .then(successNotification)
            .catch(Checkit.Error, (error) => this.setState({
                errors: error.toJSON()
            }))
            .catch(setErrors)
            .then(() => {
                this.setState({
                    updating: false
                });
            });

        function validateForm() {
            return new ValidateShortCodeFields({
                name: this.state.name,
                description: this.state.description,
                deviceNumber: this.state.deviceNumber,
                email: this.state.email
            }).execute();
        }

        function save() {
            return this.props.actions.updateShortCode(this.state);
        }

        function successNotification() {

        }

        function setErrors(e) {
            debug('setErrors: ', e);
        }
    },

    deleteShortCode() {
        debug('deleteShortCode ', this.state);
        this.setState({
            errors: {},
            deleting: true
        });

        deleteShortCd.call(this)
            .with(this)
            .then(successNotification)
            .catch(Checkit.Error, (error) => this.setState({
                errors: error.toJSON()
            }))
            .catch(setErrors)
            .then(() => {
                this.setState({
                    deleting: false
                });
            });

        function deleteShortCd() {
            return this.props.actions.deleteShortCode(this.state.id);
        }

        function successNotification() {
        }

        function setErrors(e) {
            debug('setErrors: ', e);
            if (error instanceof Checkit.Error) {
                this.setState({errors: error.toJSON()})
            }
        }
    }
});

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}