import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {connect} from 'react-redux';
import EditShortCodeView from './views/editShortCodeView';

function Resources(rest) {
    return {
        getShortCode(id) {
            return rest.get('shortcodes/' + id);
        },
        getOperators() {
        return rest.get('operators/');
        },
        updateShortCode(payload) {
            return rest.put('shortcodes/' + payload.id, payload);
        },
        deleteShortCode(id) {
            return rest.del('shortcodes/' + id);
        }
    }
}

function Actions(rest) {
    let editshortcode = Resources(rest);
    return {
        getOperators: createActionAsync('EDIT_OPERATORS_GET', editshortcode.getOperators),
        getShortCode: createActionAsync('EDIT_SHORT_CODE_GET', editshortcode.getShortCode),
        updateShortCode: createActionAsync('EDIT_SHORT_CODE_UPDATE', editshortcode.updateShortCode),
        deleteShortCode: createActionAsync('EDIT_SHORT_CODE_DELETE', editshortcode.deleteShortCode)
    }
}

function Reducers(actions) {
    return {
        operatorsget: createReducerAsync(actions.getOperators),
        editshortcodeget: createReducerAsync(actions.getShortCode),
        editshortcodeupdate: createReducerAsync(actions.updateShortCode),
        editshortcodedelete: createReducerAsync(actions.deleteShortCode)
    }
}

function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        editshortcode(){
            const mapStateToProps = (state) => ({
                operatorsget: state.get('operatorsget').toJS(),
                editshortcodeget: state.get('editshortcodeget').toJS(),
                editshortcodeupdate: state.get('editshortcodeupdate').toJS(),
                editshortcodedelete: state.get('editshortcodedelete').toJS()
            });
            return connect(mapStateToProps, mapDispatchToProps)(EditShortCodeView);
        }
    }
}

function Routes(containers) {
    return (<Route component={containers.editshortcode()} path="/editShortCode/:shortCodeId" />)
}

export default function (rest) {
    let actions = Actions(rest);
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}