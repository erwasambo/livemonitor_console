import React from 'react';
import DocTitle from 'components/docTitle';
import EditShortCodeForm from '../components/editShortCodeForm';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("components:editShortCodeView");

export default React.createClass({

    propTypes: {},

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
        }
    },
    componentDidMount () {
        let {props} = this;
        props.actions.getOperators();
        props.actions.getShortCode(props.params.shortCodeId);
    },

    render () {
        let {props} = this;
        let _this = this;
        return (
            <Paper className="text-center view">
                <DocTitle title="Edit Short Code |Livemonitor"/>
                <h2>Edit Short Code</h2>
                {props.editshortcodedelete.data.success && _this.renderShortCodeDeleteComplete()}
                {props.editshortcodeupdate.data.success && _this.renderShortCodeUpdateComplete()}
                {(!props.editshortcodeupdate.data.success && !props.editshortcodedelete.data.success) && _this.renderShortCodeForm()}
            </Paper>
        );
    },
    renderShortCodeUpdateComplete(){
        return (
            <div className="local-login-form">
                <div className="alert alert-info text-center" role="alert">
                    The ShortCode you changed has been successfully updated.
                </div>
            </div>
        );
    },
    renderShortCodeDeleteComplete(){
        return (
            <div className="local-login-form">
                <div className="alert alert-info text-center" role="alert">
                    The ShortCode you changed has been successfully deleted.
                </div>
            </div>
        );
    },
    renderShortCodeForm(){
        let {props} = this;
        return (<EditShortCodeForm {...props}/>);
    }
})