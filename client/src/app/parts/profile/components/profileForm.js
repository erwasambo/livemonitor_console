import _ from 'lodash';
import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import LaddaButton from 'react-ladda';
import CircularProgress from 'material-ui/lib/circular-progress';
import ValidateProfileForm from '../../../services/validateProfileForm';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("components:profileForm");

export default React.createClass({

    propTypes: {
        profile: React.PropTypes.object
    },

    getDefaultProps(){
        return {
            loading: false,
            profile: {}
        }
    },

    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.profile.data || {});
    },

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            language: 'US',
            updating: false,
            errors: {},
            completed: 0
        }
    },

    handleFirstNameChange: function (event) {
        this.setState({firstName: event.target.value});
    },

    handleLastNameChange: function (event) {
        this.setState({lastName: event.target.value});
    },

    handleLocationChange: function (event) {
        this.setState({location: event.target.value});
    },

    renderUpdateError(){
        let {error} = this.props.userUpdate;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    },

    updateUser(evt) {
        evt.preventDefault();
        //debug('updateProfile ', this.state);
        this.setState({
            errors: {},
            updating: true
        });

        validateForm.call(this)
            .with(this)
            .then(save)
            .then(successNotification)
            .catch(Checkit.Error, (error) => this.setState({
                errors: error.toJSON()
            }))
            .catch(setErrors)
            .then(() => {
                this.setState({
                    updating: false
                });
            });

        function validateForm() {
            return new ValidateProfileForm({
                username: this.state.username,
                email: this.state.email,
                location: this.state.location,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            })
                .execute();
        }

        function save() {
            return this.props.actions.updateUser(this.state);
        }

        function successNotification() {
        }

        function setErrors(e) {
            debug('setErrors: ', e);
        }
    },

    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;

        if (props.profile.loading) {
            return (<CircularProgress size={2}/>);
        }

        return (
                <form
                    className="form-horizontal"
                    onSubmit={ (e) => e.preventDefault() }>
                    <div className="signup-options text-center form">
                    {_this.renderUpdateError()}
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        id='firstName'
                                        floatingLabelText="First Name"
                                        value={state.firstName}
                                        onChange={_this.handleFirstNameChange}
                                        errorText={errors.firstName && errors.firstName[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    id='lastName'
                                    floatingLabelText="Last Name"
                                    value={state.lastName}
                                    onChange={_this.handleLastNameChange}
                                    errorText={errors.lastName && errors.lastName[0]}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        id='location'
                                        floatingLabelText="Location"
                                        value={state.location}
                                        onChange={_this.handleLocationChange}
                                        errorText={errors.location && errors.location[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    id='email'
                                    value={state.email}
                                    disabled={true}
                                    floatingLabelText="Email"
                                    errorText={errors.email && errors.email[0]}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <TextField
                                    id='username'
                                    floatingLabelText="Username"
                                    value={state.username}
                                    disabled={true}
                                    errorText={errors.username && errors.username[0]}
                                />
                            </div>
                            <div className="form-group col-md-6">
                            </div>
                        </div>

                    <div className='btn-signup'>
                        <LaddaButton
                            className='btn btn-lg btn-primary'
                            id='btn-login'
                            buttonColor='blue'
                            loading={props.userUpdate.loading}
                            progress={.5}
                            buttonStyle="slide-up"
                            onClick={_this.updateUser}>Update Profile</LaddaButton>
                    </div>
                    <br/>
                    </div>
                </form>
        );
    }
});

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}