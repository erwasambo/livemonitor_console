import React from 'react';
import DocTitle from 'components/docTitle';
import ProfileForm from '../components/profileForm';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("components:userView");

export default React.createClass({

    propTypes: {},

    componentDidMount () {
        this.props.actions.get()
    },

    renderUserUpdateComplete(){
        return (
            <div className="alert alert-info text-center" role="alert">
                Your profile has been successfully updated.
            </div>
        );
    },
    renderProfileForm(){
        let {props} = this;
        return (<ProfileForm {...props}/>
        );
    },
    render () {
        debug('render ', this.state);
        let {props} = this;
        let _this = this;
        return (
            <div id="profile">
                <Paper className='profile-view view'>
                        <DocTitle title="My Profile"/>
                        <h3>My Profile</h3>
                        {props.userUpdate.data.success && _this.renderUserUpdateComplete()}
                        {!props.userUpdate.data.success && _this.renderProfileForm()}
                </Paper>
            </div>
        );
    }
});