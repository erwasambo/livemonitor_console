import React from 'react';
import TodayShortCodeDataDashWidget from '../components/todayShortCodeDataDashWidget';
import ThisWeekShortCodeDataDashWidget from '../components/thisWeekShortCodeDataDashWidget';
import ThisMonthShortCodeDataDashWidget from '../components/thisMonthShortCodeDataDashWidget';
import YesterdayShortCodeDataDashWidget from '../components/yesterdayShortCodeDataDashWidget';
import LastWeekShortCodeDataDashWidget from '../components/lastWeekShortCodeDataDashWidget';
import LastMonthShortCodeDataDashWidget from '../components/lastMonthShortCodeDataDashWidget';
import CircularProgress from 'material-ui/lib/circular-progress';
import DocTitle from '../../../components/docTitle';
import ReactPivot from 'react-pivot';
import Debug from 'debug';
let debug = new Debug("components:shortcodeForm");

export default React.createClass({

    propTypes: {
        shortcode: React.PropTypes.object
    },

    getDefaultProps(){
        return {
            shortCodeLoading: false,
            shortcode: {}
        }
    },

    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.shortcode.data || {});
        this.setState(nextProps.shortCodePivotData.data || {});
    },

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            open: false,
            language: 'US',
            updating: false,
            deleting: false,
            errors: {},
            pivotData: [],
            completed: 0,
            status: "Loading",
            timestampInMillis: 0,
            updateDate: new Date(0),
            lastUpdated: "Out of Date",
            countClassNames: "chart stats-line",
            miniChartsClassNames: "mini-charts-item bgm-bluegray",
            isPivotDataLoading: true
        }
    },

    hideLoading() {
        this.setState({isPivotDataLoading: false});
    },

    showLoading() {
        this.setState({isPivotDataLoading: true});
    },

    renderPivotTable(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={1} />
            );
        }else{
            return (<ReactPivot rows={state.pivotData}
                        dimensions={[
            // "value" can be the key of what you want to group on
            // "value" can also be function that returns what you want to group on
            {
                title: 'Short Code',
                value: function(row) { return row.shortCodeName },
                template: function(value) {
                    return value
                }
            },{
                title: 'Operator',
                value: function(row) { return row.operatorName },
                template: function(value) {
                    return value
                }
            },{
                title: 'Date',
                value: function(row) { return row.date },
                template: function(value) {
                    return value
                }
            }
        ]}
                        reduce={function(row, memo) {
            // the memo object starts as {} for each group, build it up
            memo.count = (memo.count || 0) + 1;
            memo.successfulTotal = (memo.successfulTotal || 0) + parseFloat(row.successfulAmount);
            memo.delayedTotal = (memo.delayedTotal || 0) + parseFloat(row.delayedAmount);
            memo.failedTotal = (memo.failedTotal || 0) + parseFloat(row.failedAmount);
            // be sure to return it when you're done for the next pass
            return memo
        }}
                        calculations={ [
            // "value" can be the key of the "memo" object from reduce
            // "template" changes the display of the value, but not sorting behavior
            {
                title: 'Successful', value: 'successfulTotal',
                template:  function (val, row) {return val.toFixed(0)}
            },
            {
                title: 'Delayed', value: 'delayedTotal',
                template: function (val, row) {return val.toFixed(0)}
            },
            {
                title: 'Failed', value: 'failedTotal',
                template: function (val, row) {return val.toFixed(0)}
            },
            {
                title: 'Successful %',
                // "value" can also be a function
                value: function (memo) {return ((memo.successfulTotal / memo.count) * 100)},
                template: function (val, row) {return val.toFixed(0) + "%"}
            },
            {
                title: 'Delayed %',
                // "value" can also be a function
                value: function (memo) {return ((memo.delayedTotal / memo.count) * 100)},
                template: function (val, row) {return val.toFixed(0) + "%"}
            },
            {
                title: 'Failed %',
                // "value" can also be a function
                value: function (memo) {return ((memo.failedTotal / memo.count) * 100)},
                template: function (val, row) {return val.toFixed(0) + "%"}
            },
            {
                title: 'Total Sent',
                // "value" can also be a function
                value: function (memo) {return (memo.successfulTotal + memo.delayedTotal + memo.failedTotal)},
                template: function (val, row) {return val.toFixed(0)},
                // you can also give a column a custom class (e.g. right align for numbers)
                className: 'alignRight'
            }
        ]}
                        activeDimensions={['Short Code']} />);
        }
    },

    renderTodayShortCodeDataDashWidget(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<TodayShortCodeDataDashWidget loading={props.shortCodePivotData.loading} pivotData={state.pivotData}/>);
        }
    },
    renderThisWeekShortCodeDataDashWidget(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<ThisWeekShortCodeDataDashWidget loading={props.shortCodePivotData.loading} pivotData={state.pivotData}/>);
        }
    },
    renderThisMonthShortCodeDataDashWidget(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<ThisMonthShortCodeDataDashWidget loading={props.shortCodePivotData.loading} pivotData={state.pivotData}/>);
        }
    },

    renderYesterdayShortCodeDataDashWidget(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<YesterdayShortCodeDataDashWidget loading={props.shortCodePivotData.loading} pivotData={state.pivotData}/>);
        }
    },
    renderLastWeekShortCodeDataDashWidget(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<LastWeekShortCodeDataDashWidget loading={props.shortCodePivotData.loading} pivotData={state.pivotData}/>);
        }
    },

    renderLastMonthShortCodeDataDashWidget(){
        let {state, props} = this;
        if (props.shortCodePivotData.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<LastMonthShortCodeDataDashWidget loading={props.shortCodePivotData.loading} pivotData={state.pivotData}/>);
        }
    },

    renderShortCodeDetailsHeader(){
        let {state, props} = this;
        if (props.shortcode.loading) {
            return (
                <CircularProgress size={0.5} />
            );
        }else{
            return(<h2>{state.operatorName} {state.name} Shortcode Data
                <small>This is gives you an overview of happens at Shortcode {state.name}
                    of {state.operatorName} Now, Today, This Week and The whole Month
                </small>
                <DocTitle title={state.name+" Short Code Details|Livemonitor"}/>
                <small>Description: {state.description} Device Number: {state.deviceNumber} Owner
                    Email: {state.email}
                </small>
            </h2>);
        }
    },

    render () {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let _this = this;
        debug("this.props.shortcode.id ", state.id);

        return (
            <section id="main">
                <section id="content">
                    <div className="container">
                        <div className="block-header">
                            {_this.renderShortCodeDetailsHeader()}
                        </div>
                        <div className="dash-widgets">
                            <div className="row">
                                {/*_this.renderTodayShortCodeDataDashWidget()*/}
                                {_this.renderYesterdayShortCodeDataDashWidget()}
                                {_this.renderThisWeekShortCodeDataDashWidget()}
                                {_this.renderThisMonthShortCodeDataDashWidget()}
                            </div>
                        </div>
                        <div className="dash-widgets">
                            <div className="row">
                                {_this.renderLastWeekShortCodeDataDashWidget()}
                                {_this.renderLastMonthShortCodeDataDashWidget()}
                            </div>
                        </div>

                        <div className="block-header">
                            <h2>Pivot Table Data
                                <small>This is gives you all the data that you need for decision making. Just filter
                                    what you need then export it as an Excel Sheet!
                                </small>
                            </h2>
                        </div>
                        <div className="dash-widgets">
                            <div className="row">
                                {_this.renderPivotTable()}
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        );
    }
})