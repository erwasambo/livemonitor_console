import React from 'react';
import $ from 'jquery';
import moment from 'moment';
import Debug from 'debug';
let debug = new Debug("components:dashboardForm");

export default React.createClass({

    propTypes: {
        pivotData: React.PropTypes.array
    },

    getDefaultProps(){
        return {
            loading: false,
            pivotData:[]
        }
    },

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            language: 'US',
            updating: false,
            errors: {},
            completed: 0,
            delayedAmount:0,
            successAmount:0,
            failedAmount:0
        }
    },

    sparklineBar: function (id, values, height, barWidth, barColor, barSpacing) {
        $('.' + id).sparkline(values, {
            type: 'bar',
            height: height,
            barWidth: barWidth,
            barColor: barColor,
            barSpacing: barSpacing
        })
    },

    sparklineLine: function (id, values, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
        $('.' + id).sparkline(values, {
            type: 'line',
            width: width,
            height: height,
            lineColor: lineColor,
            fillColor: fillColor,
            lineWidth: lineWidth,
            maxSpotColor: maxSpotColor,
            minSpotColor: minSpotColor,
            spotColor: spotColor,
            spotRadius: spotRadius,
            highlightSpotColor: hSpotColor,
            highlightLineColor: hLineColor
        });
    },

    easyPieChart: function (id, trackColor, scaleColor, barColor, lineWidth, lineCap, size) {
        $('.' + id).easyPieChart({
            trackColor: trackColor,
            scaleColor: scaleColor,
            barColor: barColor,
            lineWidth: lineWidth,
            lineCap: lineCap,
            size: size
        });
    },

    plotSparkLinesAndBars: function (){
        /* Dash Widget Line Chart */
        if ($('.dash-widget-visits')[0]) {
            this.sparklineLine('dash-widget-visits', [9, 4, 6, 5, 6, 4, 5, 7, 9, 3, 6, 5], '100%', '70px', 'rgba(255,255,255,0.7)', 'rgba(0,0,0,0)', 2, '#fff', '#fff', '#fff', 5, 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.1)');
        }

        /* Mini Chart - Line Chart 1 */
        if ($('.stats-line')[0]) {
            this.sparklineLine('stats-line', [9, 4, 6, 5, 6, 4, 5, 7, 9, 3, 6, 5], 68, 35, '#fff', 'rgba(0,0,0,0)', 1.25, 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 3, '#fff', 'rgba(255,255,255,0.4)');
        }

        /* Mini Chart - Bar Chart 2 */
        if ($('.stats-bar-2')[0]) {
            this.sparklineBar('stats-bar-2', [4, 7, 6, 2, 5, 3, 8, 6, 6, 4, 8, 6, 5, 8], '35px', 3, '#fff', 2);
        }
        /* Mini Chart - Bar Chart 1 */
        if ($('.stats-bar')[0]) {
            this.sparklineBar('stats-bar', [6, 4, 8, 6, 5, 6, 7, 8, 3, 5, 9, 5, 8, 4], '35px', 3, '#fff', 2);
        }
    },

    componentDidMount: function() {
        var pivotDataLengthCounter = 0;
        let {props} = this;
        var delayedAmount = 0;
        var failedAmount = 0;
        var successfulAmount = 0;

        this.plotSparkLinesAndBars();
        let pivotDataLength = props.pivotData.length;
        {props.pivotData.map((pivotData) => {
            var REFERENCE = moment();   //REFERENCE IS 1466158779454
            var TODAY = REFERENCE.clone().startOf('day');// TODAY is 1466110800000
            var YESTERDAY = REFERENCE.clone().subtract(1, 'days').startOf('day');// THIS_WEEK is 1466110800000
            var pivotDataDate = moment(pivotData.date); //PIVOT DATE IS 1466037419001
            pivotDataLengthCounter +=1;
            if(pivotDataLengthCounter <=pivotDataLength) {
                if (pivotDataDate >= YESTERDAY && pivotDataDate <= TODAY) {
                    delayedAmount += Number(pivotData.delayedAmount);
                    failedAmount += Number(pivotData.failedAmount);
                    successfulAmount += Number(pivotData.successfulAmount);
                }
            }
        })}
        this.setState({delayedAmount: delayedAmount,failedAmount:failedAmount,successfulAmount:successfulAmount});
    },

    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state} = this;
        return (
            <div className="col-md-4 col-sm-6">
                <div className="dw-item bgm-bluegray" id="site-visits">
                    <div className="dwi-header">
                        <div className="p-30">
                            <div className="dash-widget-visits"/>
                        </div>
                        <div className="dwih-title">For Yesterday</div>
                    </div>
                    <div className="list-group lg-even-white">
                        <div className="list-group-item media sv-item">
                            <div className="pull-right">
                                <div className="stats-bar"/>
                            </div>
                            <div className="media-body">
                                <small>Delayed</small>
                                <h3>{state.delayedAmount}</h3>
                            </div>
                        </div>
                        <div className="list-group-item media sv-item">
                            <div className="pull-right">
                                <div className="stats-bar-2"/>
                            </div>
                            <div className="media-body">
                                <small>Successful</small>
                                <h3>{state.successfulAmount}</h3>
                            </div>
                        </div>
                        <div className="list-group-item media sv-item">
                            <div className="pull-right">
                                <div className="stats-line"/>
                            </div>
                            <div className="media-body">
                                <small>Failed</small>
                                <h3>{state.failedAmount}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    onLanguage(language){
        debug("onLanguage: ", language);
        this.setState({language: language});
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    }
});