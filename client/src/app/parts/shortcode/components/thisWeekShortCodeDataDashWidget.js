import React from 'react';
import moment from 'moment';
import $ from 'jquery';
import Debug from 'debug';
let debug = new Debug("components:thisWeekShortCodeDataDashWidget");

export default React.createClass({
    propTypes: {
        pivotData: React.PropTypes.array
    },
    getDefaultProps(){
        return {
            loading: false,
            pivotData:[]
        }
    },
    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            language: 'US',
            updating: false,
            errors: {},
            completed: 0,
            delayedAmountPercentage:0,
            successfulAmountPercentage:0,
            failedAmountPercentage:0
        }
    },

    sparklineBar: function (id, values, height, barWidth, barColor, barSpacing) {
        $('.' + id).sparkline(values, {
            type: 'bar',
            height: height,
            barWidth: barWidth,
            barColor: barColor,
            barSpacing: barSpacing
        })
    },

    sparklineLine: function (id, values, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
        $('.' + id).sparkline(values, {
            type: 'line',
            width: width,
            height: height,
            lineColor: lineColor,
            fillColor: fillColor,
            lineWidth: lineWidth,
            maxSpotColor: maxSpotColor,
            minSpotColor: minSpotColor,
            spotColor: spotColor,
            spotRadius: spotRadius,
            highlightSpotColor: hSpotColor,
            highlightLineColor: hLineColor
        });
    },

    easyPieChart: function (id, trackColor, scaleColor, barColor, lineWidth, lineCap, size) {
        $('.' + id).easyPieChart({
            trackColor: trackColor,
            scaleColor: scaleColor,
            barColor: barColor,
            lineWidth: lineWidth,
            lineCap: lineCap,
            size: size
        });
    },

    plotSparkLinesAndBars: function (){
        /* Main Pie Chart */
        if ($('.main-pie')[0]) {
            this.easyPieChart('main-pie', 'rgba(255,255,255,0.2)', 'rgba(255,255,255,0)', 'rgba(255,255,255,0.7)', 2, 'butt', 148);
        }

        /* Others */
        if ($('.sub-pie-1')[0]) {
            this.easyPieChart('sub-pie-1', 'rgba(255,255,255,0.2)', 'rgba(255,255,255,0)', 'rgba(255,255,255,0.7)', 2, 'butt', 90);
        }

        if ($('.sub-pie-2')[0]) {
            this.easyPieChart('sub-pie-2', 'rgba(255,255,255,0.2)', 'rgba(255,255,255,0)', 'rgba(255,255,255,0.7)', 2, 'butt', 90);
        }
    },

    calculatePercentage:function(amount,totalCount){
        return (isNaN(Number(amount)) ? 0 : ((Number(amount)/totalCount)*100).toFixed(0));
    },

    componentDidMount: function() {
        var pivotDataLengthCounter = 0;
        let {props} = this;
        var delayedAmount = 0;
        var failedAmount = 0;
        var successfulAmount = 0;
        let _this = this;

        this.plotSparkLinesAndBars();
        let pivotDataLength = props.pivotData.length;
        {props.pivotData.map((pivotData) => {
            var REFERENCE = moment();   //REFERENCE IS 1466158779454
            var THIS_WEEK = REFERENCE.clone().subtract(7, 'days').startOf('day');// THIS_WEEK is 1466110800000
            var pivotDataDate = moment(pivotData.date); //PIVOT DATE IS 1466037419000
            if(pivotDataLengthCounter <=pivotDataLength) {
                if (pivotDataDate >= THIS_WEEK && pivotDataDate <= REFERENCE) {
                    pivotDataLengthCounter +=1;
                    delayedAmount += Number(pivotData.delayedAmount);
                    failedAmount += Number(pivotData.failedAmount);
                    successfulAmount += Number(pivotData.successfulAmount);
                }
            }
        })}

        this.setState({
            delayedAmountPercentage: _this.calculatePercentage(delayedAmount, pivotDataLengthCounter),
            failedAmountPercentage:_this.calculatePercentage(failedAmount, pivotDataLengthCounter),
            successfulAmountPercentage:_this.calculatePercentage(successfulAmount, pivotDataLengthCounter)});

    },

    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state} = this;
        return (
            <div className="col-md-4 col-sm-6">
                <div className="dw-item bgm-pink c-white" id="pie-charts">
                    <div className="dw-item">
                        <div className="dwi-header">
                            <div className="dwih-title">This Week's Statistics</div>
                        </div>
                        <div className="clearfix"/>
                        <div className="text-center p-20 m-t-25">
                            <div data-percent={100} className="easy-pie main-pie">
                                <div className="percent">100</div>
                                <div className="pie-title">Total Test SMSes Sent</div>
                            </div>
                        </div>
                        <div className="p-t-25 p-b-20 text-center">
                            <div data-percent={state.delayedAmountPercentage} className="easy-pie sub-pie-1">
                                <div className="percent">{state.delayedAmountPercentage}</div>
                                <div className="pie-title">Delayed</div>
                            </div>
                            <div data-percent={state.successfulAmountPercentage} className="easy-pie sub-pie-2">
                                <div className="percent">{state.successfulAmountPercentage}</div>
                                <div className="pie-title">Successful</div>
                            </div>
                            <div data-percent={state.failedAmountPercentage} className="easy-pie sub-pie-2">
                                <div className="percent">{state.failedAmountPercentage}</div>
                                <div className="pie-title">Failed</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    onLanguage(language){
        debug("onLanguage: ", language);
        this.setState({language: language});
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    }

});