import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {connect} from 'react-redux';
import ShortCodeView from './views/shortCodeView';

function Resources(rest) {
    return {
        getShortCode(id) {
            return rest.get('shortcodes/' + id);
        },

        getShortCodePivotData(id) {
            return  rest.get('pivotData/' + id);
        }
    }
}

function Actions(rest) {
    let shortcode = Resources(rest);
    return {
        getShortCode: createActionAsync('SHORTCODE_GET', shortcode.getShortCode),
        getShortCodePivotData: createActionAsync('SHORTCODE_PIVOT_DATA_GET', shortcode.getShortCodePivotData)
    }
}

function Reducers(actions) {
    return {
        shortcode: createReducerAsync(actions.getShortCode),
        shortCodePivotData: createReducerAsync(actions.getShortCodePivotData)
    }
}

function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        shortcode(){
            const mapStateToProps = (state) => ({
                shortcode: state.get('shortcode').toJS(),
                shortCodePivotData: state.get('shortCodePivotData').toJS()
            });
            return connect(mapStateToProps, mapDispatchToProps)(ShortCodeView);
        }
    }
}

function Routes(containers) {
    return (<Route path="/shortcode/:shortcodeId" component={containers.shortcode()}/>)
}

export default function (rest) {
    let actions = Actions(rest);
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}