import React from 'react';
import ShortCodeForm from '../components/shortCodeForm';

export default React.createClass({

    propTypes: {},

    componentDidMount () {
        var id = this.props.params.shortcodeId;
        this.props.actions.getShortCode(id);
        this.props.actions.getShortCodePivotData(id);
    },

    render () {
        let {props} = this;
        return (<ShortCodeForm {...props}/>);
    }
})