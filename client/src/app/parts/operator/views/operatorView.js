import React from 'react';
import DocTitle from 'components/docTitle';
import OperatorForm from '../components/operatorForm';
import LaddaButton from 'react-ladda';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("views:operatorView");

export default React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    propTypes: {},

    componentDidMount () {
        var id = this.props.params.operatorId;
        this.props.actions.getOperator(id);
    },

    render () {
        let {props} = this;
        let _this = this;
        return (
            <Paper className="text-center view">
                <DocTitle title="Edit Operator|Livemonitor"/>
                <h2>Edit Operator</h2>
                {props.operatorUpdate.data.success && _this.renderOperatorUpdateComplete()}
                {props.operatorDelete.data.success && _this.renderOperatorDeleteComplete()}
                {(!props.operatorUpdate.data.success && !props.operatorDelete.data.success) && _this.renderOperatorForm()}
            </Paper>
        );
    },
    renderOperatorUpdateComplete(){
        let _this = this;
        return (
        <div className="local-login-form">
            <div className="alert alert-info text-center" role="alert">
                The Operator you changed has been successfully updated.
            </div>
            <div className="form-group col-md-6">
                <LaddaButton
                    className='btn btn-lg btn-primary'
                    id='btn-login'
                    buttonColor='blue'
                    progress={.5}
                    buttonStyle="slide-up"
                    onClick={_this.handleOpen}>Go Home</LaddaButton>
            </div>
        </div>
        );
    },
    renderOperatorDeleteComplete(){
        let _this = this;
        return (
            <div className="local-login-form">
                <div className="alert alert-info text-center" role="alert">
                    The Operator you changed has been successfully deleted.
                </div>
                <div className="form-group col-md-6">
                    <LaddaButton
                        className='btn btn-lg btn-primary'
                        id='btn-login'
                        buttonColor='blue'
                        progress={.5}
                        buttonStyle="slide-up"
                        onClick={_this.handleOpen}>Go Home</LaddaButton>
                </div>
            </div>
        );
    },
    renderOperatorForm(){
        let {props} = this;
        return (<OperatorForm {...props}/>
        );
    }
})