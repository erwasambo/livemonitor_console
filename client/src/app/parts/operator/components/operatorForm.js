import _ from 'lodash';
import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import TextArea from 'react-textarea-autosize';
import LaddaButton from 'react-ladda';
import CircularProgress from 'material-ui/lib/circular-progress';
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';
import ValidateOperatorFields from '../../../services/validateOperatorFields';
import Debug from 'debug';
let debug = new Debug("components:operatorForm");

export default React.createClass({
    propTypes: {
        operator: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            loading: false,
            operator: {}
        }
    },
    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.operator.data || {});
    },
    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            open: false,
            language: 'US',
            updating: false,
            deleting: false,
            errors: {},
            completed: 0
        }
    },
    handleOpen(evt) {
        evt.preventDefault();
        this.setState({open: true});
    },

    handleClose(){
        this.setState({open: false});
    },

    handleDeleteClose(){
        this.setState({open: false});
        this.deleteOperator();
    },

    renderUpdateError(){
        let {error} = this.props.operatorUpdate;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    renderDeleteError(){
        let {error} = this.props.operatorDelete;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    handleChange: function (event) {
        this.setState({name: event.target.value});
    },

    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;
        if (props.operator.loading) {
            return <CircularProgress size={2} />
    }
        const actions = [
            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={_this.handleClose}
            />,
            <FlatButton
                label="Delete"
                primary={true}
                keyboardFocused={true}
                onTouchTap={_this.handleDeleteClose}
            />
        ];

        return (
            <div className="local-login-form">
                <Dialog
                    title="Delete Operator"
                    actions={actions}
                    modal={false}
                    open={_this.state.open}
                    onRequestClose={_this.handleClose}>
                    Are you sure you want to delete {state.name} Operator?
                </Dialog>

                <form className="form-horizontal">
                    <div className="signup-options text-center form">
                        {_this.renderUpdateError() && _this.renderDeleteError()}

                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="id"
                                        hintText="Operator Id"
                                        floatingLabelText="Operator Id"
                                        value={state.id}
                                        disabled={true}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    ref="name"
                                    hintText="Operator Name"
                                    floatingLabelText="Operator Name"
                                    value={state.name}
                                    onChange={_this.handleChange}
                                    errorText={errors.name && errors.name[0]}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <LaddaButton
                                        className='btn btn-lg btn-primary'
                                        id='btn-login'
                                        buttonColor='blue'
                                        loading={_this.props.operatorUpdate.loading}
                                        progress={.5}
                                        buttonStyle="slide-up"
                                        onClick={_this.updateOperator}>Update</LaddaButton>
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <LaddaButton
                                    className='btn btn-lg btn-primary'
                                    id='btn-login'
                                    buttonColor='red'
                                    loading={_this.props.operatorDelete.loading}
                                    progress={.5}
                                    buttonStyle="slide-up"
                                    onClick={_this.handleOpen}>Delete</LaddaButton>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    },

    onLanguage(language){
        debug("onLanguage: ", language);
        this.setState({language: language});
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    },

    updateOperator(evt) {
        evt.preventDefault();
        debug('updateOperator ', this.state);
        this.setState({
            errors: {},
            updating: true
        });

        validateForm.call(this)
            .with(this)
            .then(save)
            .then(successNotification)
            .catch(Checkit.Error, (error) => this.setState({
                errors: error.toJSON()
            }))
            .catch(setErrors)
            .then(() => {
                this.setState({
                    updating: false
                });
            });

        function validateForm() {
            return new ValidateOperatorFields({
                name: this.state.name
            }).execute();
        }

        function save() {
            return this.props.actions.updateOperator(this.state);
        }

        function successNotification() {
        }

        function setErrors(e) {
            debug('setErrors: ', e);
            if (error instanceof Checkit.Error) {
                this.setState({errors: error.toJSON()})
            }
        }
    },
    deleteOperator() {
        debug('deleteOperator ', this.state);
        this.setState({
            errors: {},
            deleting: true
        });

        deleteOprt.call(this)
            .with(this)
            .then(successNotification)
            .catch(Checkit.Error, (error) => this.setState({
                errors: error.toJSON()
            }))
            .catch(setErrors)
            .then(() => {
                this.setState({
                    deleting: false
                });
            });

        function deleteOprt() {
            return this.props.actions.deleteOperator(this.state.id);
        }

        function successNotification() {
        }

        function setErrors(e) {
            debug('setErrors: ', e);
            if (error instanceof Checkit.Error) {
                this.setState({errors: error.toJSON()})
            }
        }
    }

});

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}