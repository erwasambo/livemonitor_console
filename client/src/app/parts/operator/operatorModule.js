import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {connect} from 'react-redux';
import OperatorView from './views/operatorView';

function Resources(rest) {
    return {
        getOperator(id) {
            return rest.get('operators/' + id);
        },

        updateOperator(payload) {
            return rest.put('operators/' + payload.id, payload);
        },

        deleteOperator(id) {
            return rest.del('operators/' + id);
        }
    }
}

function Actions(rest) {
    let operator = Resources(rest);
    return {
        getOperator: createActionAsync('OPERATOR_GET_IND', operator.getOperator),
        updateOperator: createActionAsync('OPERATOR_UPDATE_IND', operator.updateOperator),
        deleteOperator: createActionAsync('OPERATOR_DELETE_IND', operator.deleteOperator)
    }
}

function Reducers(actions) {
    return {
        operator: createReducerAsync(actions.getOperator),
        operatorUpdate: createReducerAsync(actions.updateOperator),
        operatorDelete: createReducerAsync(actions.deleteOperator)
    }
}
function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        operator(){
            const mapStateToProps = (state) => ({
                operator: state.get('operator').toJS(),
                operatorUpdate: state.get('operatorUpdate').toJS(),
                operatorDelete: state.get('operatorDelete').toJS()
            });
            return connect(mapStateToProps, mapDispatchToProps)(OperatorView);
        }
    }
}

function Routes(containers) {
    return (<Route path="/operator/:operatorId" component={containers.operator()}/>)
}

export default function (rest) {
    let actions = Actions(rest);
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}