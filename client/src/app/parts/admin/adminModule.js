import React from 'react';
import {Route, IndexRoute} from 'react-router';
import {connect} from 'react-redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {bindActionCreators} from 'redux';
import AdminPanelView from './views/adminPanelView';
import AddOperatorView from './views/addOperatorView';
import AddShortCodeView from './views/addShortCodeView';

function Resources(rest) {
    return {

        addOperator(payload) {
            return rest.post('operators/', payload);
        },
        addShortCode(payload) {
            return rest.post('shortcodes/', payload);
        },
        getUsers(data) {
            return rest.get('users/', data);
        },
        getOperators() {
            return rest.get('operators/');
        },
        getFilteredOperators(data) {
            return rest.get('operators/', data);
        },
        getShortCodes() {
            return rest.get('shortcodes/');
        },
        getFilteredShortCodes(data) {
            return rest.get('shortcodes/', data);
        },
        getDevices() {
            return rest.get('devices/');
        },
        getTriageLogs(data) {
            return rest.get('triagelogs/', data);
        },
        getSentMessages(data) {
            return rest.get('sentMessages/', data);
        },
        getReceivedMessages(data) {
            return rest.get('receivedMessages/', data);
        },
        getMyallocatedShortCodeIds() {
            return rest.get('me/shortCodes');
        }
    }
}

function Actions(rest) {
    let admin = Resources(rest);
    return {
        //Add actions
        addOperator: createActionAsync('ADD_OPERATOR_ADMIN', admin.addOperator),
        //Get actions
        getFilteredOperators: createActionAsync('OPERATORS_GET_FILTERED_ADMIN', admin.getFilteredOperators),
        getOperators: createActionAsync('OPERATORS_GET_ADMIN', admin.getOperators),
        getFilteredShortCodes: createActionAsync('SHORT_CODES_GET_FILTERED_ADMIN', admin.getFilteredShortCodes),
        getShortCodes: createActionAsync('SHORT_CODES_GET_ADMIN', admin.getShortCodes),
        getUsers: createActionAsync('USERS_GET_ADMIN', admin.getUsers),
        getMyallocatedShortCodeIds: createActionAsync('ADMIN_SHORT_CODE_GET', admin.getMyallocatedShortCodeIds)
    }
}

function Reducers(actions) {
    return {
        //Add reducers
        operatoradd: createReducerAsync(actions.addOperator),

        //Get Reducers
        operatorsgetfiltered: createReducerAsync(actions.getFilteredOperators),
        operatorsget: createReducerAsync(actions.getOperators),
        shortcodesgetfiltered: createReducerAsync(actions.getFilteredShortCodes),
        shortcodesget: createReducerAsync(actions.getShortCodes),
        usersget: createReducerAsync(actions.getUsers),
        myshortcodesGet: createReducerAsync(actions.getMyallocatedShortCodeIds)

    }
}

function Containers(actions, resources) {
    return {
        adminpanel(){
            const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
            const mapStateToProps = (state) => ({resources});
            return connect(mapStateToProps, mapDispatchToProps)(AdminPanelView);
        },
        addOperator(){
            const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
            const mapStateToProps = (state) => ({
                operatoradd: state.get('operatoradd').toJS()
            });
            return connect(mapStateToProps, mapDispatchToProps)(AddOperatorView);
        }
    }
}

function Routes(containers) {
    return (
        <Route path="/admin">
            <IndexRoute component={containers.adminpanel()}/>
            <Route component={containers.addOperator()} path="addOperator"/>
        </Route>
    )
}

export default function (rest) {
    let actions = Actions(rest);
    let resources = Resources(rest);
    let containers = Containers(actions, resources);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}