import React from 'react';
import ReceivedMessageRestTableComponent from '../../../components/receivedMessageRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:receivedMessages");

const columns = [
    {
        property: 'id',
        header: 'ID'
    },
    {
        property: 'receivedMessageId',
        header: 'Received Message'
    },
    {
        property: 'address',
        header: 'Address'
    },
    {
        property: 'body',
        header: 'Body'
    },
    {
        property: 'device',
        header: 'Device'
    },
    {
        property: 'timeStamp',
        header: 'TimeStamp'
    },
    {
        property: 'createdAt',
        header: 'Created At'
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">Received Messages</h3>
                </div>
                <div className="panel-body">
                    <ReceivedMessageRestTableComponent {...this.props} pathName={"receivedMessage"} columns={columns}
                                                        getData={this.props.resources.getReceivedMessages}/>
                </div>
            </div>
        );
    }
});
