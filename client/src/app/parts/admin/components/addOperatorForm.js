import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import LaddaButton from 'react-ladda';
import Debug from 'debug';

let debug = new Debug("components:addOperatorForm");

export default React.createClass({
    propTypes: {
        addOperator: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            loading: false,
            addOperator: {}
        }
    },
    getInitialState() {
        return {
            updating: false,
            loading: false,
            errors: {},
            completed: 0
        };
    },
    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.operatoradd.data || {});
    },

    renderError(){
        let {error} = this.props.operatoradd;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },
    render() {
        debug('render state:', this.state);
        debug('render props:', this.props);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;

        return (
            <div className="local-login-form">
                <form>
                    <div className="signup-options text-center form">
                        {_this.renderError()}
                        <div className='form-group name'>
                            <TextField
                                ref="name"
                                hintText="Operator Name"
                                floatingLabelText="Operator Name"
                                errorText={errors.name && errors.name[0]}
                            />
                        </div>

                        <div className='btn-signup'>
                            <LaddaButton
                                className='btn btn-lg btn-primary'
                                id='btn-login'
                                buttonColor='green'
                                loading={props.operatoradd.loading}
                                progress={.5}
                                buttonStyle="slide-up"
                                onClick={_this.addOperator}>Create Operator</LaddaButton>
                        </div>
                    </div>
                </form>
            </div>
        );
    },

    addOperator(evt) {
        evt.preventDefault();
        let {name} = this.refs;
        let payload = {name: name.getValue()};

        return validateOperator.call(this, payload)
            .with(this)
            .then(this.props.actions.addOperator)
            .catch(setErrors);
    }
});

function validateOperator(payload) {
    let rules = new Checkit({
        name: ['required', 'alphaDash', 'minLength:3', 'maxLength:64']
    });
    return rules.run(payload);
}

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}