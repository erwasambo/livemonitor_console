import React from 'react';
import DeviceRestTableComponent from '../../../components/deviceRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:devices");

const columns = [
    {
        property: 'id',
        header: 'Id'
    },
    {
        property: 'deviceId',
        header: 'Device'
    },
    {
        property: 'phoneNumber',
        header: 'Phone Number'
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">Devices</h3>
                </div>
                <div className="panel-body">
                    <DeviceRestTableComponent {...this.props} pathName={"device"} columns={columns}
                                                        getData={this.props.resources.getDevices}/>
                </div>
            </div>
        );
    }
});