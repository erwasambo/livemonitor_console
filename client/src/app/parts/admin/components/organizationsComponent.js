import React from 'react';
import { Link } from 'react-router'
import RestTableComponent from '../../../components/restTableComponent';

import Debug from 'debug';
let debug = new Debug("components:organizations");

const columns = [
    {
        property: 'name',
        header: 'Name'
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <div className="pull-right">
                        <Link to={'/admin/addOrganization'}>
                            <button className="btn btn-xs btn-success">
                                Add Organization
                            </button>
                        </Link>
                    </div>
                    <h3 className="panel-title">Organizations</h3>
                </div>
                <div className="panel-body">
                    <RestTableComponent {...this.props} pathName={"organization"} columns={columns}
                                                        getData={this.props.resources.getOrganizations}/>
                </div>
            </div>
        );
    }
});