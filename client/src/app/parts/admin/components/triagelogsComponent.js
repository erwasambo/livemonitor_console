import React from 'react';
import TriageLogRestTableComponent from '../../../components/triageLogRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:triagelogs");

const columns = [
    {
        property: 'sentMessageId',
        header: 'Sent Message'
    },
    {
        property: 'triageStatus',
        header: 'Status'
    },
    {
        property: 'timeStamp',
        header: 'TimeStamp'
    },
    {
        property: 'createdAt',
        header: 'Created At'
    }
];


export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">Triage Logs</h3>
                </div>
                <div className="panel-body">
                    <TriageLogRestTableComponent
                        columns={columns}
                        getData={this.props.resources.getTriageLogs}/>
                </div>
            </div>
        );
    }
});
