import React from 'react';
import { Link } from 'react-router'
import moment from 'moment';
import ShortCodeRestTableComponent from '../../../components/shortCodeRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:shortcodes");

const columns = [
    {
        property: 'name',
        header: 'Name'
    },
    {
        property: 'description',
        header: 'Description'
    },
    {
        property: 'deviceNumber',
        header: 'Device Number'
    },
    {
        property: 'operator',
        header: 'Operator'
    },
    {
        property: 'email',
        header: 'Owner Email'
    },
    {
        property: 'createdAt',
        header: 'Created At',
        cell: (v) => moment.utc(v).format('LLLL')
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <div className="pull-right">
                        <Link to={'/newShortCode'}>
                            <button className="btn btn-xs btn-success">
                                Add Short Code
                            </button>
                        </Link>
                    </div>
                    <h3 className="panel-title">Short Codes</h3>
                </div>
                <div className="panel-body">
                    <ShortCodeRestTableComponent {...this.props} pathName={"editShortCode"} columns={columns}
                                                        getData={this.props.resources.getFilteredShortCodes}/>
                </div>
            </div>
        );
    }
});