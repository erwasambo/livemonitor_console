import React, {PropTypes} from 'react';
import Debug from 'debug';
import firebase from 'firebase';
import Paper from 'material-ui/lib/paper';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import LaddaButton from 'react-ladda';
let debug = new Debug("components:customizedEmailMessages");

export default React.createClass({

    getInitialState () {
        return {
            finalValue: "Loading...",
            format: 'html'
        };
    },

    componentDidMount () {
        let _this = this;
        firebase.database().ref('emailTemplate').once("value", function (snapshot) {
            debug('loadTemplate: function(evt) emailTemplate', snapshot.val());
            _this.setState({emailTemplate:snapshot.val()});
        });
    },

    handleEmailTemplateChange: function (event) {
        this.setState({emailTemplate: event.target.value});
    },

    render () {
        debug('render ', this.state);
        let _this = this;
        let {state} = this;
        return (
            <Paper>
                <div className="local-login-form">
                    <form className="form-horizontal">
                        <h3 className="panel-title">Customized Alert Email Template</h3>
                        <div className="signup-options text-center form">
                            <div className="editor-demo">
                                <div className="row">
                                </div>
                                <div className="row">
                                    <TextField
                                        style={{width:'100%'}}
                                        hintText="Default email sending template"
                                        floatingLabelText="Alert Message Template"
                                        value = {state.emailTemplate}
                                        onChange={_this.handleEmailTemplateChange}
                                        multiLine={true}
                                        rows={6}
                                    />
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="form-group col-md-12">
                                        <LaddaButton
                                            className='btn btn-lg btn-primary'
                                            id='btn-login'
                                            buttonColor='green'
                                            buttonStyle="slide-up"
                                            onClick={_this.saveTemplate}>Save</LaddaButton>
                                    </div>
                                </div>
                                <br/>
                            </div>
                        </div>
                    </form>
                </div>
            </Paper>
        );
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    },

    saveTemplate: function(evt) {
        evt.preventDefault();
        let {state} = this;
        firebase.database().ref('emailTemplate').set(state.emailTemplate);
    }
});