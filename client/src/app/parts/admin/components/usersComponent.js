import React from 'react';
import moment from 'moment';
import UserRestTableComponent from '../../../components/userRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:users");

const columns = [
    {
        property: 'username',
        header: 'Username'
    },
    {
        property: 'email',
        header: 'Email'
    },
    {
        property: 'firstName',
        header: 'First Name'
    },
    {
        property: 'lastName',
        header: 'Last Name'
    },
    {
        property: 'location',
        header: 'Location'
    },
    {
        property: 'createdAt',
        header: 'Created At',
        cell: (v) => moment.utc(v).format('LLLL')
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">Users</h3>
                </div>
                <div className="panel-body">
                    <UserRestTableComponent {...this.props} pathName={"user"} columns={columns}
                                                        getData={this.props.resources.getUsers}/>
                </div>
            </div>
        );
    }
});