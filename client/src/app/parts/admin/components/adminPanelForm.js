import React from 'react';
import DocTitle from '../../../components/docTitle';
import UsersComponent from './../components/usersComponent';
import TriageLogsComponent from './../components/triagelogsComponent';
import OperatorsComponent from './../components/operatorsComponent';
import ShortCodesComponent from './../components/shortCodesComponent';
import SentMessagesComponent from './../components/sentMessagesComponent';
import ReceivedMessagesComponent from './../components/receivedMessagesComponent';
import CustomizedEmailMessageComponent from './../components/customizedEmailMessageComponent';
import Paper from 'material-ui/lib/paper';
import CircularProgress from 'material-ui/lib/circular-progress';
import Debug from 'debug';
let debug = new Debug("components:adminPanelForm");

export default React.createClass({

    propTypes: {
        adminpanel: React.PropTypes.object,
        getMyallocatedShortCodeIds: React.PropTypes.func.isRequired
    },

    getDefaultProps(){
        return {
            loading: false,
            count: 0,
            shortCodeLoading: false,
            adminpanel: {}
        }
    },

    getInitialState() {
        return {
            loading: false,
            count: 0,
            ShortCodes: [],
            updating: false,
            completed: 0
        }
    },
    renderLoading(){
        if(this.state.loading){
            return (<Paper className='profile-view view'>
                <CircularProgress size={2} />
            </Paper>);
        }
    },
    renderAdminOnlyItems(){
        let {state, props} = this;
        if(state.username == "admin") {
            return (
                <section id="main">
                    <section id="content">
                        <div className="container">
                            <DocTitle title="Admin Panel"/>
                            <OperatorsComponent {...props}/>
                            <ShortCodesComponent {...props}/>
                            <UsersComponent {...props}/>
                            <TriageLogsComponent {...props}/>
                            <SentMessagesComponent {...props}/>
                            <ReceivedMessagesComponent {...props}/>
                            <CustomizedEmailMessageComponent {...props}/>
                        </div>
                    </section>
                </section>
            );
        };
    },

    renderNonAdminMessage(){
        if(this.state.username != "admin") {
            return (<Paper className='profile-view view'>
                <h3>You must be admin to access these privileges</h3>
            </Paper>);
        }
    },

    render () {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        return (
            <div>
                {this.renderNonAdminMessage()}
                {this.renderAdminOnlyItems()}
                {this.renderLoading()}
            </div>
        );
    },
    componentDidMount () {
        //debug('componentDidMount ', this.props);
        this.onSelectPage(this.props);
    },
    componentWillReceiveProps(props){
        //debug('componentWillReceiveProps ', props);
        this.onSelectPage(props);
    },

    onSelectPage(){
        this.setSelectPage(this.props);
    },

    setSelectPage(props){
        this.setState({loading: true});
        props.getMyallocatedShortCodeIds()
            .then(result => {
                if (this.isMounted()) {
                    this.setState({
                        username: result.username,
                        loading: false
                    });
                }
            })
            .catch(err => {
                debug('error: ', err);
                if (this.isMounted()) {
                    this.setState({
                        error: err,
                        loading: false
                    });
                }
            });
    }
});