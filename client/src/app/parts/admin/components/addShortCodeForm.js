import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import Paper from 'material-ui/lib/paper';
import LaddaButton from 'react-ladda';
import Debug from 'debug';
let debug = new Debug("components:AddShortCodeForm");

export default React.createClass({
    propTypes: {
        addshortcode: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            loading: false,
            addshortcode: {}
        }
    },

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            operatorName: "",
            operatorId: "",
            operatorData: [],
            errors: {},
            updating: false,
            loading: false,
            operators: {},
            completed: 0,
            value: 1
        };
    },

    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.shortcodeadd.data || {});
        this.setState(nextProps.operatorsget.data || {});
        debug("componentWillReceiveProps  operatorsget LOG", JSON.stringify(nextProps.operatorsget.data));
    },

    handleChange: function (event, index, operatorId) {
        let selectedOperator = this.state.operatorData[index];
        this.setState({value: operatorId, operatorId: operatorId, operatorName: selectedOperator.name});
        debug("handleChange: ", "operatorId is: " + this.state.operatorId + 'operatorName is: ' + this.state.operatorName);
    },

    renderError(){
        let {error} = this.props.shortcodeadd;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    render() {
        debug('render state:', this.state);
        debug('render props:', this.props);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;

        return (
            <section id="main">
                <section id="content">
                    <div className="container">
                        <Paper className='operator-view view text-center'>
                            <h2>Add Short Code</h2>
                            <form>
                                <div className="signup-options text-center form">
                                    {_this.renderError()}
                                    <div className='form-group name'>
                                        <TextField
                                            ref="name"
                                            hintText="Name"
                                            floatingLabelText="Name"
                                            errorText={errors.name && errors.name[0]}
                                        />
                                    </div>
                                    <div className='form-group description'>
                                        <TextField
                                            ref="description"
                                            hintText="Description"
                                            floatingLabelText="Description"
                                            errorText={errors.description && errors.description[0]}
                                        />
                                    </div>
                                    <div className='form-group deviceNumber'>
                                        <TextField
                                            ref="deviceNumber"
                                            hintText="Device Number"
                                            floatingLabelText="Device Number"
                                            errorText={errors.deviceNumber && errors.deviceNumber[0]}
                                        />
                                    </div>
                                    <div className='form-group operator'>
                                        <SelectField
                                            ref="operator"
                                            hintText="Operator"
                                            onChange={_this.handleChange}
                                            errorText={state.errors.operator && state.errors.operator[0]}>
                                            {_this.state.operatorData.map((operator) => {
                                                return (<MenuItem key={operator.id} value={operator.id} primaryText={operator.name}/>);
                                            })}
                                        </SelectField>
                                    </div>
                                    <div className='form-group email'>
                                        <TextField
                                            ref="email"
                                            hintText="Owner Email"
                                            floatingLabelText="Owner Email"
                                            errorText={errors.email && errors.email[0]}
                                        />
                                    </div>
                                    <div className='btn-signup'>
                                        <LaddaButton
                                            className='btn btn-lg btn-primary'
                                            id='btn-login'
                                            buttonColor='green'
                                            loading={props.shortcodeadd.loading}
                                            progress={.5}
                                            buttonStyle="slide-up"
                                            onClick={_this.addShortCode}>Create Short Code</LaddaButton>
                                    </div>
                                </div>
                            </form>
                        </Paper>
                    </div>
                </section>
            </section>
        );
    },

    addShortCode(evt) {
        evt.preventDefault();
        let {name, description, deviceNumber, email} = this.refs;
        let operatorId = JSON.stringify(this.state.operatorId);
        let operatorName = this.state.operatorName;
        let payload = {
            name: name.getValue(),
            description: description.getValue(),
            deviceNumber: deviceNumber.getValue(),
            operatorId: operatorId,
            operatorName: operatorName,
            email: email.getValue()
        };
        return validateShortCode.call(this, payload)
            .with(this)
            .then(this.props.actions.addShortCode)
            .catch(setErrors);

    }
});

function validateShortCode(payload) {
    let rules = new Checkit({
        name: ['required', 'numeric'],
        description: ['minLength:6', 'maxLength:64'],
        operatorId: ['required'],
        operatorName: ['required'],
        deviceNumber: ['alphaNumeric', 'minLength:6', 'maxLength:64'],
        email: ['required', 'email', 'minLength:6', 'maxLength:64']
    });

    return rules.run(payload);
}

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}