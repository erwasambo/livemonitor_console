import React from 'react';
import SentMessageRestTableComponent from '../../../components/sentMessageRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:sentMessages");

const columns = [
    {
        property: 'sentMessageId',
        header: 'sent Message'
    },
    {
        property: 'device',
        header: 'Device'
    },
    {
        property: 'address',
        header: 'Address'
    },
    {
        property: 'body',
        header: 'Message Text'
    },
    {
        property: 'shortCode',
        header: 'Short Code'
    },
    {
        property: 'createdAt',
        header: 'Created At'
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">Sent Messages</h3>
                </div>
                <div className="panel-body">
                    <SentMessageRestTableComponent {...this.props} pathName={"sentMessage"} columns={columns}
                                                        getData={this.props.resources.getSentMessages}/>
                </div>
            </div>
        );
    }
});
