import React from 'react';
import {Link } from 'react-router'
import OperatorRestTableComponent from '../../../components/operatorRestTableComponent';

import Debug from 'debug';
let debug = new Debug("components:operators");

const columns = [
    {
        property: 'name',
        header: 'Name'
    }
];

export default React.createClass({
    render () {
        debug('render ', this.state);
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <div className="pull-right">
                        <Link to={'/admin/addOperator'}>
                            <button className="btn btn-xs btn-success">
                                Add Operator
                            </button>
                        </Link>
                    </div>
                    <h3 className="panel-title">Operators</h3>
                </div>
                <div className="panel-body">
                    <OperatorRestTableComponent {...this.props} pathName={"operator"} columns={columns}
                                                        getData={this.props.resources.getFilteredOperators}/>
                </div>
            </div>
        );
    }
});