import React from 'react';
import AdminPanelForm from './../components/adminPanelForm';
import Debug from 'debug';
let debug = new Debug("components:dashboardview");

export default React.createClass({

    render () {
        let {props} = this;

        debug('render ', this.state);
        debug('render ', this.props);

        return (
            <AdminPanelForm {...props} getMyallocatedShortCodeIds={props.resources.getMyallocatedShortCodeIds}/>);
    }
});