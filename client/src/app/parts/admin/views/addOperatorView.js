import React from 'react';
import Paper from 'material-ui/lib/paper';
import DocTitle from '../../../components/docTitle';
import OperatorForm from '../components/addOperatorForm';
import Debug from 'debug';
let debug = new Debug("views:addOperatorView");

export default React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    propTypes: {},

    render() {
        debug('render ', this.state);
        let {props} = this;
        let _this = this;

        return (
            <Paper className="text-center view">
                <div id="signup">
                    <DocTitle title="Add Operator|Livemonitor"/>
                    <h2>Add Operator</h2>
                    {props.operatoradd.data.success && _this.renderOperatorAddComplete() }
                    {!props.operatoradd.data.success && _this.renderOperatorForm() }
                </div>
            </Paper>
        );
    },

    renderOperatorAddComplete(){
        return (
            <div className="alert alert-info text-center" role="alert">
                The operator you added has been successfully created.
            </div>
        );
    },

    renderOperatorForm(){
        let {props} = this;
        return (<OperatorForm {...props}/>);
    }
});