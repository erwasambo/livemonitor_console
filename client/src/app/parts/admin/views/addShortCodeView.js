import React from 'react';
import DocTitle from '../../../components/docTitle';
import AddShortCodeForm from '../components/addShortCodeForm';
import Debug from 'debug';
let debug = new Debug("components:addShortCodeView");

export default React.createClass({
    propTypes: {},
    componentDidMount () {
        this.props.actions.getOperators()
    },
    render () {
        let {props} = this;
        return (
            <div id="profile">
                <DocTitle title="My Profile"/>
                <AddShortCodeForm {...props}/>
            </div>
        );
    }
});