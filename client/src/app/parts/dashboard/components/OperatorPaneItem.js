import React from 'react';
import {Link} from 'react-router';
import firebase from 'firebase';
import Spinner from '../../../components/spinner';
import $ from 'jquery';
require('jquery-sparkline/jquery.sparkline.min.js');
require('easy-pie-chart/dist/jquery.easypiechart.min.js');

export default React.createClass({

    getInitialState() {
        return {
            status: "Loading",
            timestampInMillis: 0,
            updateDate:  new Date(0),
            lastUpdated: "Out of Date",
            countClassNames : "chart stats-line",
            miniChartsClassNames: "mini-charts-item bgm-bluegray"
        }
    },

    componentDidMount () {

        /* Mini Chart - Bar Chart 1 */
        if ($('.stats-bar')[0]) {
            this.sparklineBar('stats-bar', [6, 4, 8, 6, 5, 6, 7, 8, 3, 5, 9, 5, 8, 4], '35px', 3, '#fff', 2);
        }

        /* Mini Chart - Line Chart 1 */
        if ($('.stats-line')[0]) {
            this.sparklineLine('stats-line', [9, 4, 6, 5, 6, 4, 5, 7, 9, 3, 6, 5], 68, 35, '#fff', 'rgba(0,0,0,0)', 1.25, 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.4)', 3, '#fff', 'rgba(255,255,255,0.4)');
        }
    },

    sparklineBar: function (id, values, height, barWidth, barColor, barSpacing) {
        $('.' + id).sparkline(values, {
            type: 'bar',
            height: height,
            barWidth: barWidth,
            barColor: barColor,
            barSpacing: barSpacing
        })
    },

    sparklineLine: function (id, values, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
        $('.' + id).sparkline(values, {
            type: 'line',
            width: width,
            height: height,
            lineColor: lineColor,
            fillColor: fillColor,
            lineWidth: lineWidth,
            maxSpotColor: maxSpotColor,
            minSpotColor: minSpotColor,
            spotColor: spotColor,
            spotRadius: spotRadius,
            highlightSpotColor: hSpotColor,
            highlightLineColor: hLineColor
        });
    },

    render: function () {
        let _this = this;
        let {state, props} = this;
        let shortCode = props.shortCode;
        // Attach an asynchronous callback to read the data at our posts reference
        firebase.database().ref('dashboarders/' + this.props.shortCode.id + '/').on("value", function (snapshot) {
            var status = snapshot.val().status;
            var timestampInMillis = snapshot.val().lastUpdated;
            var updateDate = new Date(timestampInMillis);
            var lastUpdated = updateDate.getDate() + "/" + (updateDate.getMonth() + 1) + "/" + updateDate.getFullYear() + " " + updateDate.getHours() + ":" + updateDate.getMinutes();
            var countClassNames = "chart stats-line";
            var miniChartsClassNames = "mini-charts-item bgm-bluegray";
            if(status == "Up"){
                countClassNames = "chart stats-bar";
                miniChartsClassNames = "mini-charts-item bgm-cyan";
            }else if(status == "Down"){
                countClassNames = "chart stats-line";
                miniChartsClassNames = "mini-charts-item bgm-red";
            }else if(status == "Loading"){
                countClassNames = "chart stats-line";
                miniChartsClassNames = "mini-charts-item bgm-bluegray";
            }
            _this.setState({status:status, timestampInMillis:timestampInMillis, updateDate:updateDate, lastUpdated:lastUpdated, countClassNames:countClassNames, miniChartsClassNames:miniChartsClassNames});
        });

        return (<Link to={`/shortcode/${shortCode.id}`}>
                    <div className="col-sm-3">
                        <div className={state.miniChartsClassNames}>
                            <div className="clearfix">
                                <div className={state.countClassNames}></div>
                                <div className="count">
                                    <small>{shortCode.operator}-{shortCode.name}</small>
                                    {state.status == "Loading" ? <Spinner/> : <h2>{state.status}</h2>}
                                </div>
                            </div>
                        </div>
                    </div>
                </Link>);
    }
});