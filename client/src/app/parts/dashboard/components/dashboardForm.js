import React from 'react';
import OperatorPaneItem from '../components/OperatorPaneItem'
import CircularProgress from 'material-ui/lib/circular-progress';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("components:dashboardForm");

export default React.createClass({
    propTypes: {
        dashboard: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            loading: false,
            dashboard: {}
        }
    },
    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.shortcodesGet.data || {});
        debug("shortcodes.data", JSON.stringify(nextProps.shortcodesGet.data));
    },
    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            ShortCodes: [],
            updating: false,
            completed: 0
        }
    },
    populateDashboardItems(_dashboardItems) {
        if (_dashboardItems && _dashboardItems.length == 0) {
            return (<Paper className='profile-view view'>
                <CircularProgress size={2} />
            </Paper>);
        } else {
            return (_dashboardItems);
        }
    },
    render() {
        var _dashboardItems = [];
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let _this = this;
        if (props.dashboard.loading) {
            return (<Paper className='profile-view view'>
                <CircularProgress size={2} />
            </Paper>);
        }
        state.ShortCodes.map((shortcode) => {
            _dashboardItems.push(<OperatorPaneItem key={shortcode.id} shortCode={shortcode}/>);
        });

        return (
            <div className="row">
                {_this.populateDashboardItems(_dashboardItems)}
            </div>
        );
    }
});