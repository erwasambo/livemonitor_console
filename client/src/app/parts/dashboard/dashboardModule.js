import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {connect} from 'react-redux';
import DashboardView from './views/dashboardView';

function Resources(rest) {
    return {
        getMyallocatedShortCodeIds() {
            return rest.get('me/shortCodes');
        }
    }
}

function Actions(rest) {
    let dashboard = Resources(rest);
    return {
        getMyallocatedShortCodeIds: createActionAsync('DASH_SHORT_CODE_GET', dashboard.getMyallocatedShortCodeIds)
    }
}

function Reducers(actions) {
    return {
        shortcodesGet: createReducerAsync(actions.getMyallocatedShortCodeIds)
    }
}

function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        dashboard(){
            const mapStateToProps = (state) => ({
                shortcodesGet: state.get('shortcodesGet').toJSON()
            });
            return connect(mapStateToProps, mapDispatchToProps)(DashboardView);
        }
    }
}

function Routes(containers) {
    return (
        <Route component={containers.dashboard()} path="dashboard"/>
    )
}

export default function (rest) {
    let actions = Actions(rest);
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}