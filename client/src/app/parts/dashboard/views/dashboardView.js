import React from 'react';
import DocTitle from 'components/docTitle';
import DashboardForm from '../components/dashboardForm';
import Debug from 'debug';
let debug = new Debug("components:dashboardview");

export default React.createClass({

    propTypes: {},

    componentDidMount () {
        this.props.actions.getMyallocatedShortCodeIds();
    },

    render () {
        let {props} = this;
        return (
            <section id="main">
                <section id="content">
                    <div className="container">
                        <DocTitle title="Dashboard|Livemonitor"/>
                        <div className="block-header">
                            <h2>All Short Codes Currrent Status
                                <small>This is gives you an overview of what is happening at our Short Codes</small>
                            </h2>
                        </div>
                        <div className="mini-charts">
                            <DashboardForm {...props}/>
                        </div>
                    </div>
                </section>
            </section>
        );
    }
})