import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {connect} from 'react-redux';
import UserView from './views/userView';

function Resources(rest) {
    return {
        getMyallocatedShortCodeIds() {
            return rest.get('me/shortCodes');
        },
        getShortCodes() {
            return rest.get('shortcodes/');
        },
        getUser(id) {
            return rest.get('users/' + id+'/shortCodes');
        },
        updateUser(payload) {
            return rest.put('users/' + payload.id, payload);
        },
        deleteUser(id) {
            return rest.del('users/' + id);
        }
    }
}

function Actions(rest) {
    let user = Resources(rest);
    return {
        getMyallocatedShortCodeIds: createActionAsync('USER_MY_ALLOCATED_SHORT_CODE_GET', user.getMyallocatedShortCodeIds),
        getShortCodes: createActionAsync('USER_SHORT_CODE_GET', user.getShortCodes),
        getUser: createActionAsync('USER_GET_BY_ID', user.getUser),
        updateUser: createActionAsync('USER_GET_UPDATE', user.updateUser),
        deleteUser: createActionAsync('USER_DELETE_IND', user.deleteUser)
    }
}

function Reducers(actions) {
    return {
        myshortcodesGet: createReducerAsync(actions.getMyallocatedShortCodeIds),
        shortcodes: createReducerAsync(actions.getShortCodes),
        user: createReducerAsync(actions.getUser),
        userUpdate: createReducerAsync(actions.updateUser),
        userDelete: createReducerAsync(actions.deleteUser)
    }
}

function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        user(){
            const mapStateToProps = (state) => ({
                myshortcodesGet: state.get('myshortcodesGet').toJSON(),
                shortcodes: state.get('shortcodes').toJSON(),
                user: state.get('user').toJS(),
                userUpdate: state.get('userUpdate').toJS(),
                userDelete: state.get('userDelete').toJS()
            });
            return connect(mapStateToProps, mapDispatchToProps)(UserView);
        }
    }
}

function Routes(containers) {
    return (<Route path="/user/:userId" component={containers.user()}/>)
}

export default function (rest) {
    let actions = Actions(rest);
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}