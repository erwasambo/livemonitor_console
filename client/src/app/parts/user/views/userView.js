import React from 'react';
import DocTitle from 'components/docTitle';
import UserForm from '../components/userForm';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("components:userView");

export default React.createClass({

    propTypes: {},

    componentDidMount () {
        var id = this.props.params.userId;
        this.props.actions.getUser(id);
        this.props.actions.getShortCodes();
    },

    render () {
        debug('render ', this.state);
        let {props} = this;
        let _this = this;
        return (
            <Paper className="text-center view">
                <DocTitle title="Edit User|Livemonitor"/>
                <h2>Edit User</h2>
                {props.userUpdate.data.success && _this.renderUserUpdateComplete()}
                {props.userDelete.data.success && _this.renderUserDeleteComplete()}
                {!props.userUpdate.data.success && !props.userDelete.data.success && _this.renderUserForm()}
            </Paper>
        );
    },
    renderUserUpdateComplete(){
        return (
            <div className="alert alert-info text-center" role="alert">
                The User you changed has been successfully updated.
            </div>
        );
    },
    renderUserDeleteComplete(){
        return (
            <div className="alert alert-info text-center" role="alert">
                The User you changed has been successfully deleted.
            </div>
        );
    },
    renderUserForm(){
        let {props} = this;
        return (<UserForm {...props}/>
        );
    }
})