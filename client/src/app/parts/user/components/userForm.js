import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import LaddaButton from 'react-ladda';
import CircularProgress from 'material-ui/lib/circular-progress';
import ValidateUserForm from '../../../services/validateUserForm';
import Table from 'material-ui/lib/table/table';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableHeader from 'material-ui/lib/table/table-header';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import TableBody from 'material-ui/lib/table/table-body';
import Debug from 'debug';
let debug = new Debug("components:userForm");

export default React.createClass({
    propTypes: {
        user: React.PropTypes.object,
        shortcodes: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            loading: false,
            user: {},
            shortcodes: {}
        }
    },

    componentWillUnmount: function() {
        this.setState({ShortCodes:[],
            allShortCodeIdsThereExists:[],
            allocatedShortCodeIds:[],
            newAllocatedShortCodeIds:[],
            tableData : []});
    },

    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.shortcodes.data || {});
        this.setState(nextProps.user.data || {});
    },

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            allShortCodeIdsThereExists:[],
            allocatedShortCodeIds:[],
            nonAllocatedShortCodeIds:[],
            //Now this is api allocated data
            ShortCodes:[],
            tableData : [],
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: true,
            selectable: true,
            multiSelectable: true,
            enableSelectAll: true,
            deselectOnClickaway: true,
            height: '300px',
            //Api load of all Shortcodes
            shortCodeData: [],
            open: false,
            language: 'US',
            updating: false,
            errors: {},
            completed: 0
        }
    },

    handleOpen(evt) {
        evt.preventDefault();
        this.setState({open: true});
    },

    handleClose(){
        this.setState({open: false});
    },

    handleDeleteClose(){
        this.setState({open: false});
        this.deleteUser();
    },

    renderUpdateError(){
        let {error} = this.props.userUpdate;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    renderDeleteError(){
        let {error} = this.props.userDelete;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    handleFirstNameChange: function (event) {
        this.setState({firstName: event.target.value});
    },

    handleLastNameChange: function (event) {
        this.setState({lastName: event.target.value});
    },

    handleLocationChange: function (event) {
        this.setState({location: event.target.value});
    },

    handleEmailChange: function (event) {
        this.setState({email: event.target.value});
    },

    handleRowSelection(indices) {
        var newTableData = [];
        var currentTableData = this.state.tableData;
        var newAllocatedShortCodeIds = [];
        this.setState({allocatedShortCodeIds: []});
        if (indices === 'all') {
            newAllocatedShortCodeIds = [];
            currentTableData.map((row, index) => {newAllocatedShortCodeIds.push(row.id)});
        } else if (indices === 'none') {
            newAllocatedShortCodeIds = [];
        } else {
            newAllocatedShortCodeIds = [];
            indices.map(index => {newAllocatedShortCodeIds.push(currentTableData[index].id)});
        }

        currentTableData.map((row, index) => {
            if(newAllocatedShortCodeIds.includes(row.id)){
                newTableData.push({id: row.id, name: row.name, operator: row.operator,selected: true});
            }else{
                newTableData.push({id: row.id, name: row.name, operator: row.operator,selected: false});
            }
        });

        this.setState({tableData: newTableData,newAllocatedShortCodeIds: newAllocatedShortCodeIds});
        //Clean new table data
        newTableData = [];
        newAllocatedShortCodeIds = [];
    },

    getDataLoaded: function () {

        //Now this is api allocated data
        this.state.ShortCodes.map((shortcode) => {
            if(!this.state.allocatedShortCodeIds.includes(shortcode.id)){
                this.state.allocatedShortCodeIds.push(shortcode.id);
            }
        });

        //Fill up Table Data and user specific short code selection
        this.state.shortCodeData.map((shortCodeData) => {
            if(!this.state.allShortCodeIdsThereExists.includes(shortCodeData.id)){
                this.state.allShortCodeIdsThereExists.push(shortCodeData.id);
                if(this.state.allocatedShortCodeIds.includes(shortCodeData.id)){
                    this.state.tableData.push({
                        id: shortCodeData.id,
                        name: shortCodeData.name,
                        operator: shortCodeData.operator,
                        selected: true
                    });
                }else{
                    this.state.tableData.push({
                        id: shortCodeData.id,
                        name: shortCodeData.name,
                        operator: shortCodeData.operator,
                        selected: false
                    });
                }
            }
        });
    },

    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;

        if (props.user.loading || props.shortcodes.loading) {
            return <CircularProgress size={2} />
        }

        this.getDataLoaded();

        return (
            <div className="local-login-form">
                <form
                    className="form-horizontal"
                    onSubmit={ (e) => e.preventDefault() }>
                    <div className="signup-options text-center form">
                        {_this.renderUpdateError()}
                        {_this.renderDeleteError()}
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        id='firstName'
                                        floatingLabelText="First Name"
                                        value={state.firstName}
                                        onChange={_this.handleFirstNameChange}
                                        errorText={errors.firstName && errors.firstName[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    id='lastName'
                                    floatingLabelText="Last Name"
                                    value={state.lastName}
                                    onChange={_this.handleLastNameChange}
                                    errorText={errors.lastName && errors.lastName[0]}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        id='location'
                                        floatingLabelText="Location"
                                        value={state.location}
                                        onChange={_this.handleLocationChange}
                                        errorText={errors.location && errors.location[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    id='email'
                                    value={state.email}
                                    floatingLabelText="Email"
                                    onChange={_this.handleEmailChange}
                                    errorText={errors.email && errors.email[0]}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <TextField
                                    id='username'
                                    floatingLabelText="Username"
                                    value={state.username}
                                    disabled={true}
                                    errorText={errors.username && errors.username[0]}
                                />
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    ref="id"
                                    hintText="User Id"
                                    floatingLabelText="User Id"
                                    value={state.id}
                                    disabled={true}
                                />
                            </div>
                        </div>

                        <div className='form-group name'>
                            <Table
                                height={state.height}
                                fixedHeader={state.fixedHeader}
                                fixedFooter={state.fixedFooter}
                                selectable={state.selectable}
                                multiSelectable={state.multiSelectable}
                                onRowSelection={_this.handleRowSelection}>
                                <TableHeader enableSelectAll={state.enableSelectAll}>
                                    <TableRow>
                                        <TableHeaderColumn colSpan="3" tooltip="Allocated Short Codes" style={{textAlign: 'center'}}>
                                            Allocated Short Codes
                                        </TableHeaderColumn>
                                    </TableRow>
                                    <TableRow>
                                        <TableHeaderColumn tooltip="Short Code">Short Code</TableHeaderColumn>
                                        <TableHeaderColumn tooltip="Operator">Operator</TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody
                                    deselectOnClickaway={false}
                                    showRowHover={state.showRowHover}
                                    stripedRows={state.stripedRows}>
                                    {state.tableData.map((row, index) => {
                                        return (
                                            <TableRow key={index} selected={row.selected}>
                                                <TableRowColumn>{row.name}</TableRowColumn>
                                                <TableRowColumn>{row.operator}</TableRowColumn>
                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </Table>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <LaddaButton
                                        className='btn btn-lg btn-primary'
                                        id='btn-login'
                                        buttonColor='blue'
                                        loading={props.userUpdate.loading}
                                        progress={.5}
                                        buttonStyle="slide-up"
                                        onClick={_this.updateUser}>Update</LaddaButton>
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <LaddaButton
                                        className='btn btn-lg btn-primary'
                                        id='btn-login'
                                        buttonColor='red'
                                        loading={props.userDelete.loading}
                                        progress={.5}
                                        buttonStyle="slide-up"
                                        onClick={_this.deleteUser}>Delete</LaddaButton>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </form>
            </div>
        );
    },

    onLanguage(language){
        debug("onLanguage: ", language);
        this.setState({language: language});
    },

    onChange(id, e) {
        debug(`onChange: ${id}: ${e.target.value}`);
        this.setState({[id]: e.target.value});
    },

    updateUser(evt) {
        evt.preventDefault();
        //debug('updateProfile ', this.state);
        this.setState({
            errors: {},
            updating: true
        });

        validateForm.call(this)
            .with(this)
            .then(save)
            .then(successNotification)
            .catch(Checkit.Error, (error) => this.setState({
                errors: error.toJSON()
            }))
            .catch(setErrors)
            .then(() => {
                this.setState({
                    updating: false
                });
            });

        function validateForm() {
            return new ValidateUserForm({
                username: this.state.username,
                email: this.state.email,
                location: this.state.location,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            })
                .execute();
        }

        function save() {
            return this.props.actions.updateUser(this.state);
        }

        function successNotification() {
        }

        function setErrors(e) {
            debug('setErrors: ', e);
        }
    },

    deleteUser() {
        debug('deleteUser ', this.state);
        if(this.state.username=="admin"){
            alert("You are not allowed to delete Admin account")
        }else {
            this.setState({
                errors: {},
                deleting: true
            });

            deleteUsr.call(this)
                .with(this)
                .then(successNotification)
                .catch(Checkit.Error, (error) => this.setState({
                    errors: error.toJSON()
                }))
                .catch(setErrors)
                .then(() => {
                    this.setState({
                        deleting: false
                    });
                });

            function deleteUsr() {
                return this.props.actions.deleteUser(this.state.id);
            }

            function successNotification() {
            }

            function setErrors(e) {
                debug('setErrors: ', e);
                if (error instanceof Checkit.Error) {
                    this.setState({errors: error.toJSON()})
                }
            }
        }
    }
});

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}