import React from 'react';
import DocTitle from 'components/docTitle';
import AddShortCodeForm from '../components/addShortCodeForm';
import Snackbar from 'material-ui/lib/snackbar';
import Paper from 'material-ui/lib/paper';
import Debug from 'debug';
let debug = new Debug("components:addShortCodeView");

export default React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    propTypes: {},

    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            autoHideDuration: 4000,
            message: 'This ShortCode has been created successfully.',
            open: false
        }
    },

    handleTouchTap ()  {
        this.setState({
            open: true
        });
    },

    handleActionTouchTap (){
        this.setState({
            open: false
        });
       // alert('Event removed from your calendar.');
    },

    handleRequestClose () {
        this.setState({
            open: false,
        });
    },

    componentDidMount () {
        this.props.actions.getOperators()
    },

    render() {
        let {props} = this;
        let _this = this;
        return (
            <Paper className="text-center view">
                <DocTitle title="Add Short Code |Livemonitor"/>
                <h2>Add Short Code</h2>
                {props.shortcodeadd.data.success && _this.renderShortCodeAddComplete() }
                {!props.shortcodeadd.data.success && _this.renderShortCodeForm() }
            </Paper>
        );
    },

    renderShortCodeAddComplete(){
        this.handleTouchTap();
        return (
        <div className="local-login-form">
            <div className="alert alert-info text-center" role="alert">
                The ShortCode you added has been successfully created.
            </div>
        </div>
        );
    },

    renderShortCodeForm(){
        let {state,props} = this;
        return (
            <div>
            <AddShortCodeForm {...props}/>
                <Snackbar
                    open={state.open}
                    message={state.message}
                    action="undo"
                    autoHideDuration={state.autoHideDuration}
                    onActionTouchTap={this.handleActionTouchTap}
                    onRequestClose={this.handleRequestClose}
                />
                </div>);
    }
});