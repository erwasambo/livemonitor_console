import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import LaddaButton from 'react-ladda';
import CircularProgress from 'material-ui/lib/circular-progress';
import Debug from 'debug';
let debug = new Debug("components:addshortcodeForm");

export default React.createClass({
    propTypes: {
        addshortcode: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            loading: false,
            addshortcode: {}
        }
    },
    getInitialState() {
        debug("getInitialState: props: ", this.props);
        return {
            operator: "",
            operatorId: "",
            count: 0,
            operatorData: [],
            errors: {},
            updating: false,
            loading: false,
            operators: {},
            completed: 0
        }
    },

    componentWillReceiveProps(nextProps){
        debug("componentWillReceiveProps", nextProps);
        this.setState(nextProps.shortcodeadd.data || {});
        this.setState(nextProps.operatorsget.data || {});
    },

    handleChange: function (event, index, operatorId) {
        let selectedOperator = this.state.operatorData[index];
        this.setState({value: selectedOperator.id, operatorId: selectedOperator.id, operator: selectedOperator.name});
    },

    renderError(){
        let {error} = this.props.shortcodeadd;
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },

    render() {
        debug("render props: ", this.props);
        debug("state: ", this.state);
        let {state, props} = this;
        let {errors} = state;
        let _this = this;

        if (props.operatorsget.loading) {
            return <CircularProgress size={2} />
        }

        return (
        <div className="local-login-form">
                <form className="form-horizontal">
                    <div className="signup-options text-center form">
                        {_this.renderError()}
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="name"
                                        hintText="Name"
                                        floatingLabelText="Name"
                                        errorText={errors.name && errors.name[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <TextField
                                    ref="description"
                                    hintText="Description"
                                    floatingLabelText="Description"
                                    errorText={errors.description && errors.description[0]}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="deviceNumber"
                                        hintText="Device Number"
                                        floatingLabelText="Device Number"
                                        errorText={errors.deviceNumber && errors.deviceNumber[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                                <SelectField
                                    ref="operator"
                                    hintText="Operator"
                                    floatingLabelText="Operator"
                                    onChange={_this.handleChange}
                                    value={state.value}
                                    errorText={state.errors.operator && state.errors.operator[0]}>
                                    {state.operatorData.map((operator) => {
                                        return (<MenuItem key={operator.id} value={operator.id} primaryText={operator.name}/>);
                                    })}
                                </SelectField>
                            </div>
                        </div>

                        <div className="row">
                            <div className="form-group col-md-6">
                                <div className="col-sm-5 col-md-4 col-lg-3">
                                    <TextField
                                        ref="email"
                                        hintText="Owner Email"
                                        floatingLabelText="Owner Email"
                                        errorText={errors.email && errors.email[0]}
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-6">
                            </div>
                        </div>
                        <div className='btn-signup'>
                            <LaddaButton
                                className='btn btn-lg btn-primary'
                                id='btn-login'
                                buttonColor='green'
                                loading={props.shortcodeadd.loading}
                                progress={.5}
                                buttonStyle="slide-up"
                                onClick={this.addShortCode}>Create Short Code</LaddaButton>
                        </div>
                    </div>
                </form>
        </div>);
    },

    addShortCode(evt) {
        evt.preventDefault();
        let {name, description, deviceNumber, email} = this.refs;
        let operatorId = JSON.stringify(this.state.operatorId);
        let operator = this.state.operator;
        let payload = {
            name: name.getValue(),
            description: description.getValue(),
            deviceNumber: deviceNumber.getValue(),
            operatorId: operatorId,
            operator: operator,
            email: email.getValue()
        };
        return validateShortCode.call(this, payload)
            .with(this)
            .then(this.props.actions.addShortCode)
            .catch(setErrors);

    }
});

function validateShortCode(payload) {
    let rules = new Checkit({
        name: ['required', 'numeric'],
        description: ['minLength:6', 'maxLength:64'],
        operatorId: ['required'],
        operator: ['required'],
        deviceNumber: ['alphaNumeric', 'minLength:6', 'maxLength:64'],
        email: ['required', 'email', 'minLength:6', 'maxLength:64']
    });

    return rules.run(payload);
}

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}