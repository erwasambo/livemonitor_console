import React from 'react';
import {Route} from 'react-router';
import {bindActionCreators} from 'redux';
import {createActionAsync, createReducerAsync} from 'redux-act-async';
import {connect} from 'react-redux';
import AddShortCodeView from './views/addShortCodeView';

function Resources(rest) {
    return {
        getOperators() {
            return rest.get('operators/');
        },
        addShortCode(payload) {
            return rest.post('shortcodes/', payload);
        }
    }
}

function Actions(rest) {
    let addshortcode = Resources(rest);
    return {
        getOperators: createActionAsync('OPERATORS_GET_ADD_SHORT_CODE', addshortcode.getOperators),
        addShortCode: createActionAsync('SHORT_CODE_ADD_SHORT_CODE', addshortcode.addShortCode)
    }
}

function Reducers(actions) {
    return {
        operatorsget: createReducerAsync(actions.getOperators),
        shortcodeadd: createReducerAsync(actions.addShortCode)
    }
}
function Containers(actions) {
    const mapDispatchToProps = (dispatch) => ({actions: bindActionCreators(actions, dispatch)});
    return {
        addshortcode(){
            const mapStateToProps = (state) => ({
                operatorsget: state.get('operatorsget').toJS(),
                shortcodeadd: state.get('shortcodeadd').toJS()
            });
            return connect(mapStateToProps, mapDispatchToProps)(AddShortCodeView);
        }
    }
}

function Routes(containers) {
    return (
        <Route component={containers.addshortcode()} path="newshortcode"/>
    )
}

export default function (rest) {
    let actions = Actions(rest);
    let containers = Containers(actions);
    let routes = Routes(containers);
    return {
        actions,
        reducers: Reducers(actions),
        containers,
        routes
    }
}