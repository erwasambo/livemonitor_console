import React from 'react';
import Checkit from 'checkit';
import TextField from 'material-ui/lib/text-field';
import LaddaButton from 'react-ladda';
import Debug from 'debug';
let debug = new Debug("components:localLoginForm");

export default React.createClass({
    propTypes: {},
    getInitialState() {
        return {
            errors: {}
        };
    },
    renderError(){
        let {error} = this.props.login;
        if (this.props.login.error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>Username</strong> and <strong>Password</strong> do not match
                </div>
            )
        }
        if (error) {
            return (
                <div className="alert alert-danger text-center" role="alert">
                    <strong>{error.name}</strong>
                    <strong>{error.statusText} </strong>
                    <strong>{error.status}</strong>
                </div>
            )
        }
    },
    render() {
        debug('render state:', this.state);
        debug('render props:', this.props);
        let {errors} = this.state;
        return (
            <div className="local-login-form">
                <form>
                    <div className="signup-options text-center form">
                        {this.renderError()}
                        <div className='form-group username'>
                            <TextField
                                ref="username"
                                hintText='Username'
                                floatingLabelText="Username"
                                errorText={errors.username && errors.username[0]}
                            />
                        </div>
                        <div className='form-group password'>
                            <TextField
                                id='password'
                                ref="password"
                                hintText="Password"
                                floatingLabelText="Password"
                                type='password'
                                errorText={errors.password && errors.password[0]}
                            />
                        </div>

                        <div>
                            <LaddaButton
                                className='btn btn-lg btn-primary'
                                id='btn-login'
                                buttonColor='green'
                                loading={this.props.login.loading}
                                progress={.5}
                                buttonStyle="slide-up"
                                onClick={this.login}>Login</LaddaButton>
                        </div>
                    </div>
                </form>
            </div>
        );
    },

    login(evt) {
        evt.preventDefault();
        let {username, password} = this.refs;
        let payload = {
            username: username.getValue(),
            password: password.getValue()
        };

        debug('login payload:', JSON.stringify(payload));

        return validateLogin.call(this, payload)
            .with(this)
            .then(this.props.actions.login)
            .catch(setErrors);
    }

});

function validateLogin(payload) {
    let rules = new Checkit({
        username: ['required', 'alphaDash', 'minLength:3', 'maxLength:64'],
        password: ['required', 'alphaDash', 'minLength:6', 'maxLength:64'],
    });

    return rules.run(payload);
}

function setErrors(error) {
    debug("setErrors", error);
    if (error instanceof Checkit.Error) {
        this.setState({errors: error.toJSON()})
    }
}