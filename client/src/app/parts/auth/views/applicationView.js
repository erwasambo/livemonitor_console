import React from 'react';
import NavBar from '../../core/components/navbar';

export default React.createClass({
    componentWillMount () {
        this.props.actions.me();
    },
    render() {
        return (
            <div id="application-view">
                <NavBar authenticated={this.props.authenticated}/>
                <div id='main-container' className="container">
                    {this.props.children}
                </div>
            </div>
        );
    }
});