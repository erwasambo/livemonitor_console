import _ from 'lodash';
let log = require('logfilename')(__filename);

export default function PivotDataApi(app) {
  let models = app.data.models();
  let validateJson = app.utils.api.validateJson;

  return {
    createPivotData: async function (shortCodeIn) {
      var recordPreExists = false;
      validateJson(shortCodeIn, require('./schema/createPivotData.json'));
      log.debug("createPivotData: ", shortCodeIn);

      //Look out for duplicates
      let pivotData = await models.PivotData.findAll({
        where: {sentMessageId: shortCodeIn.sentMessageId, date: shortCodeIn.date}
      });

      if(pivotData) {
        for (let pivot of pivotData) {
          let delayedAmount = pivot.delayedAmount;
          let failedAmount = pivot.failedAmount;
          let successfulAmount = pivot.successfulAmount;
          //"delayedAmount":"1","failedAmount":"0","successfulAmount":"0"
          if((shortCodeIn.delayedAmount == delayedAmount) && delayedAmount==1){
            log.debug("duplicate not recorded: ", shortCodeIn);
            recordPreExists = true;
          }else if((shortCodeIn.failedAmount == failedAmount) && failedAmount==1){
            log.debug("duplicate not recorded: ", shortCodeIn);
            recordPreExists = true;
          }else if((shortCodeIn.successfulAmount == successfulAmount) && successfulAmount==1){
            log.debug("duplicate not recorded: ", shortCodeIn);
            recordPreExists = true;
          }
        }

        if(!recordPreExists){
          let result = await models.PivotData.create(shortCodeIn);
          log.debug("if(!recordPreExists){", shortCodeIn);
        }
      }else{
        log.debug("else of if(pivotData) {", shortCodeIn);
        let result = await models.PivotData.create(shortCodeIn);
      }

      return {
        success: true,
        message: "success in pivot data creation"
      };
    },
    isIterable: async function (obj) {
      // checks for null and undefined
      if (obj == null) {
        return false;
      }
      return typeof obj[Symbol.iterator] === 'function';
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 100000000000000000,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.PivotData.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let pivotDatas = await _.map(result.rows, pivotData => pivotData.toJSON());
      log.info('getAll: async function pivotDatas: '+JSON.stringify(pivotDatas));
      return {
        count: result.count,
        pivotData: pivotDatas
      };
    },
    getOne: async function (pivotDataShortCodeId) {
      log.debug("get pivotDataId: ", pivotDataShortCodeId);
      let result = await models.PivotData.findAndCountAll({where: {shortCodeId: pivotDataShortCodeId}});
      log.info(`getOne: async function count: ${result.count}`);
      let pivotDatas = await _.map(result.rows, pivotData => pivotData.toJSON());
      log.info('getOne: async function pivotDatas: '+JSON.stringify(pivotDatas));
      return {
        count: result.count,
        pivotData: pivotDatas
      };
    }
  };
}