import Router from 'koa-66';
import Qs from 'qs';

let log = require('logfilename')(__filename);

export function PivotDataHttpController(app, pivotDataApi){
  log.debug("PivotDataHttpController");

  let respond = app.utils.http.respond;
  return {
    async createPivotData(context){
      return respond(context, pivotDataApi, pivotDataApi.createPivotData, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        pivotDataApi,
        pivotDataApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let pivotDataId = context.params.id;
      return respond(context, pivotDataApi, pivotDataApi.getOne, [pivotDataId]);
    }
  };
}

export default function PivotDataRouter(app, pivotDataApi){
  let router = new Router();
  let pivotDataHttpController = PivotDataHttpController(app, pivotDataApi);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', pivotDataHttpController.createPivotData);
  router.get('/', pivotDataHttpController.getAll);
  router.get('/:id', pivotDataHttpController.getOne);

  app.server.baseRouter().mount("/pivotData", router);
  return router;
}