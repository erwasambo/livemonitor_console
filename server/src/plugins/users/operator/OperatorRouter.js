import Router from 'koa-66';
import Qs from 'qs';
import OperatorApi from './OperatorApi';

let log = require('logfilename')(__filename);

export function OperatorHttpController(app, publisherOperator){
  log.debug("OperatorHttpController");
  let operatorApi = OperatorApi(app, publisherOperator);
  let respond = app.utils.http.respond;
  return {
    async addOperator(context){
      return respond(context, operatorApi, operatorApi.createOperator, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        operatorApi,
        operatorApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let operatorId = context.params.id;
      return respond(context, operatorApi, operatorApi.getOne, [operatorId]);
    },
    async put(context) {
      let operatorId = context.params.id;
      return respond(context, operatorApi, operatorApi.put, [operatorId, context.request.body]);
    },
    async delete(context) {
      let operatorId = context.params.id;
      return respond(context, operatorApi, operatorApi.delete, [operatorId]);
    }
  };
}

export default function OperatorRouter(app, publisherOperator){
  let router = new Router();
  let operatorHttpController = OperatorHttpController(app, publisherOperator);

  //TODO::ENABLE AUTHORIZATION LATER
/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', operatorHttpController.addOperator);
  router.get('/', operatorHttpController.getAll);
  router.get('/:id', operatorHttpController.getOne);
  router.put('/:id', operatorHttpController.put);
  router.delete('/:id', operatorHttpController.delete);

  app.server.baseRouter().mount("/operators", router);
  return router;
}