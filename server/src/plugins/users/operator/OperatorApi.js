import _ from 'lodash';
import Log from 'logfilename';
import firebase from 'firebase';
// Initialize Firebase
var app = firebase.initializeApp({
  apiKey: "AIzaSyA6nfru2V9ILabQw8c3GQuB3SVzyR9pZtw",
  authDomain: "shaqodoonlivemonitor.firebaseapp.com",
  databaseURL: "https://shaqodoonlivemonitor.firebaseio.com",
  storageBucket: "shaqodoonlivemonitor.appspot.com",
  serviceAccount: {
    "type": "service_account",
    "project_id": "shaqodoonlivemonitor",
    "private_key_id": "415e64d34aafcb602b57daea7b06ce963a9c2b04",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDtH2vdwf2xjHOK\nKbvBgh7MzzYg/r/KaDTwuJokkev95j3J+ay3TvnhChGXHa0RSI29ENr6YvrGlQFe\nK3Os1nzPsQdRXnMmnJC2l9BvpI95c3v0Q20fx32kvbcXjR4n/6YduEWzDoCopiuf\nxJrdys0g3o+2zEK/tGEEepPvAZYu2jmD0WzGcwx0K9XWNIGyuWzvNYulVoqEMZS/\n+vTE0nMR35ViF/vB8hBDRXDdRdvzffHvSizEC9iVohR9fi0XnE7n5QdwH9fWd7ny\nqwk3Y8DK/QzwxaWV4l296YkW9bimmK7+e1XuxcqzkJd1Ja9MEi0cMLOA0OUW+W8+\n1To8khPJAgMBAAECggEALKipURezJxhTvmbDxNsz39D62dYA2ycrKVcrOM3+y0/s\nvTh3zgJiz35s97AQI1wtzbj59QRlRLdVjZ3TaG7xDNDDlxCZ0WDVZH5NNGmi1/Xn\nF4cHa1/QCxHsBNSAJHpil+BXyDRoCNtbBfkqTbFfVtxz5rKrs51OVGCdsfrzhRkZ\ns8mlqZozI2+BbTOMUSmpio4RpQROpkgCg7scVBHcfVagXjNoaP0V92En25oieUBB\nHbVAlEYCC2RAdmVhqehpp8gZa95B3Z9tRTbyFAm7SspGWY7ki5LHCoTdHKE8ljgi\nzGkAyj8iNZD+ibyqm/rdYFGs5nk1LGdPbwNTR4WHAQKBgQD/hkBIQ0jVD8Wthqft\n1HpJYzDnfM/7Zivo9t6aWGT5lJIGF/eVu8NCd/iuaxpH1W+r8Fx85W4qphpxeZA8\nhiY0hW6F/3eFEtlyiVxby+oSFQKYm6fv1MYH2dlur2+Imtro+1vYOFDIlfys8sYj\nBlvPRGEtllcDx+txVm9G5on3KQKBgQDtkGcHstmlGKQx9g3s5HLPQMeZCiRMJ5yA\nDJutjDgJEmIeoXguLrhgZiqoQXjAnldllZm+hbBxaBaVVXv9e8ky1WlvVZO7xplN\nrrNYgjCrRyEcn9bLyrTzvZyrTttCvo0IbsU0GXlKxpaBpp+WYVmBA9zFkMAdYaDL\n5VAazvProQKBgBNAwFMxjqlPGOvsO3b2DYi+Co9Gnj0pA6H4B+nFQ0Zy5VXENIl5\nrkqx03EA7OgAsvtXjwByIQzhmxh1umJ1ESff7eX2y8ChHPU+WFEqWiamop1xDQRL\nGei5h2KJoXO/h0MKvW7bP2r9sh4t5LP65KwHUPMoYsIMLDdL9fJ3qGmRAoGBANlN\nDCFLpK/SkWat5mD5Old/rtO7JiaO2SV74u7lYuBRc3UHR/IehEKRUZWkhbrTW6gt\nJnvSt+mUR4wDrK3aC9k2wwG26rKJX/NhyOU9+RzCrHQNdSHmYozNtUAxmng76q96\n+2HDp2IRstwXOMUKeR99E/WXiVOW2MmJETPkd9PhAoGADQFo4nApO9TLOdX7P+0e\n4xzrSRNLJQQFd78D9hpgoPm9VQbWeIhk2UtVV0gQFpnYtLor8u93PaCHMh2Jes1P\nGyYDWJuud2CQlHPSqDHhtpH7agKUvm5UGU/UM/D8n1P2eNmI67mKZ7ScrG1dV/DX\n5vQuBkU7eV924rG15RUImck=\n-----END PRIVATE KEY-----\n",
    "client_email": "shaqodoonlivemonitor@appspot.gserviceaccount.com",
    "client_id": "113787128728006224028",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/shaqodoonlivemonitor%40appspot.gserviceaccount.com"
  }
});

// Get a reference to the database service
var database = app.database();

export default function OperatorApi(app, publisherOperator) {
  let models = app.data.models();
  let log = new Log(__filename);
  let validateJson = app.utils.api.validateJson;

  //Now publish data to firebase
  async function writeOperatorDataToFirebase(operatorId, payload) {

    database.ref('operators/' + operatorId).set({
      "id": Number(operatorId),
      "name": payload.name
    });

    var operatorIdsRef = database.ref('operatorIds/');
    // sync down from server
    var list = [];
    operatorIdsRef.on('value', function (snap) {
      list = snap.val();
    });
    // time to remove 'bar'!
    // this is the correct way to change an array
    if (list) {
      list.push(Number(operatorId));
      operatorIdsRef.set(list);
    } else {
      operatorIdsRef.set([Number(operatorId)]);
    }
  }

  async function updateOperatorDataToFirebase(operatorId, payload) {

    database.ref('operators/' + operatorId).update({
      "id": Number(operatorId),
      "name": payload.name
    });

    var operatorIdsRef = database.ref('operatorIds/');
    // sync down from server
    var list = [];
    operatorIdsRef.on('value', function (snap) {
      list = snap.val();
    });
    // time to remove 'bar'!
    // this is the correct way to change an array
    if (list) {
      list.push(Number(operatorId));
      operatorIdsRef.update(list);
    } else {
      operatorIdsRef.update([Number(operatorId)]);
    }
  }

  async function deleteOperatorDataFromFirebase(operatorId) {
    database.ref('operators/' + operatorId).remove();

    var operatorIdsRef = database.ref('operatorIds/');
    // sync down from server
    var list = [];
    var updatedList = [];
    var i;
    operatorIdsRef.on('value', function (snap) {
      list = snap.val();
    });
    // time to remove 'bar'!
    // this is the correct way to change an array
    if (list) {
      for(i=0; i<=list.length; i++){
        if(list[i]==Number(operatorId)){
          //don't add it to the new list
        }else{
          updatedList.push(Number(list[i]));
        }
      }
      operatorIdsRef.update(updatedList);
    }
  }

  return {
    createOperator: async function (operatorIn) {
      validateJson(operatorIn, require('./schema/createOperator.json'));
      log.debug("createOperator: ", operatorIn);
      let operator = await models.Operator.findByName(operatorIn.name);
      if (!operator) {
        let savedOperator = await models.Operator.createOperator(operatorIn);
        writeOperatorDataToFirebase(savedOperator.id, operatorIn);
        //return savedOperator.toJSON();
      } else {
        log.info("already created", operatorIn.name);
      }
      return {
        success: true,
        message: "success in operator creation"
      };
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.Operator.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let operators = _.map(result.rows, operator => operator.toJSON());
      return {
        count: result.count,
        operatorData: operators
      };
    },
    getOne: async function (operatorId) {
      log.debug("get operatorId: ", operatorId);
      return models.Operator.findByOperatorId(operatorId);
    },
    put: async function (operatorId, data) {
      validateJson(data, require('./schema/createOperator.json'));
      log.debug("put operatorId %s, data: ", operatorId, data);
      await models.Operator.update({
        name: data.name
      }, {
        where: {
          id: operatorId
        }
      });

      updateOperatorDataToFirebase(operatorId, data);
      log.debug("put done");
      return {
        success: true,
        message: "success in shortCode creation"
      };
    },
    delete: async function (operatorId) {
      log.debug("delete operatorId: ", operatorId);
      await models.Operator.destroy({
        where: {
          id: operatorId //this will be your id that you want to delete
        }
      }).then(function (rowDeleted) { // rowDeleted will return number of rows deleted
        if (rowDeleted === 1) {
          log.debug('Deleted successfully');
        }
      }, function (err) {
        log.debug(err);
      });

      deleteOperatorDataFromFirebase(operatorId);
      return {
        success: true,
        message: "success in shortCode creation"
      };
    }
  };
}