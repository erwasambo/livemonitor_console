import _ from 'lodash';
import Log from 'logfilename';

export default function UserApi(app) {
  let models = app.data.models();
  let log = new Log(__filename);
  let validateJson = app.utils.api.validateJson;
  return {
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.User.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let users = _.map(result.rows, user => user.toJSON());
      return {
        count: result.count,
        userData: users
      };
    },

    getOne: async function (userId) {
      log.debug("get userId: ", userId);
      return await models.User.findByUserId(userId);
    },

    getShortCodes: async function (userId) {
      log.debug("get userId: ", userId);
      return await models.User.getShortCodes(userId);
    },

    put: async function (userId, data) {
      validateJson(data, require('./schema/put.json'));
      log.debug("put userId %s, data: ", userId, data);
      await models.User.updateUser(userId,data);
      return {
        success: true,
        message: "success in shortCode creation"
      };
    },

    delete: async function (userId) {
      log.debug("delete userId: ", userId);
      await models.User.deleteUser(userId);
      return {
        success: true,
        message: "success in shortCode creation"
      };
    }
  };
}