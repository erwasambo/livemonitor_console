import Router from 'koa-66';
import Qs from 'qs';
import UserApi from './UserApi';

let log = require('logfilename')(__filename);

export function UserHttpController(app, publisherUser){
  log.debug("UserHttpController");
  let userApi = UserApi(app, publisherUser);
  let respond = app.utils.http.respond;
  return {
    async getAll(context) {
      return respond(
        context,
        userApi,
        userApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let userId = context.params.id;
      return respond(context, userApi, userApi.getOne, [userId]);
    },
    async getShortCodes(context) {
      let userId = context.params.id;
      return respond(context, userApi, userApi.getShortCodes, [userId]);
    },
    async put(context) {
      let userId = context.params.id;
      return respond(context, userApi, userApi.put, [userId, context.request.body]);
    },
    async delete(context) {
      let operatorId = context.params.id;
      return respond(context, userApi, userApi.delete, [operatorId]);
    }
  };
}

export default function UserRouter(app, publisherUser){
  let router = new Router();
  let userHttpController = UserHttpController(app, publisherUser);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.get('/', userHttpController.getAll);
  router.get('/:id/shortCodes', userHttpController.getShortCodes);
  router.get('/:id', userHttpController.getOne);
  router.put('/:id', userHttpController.put);
  router.delete('/:id', userHttpController.delete);

  app.server.baseRouter().mount("/users", router);
  return router;
}