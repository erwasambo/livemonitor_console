import Router from 'koa-66';
import Qs from 'qs';
import ShortCodeApi from './ShortCodeApi';

let log = require('logfilename')(__filename);

export function ShortCodeHttpController(app, publisherShortCode){
  log.debug("ShortCodeHttpController");
  let shortCodeApi = ShortCodeApi(app, publisherShortCode);
  let respond = app.utils.http.respond;
  return {
    async addShortCode(context){
      return respond(context, shortCodeApi, shortCodeApi.createShortCode, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        shortCodeApi,
        shortCodeApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let shortCodeId = context.params.id;
      return respond(context, shortCodeApi, shortCodeApi.getOne, [shortCodeId]);
    },
    async put(context) {
      let userId = context.params.id;
      return respond(context, shortCodeApi, shortCodeApi.put, [userId, context.request.body]);
    },
    async delete(context) {
      let userId = context.params.id;
      return respond(context, shortCodeApi, shortCodeApi.delete, [userId]);
    }
  };
}

export default function ShortCodeRouter(app, publisherShortCode){
  let router = new Router();
  let shortCodeHttpController = ShortCodeHttpController(app, publisherShortCode);
  //TODO::ENABLE AUTHORIZATION LATER
/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', shortCodeHttpController.addShortCode);
  router.get('/', shortCodeHttpController.getAll);
  router.get('/:id', shortCodeHttpController.getOne);
  router.put('/:id', shortCodeHttpController.put);
  router.delete('/:id', shortCodeHttpController.delete);

  app.server.baseRouter().mount("/shortcodes", router);
  return router;
}