import _ from 'lodash';
import Log from 'logfilename';
import firebase from 'firebase';
// DO not Initialize Firebase for it has already been initialized somewhere
// Get a reference to the database service
var database = firebase.database();

export default function ShortCodeApi(app, publisherShortCode) {
  let models = app.data.models();
  let log = new Log(__filename);
  let validateJson = app.utils.api.validateJson;

  //Now publish data to firebase
  async function writeOperatorDataToFirebase(shortcodeId, payload) {
    database.ref('shortcodes/' + shortcodeId).set({
      "id": Number(shortcodeId),
      "description": payload.description,
      "deviceNumber": payload.deviceNumber,
      "name": Number(payload.name),
      "operatorId": Number(payload.operatorId),
      "operator": payload.operator,
      "email": payload.email
    });

    database.ref('dashboarders/' + shortcodeId).set({
      "id": Number(shortcodeId),
      "lastUpdated": 1463301426129,
      "shortCodeId": Number(shortcodeId),
      "status": "Down"
    });

    var shortCodeIdsRef = database.ref('shortCodeIds/');
    // sync down from server
    var list = [];
    shortCodeIdsRef.on('value', function (snap) {
      list = snap.val();
    });
    // time to remove 'bar'!
    // this is the correct way to change an array
    if (list) {
      list.push(Number(shortcodeId));
      shortCodeIdsRef.set(list);
    } else {
      shortCodeIdsRef.set([Number(shortcodeId)]);
    }
  }

  //Now publish data to firebase
  async function updateShortCodeDataToFirebase(shortcodeId, payload) {

    var shortCodeIdsRef = database.ref('shortCodeIds/');
    // sync down from server
    var list = [];
    shortCodeIdsRef.on('value', function (snap) {
      list = snap.val();
    });
    // time to remove 'bar'!
    // this is the correct way to change an array
    if (list) {
      list.push(Number(shortcodeId));
      shortCodeIdsRef.update(list);
    } else {
      shortCodeIdsRef.update([Number(shortcodeId)]);
    }
  }

  //Now publish data to firebase
  async function deleteShortCodeDataFromFirebase(shortcodeId) {
    database.ref('shortcodes/' + shortcodeId).remove();
    database.ref('dashboarders/' + shortcodeId).remove();

    var shortCodeIdsRef = database.ref('shortCodeIds/');
    // sync down from server
    var list = [];
    var updatedList = [];
    var i;
    shortCodeIdsRef.on('value', function (snap) {
      list = snap.val();
    });
    // time to remove 'bar'!
    // this is the correct way to change an array
    if (list) {
      for(i=0; i<=list.length; i++){
        if(list[i]==Number(shortcodeId)){
          //don't add it to the new list
        }else{
          updatedList.push(Number(list[i]));
        }
      }
      shortCodeIdsRef.update(updatedList);
    }
  }

  return {
    createShortCode: async function (shortCodeIn) {
      validateJson(shortCodeIn, require('./schema/createShortCode.json'));
      log.debug("createShortCode: ", shortCodeIn);
      //TODO::See how you can implement already created later
      let result = await models.ShortCode.createShortCodeInOperator(shortCodeIn);
      writeOperatorDataToFirebase(result.id, shortCodeIn);
      //return savedShortCode.toJSON();
      return {
        success: true,
        message: "success in shortCode creation"
      };
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 100000,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.ShortCode.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let shortCodes = _.map(result.rows, shortCode => shortCode.toJSON());
      return {
        count: result.count,
        shortCodeData: shortCodes
      };
    },
    getOperator: async function (shortCodeId) {
      log.debug("getUsers: ", shortCodeId);
      return await models.ShortCode.getOperator(shortCodeId);
    },
    getUsers: async function (shortCodeId) {
      log.debug("getUsers: ", shortCodeId);
      return await models.ShortCode.getUsers(shortCodeId);
    },
    getOne: async function (shortCodeId) {
      log.debug("get shortCodeId: ", shortCodeId);
      return await models.ShortCode.findByShortCodeId(shortCodeId);
    },
    put: async function (shortCodeId, data) {
      validateJson(data, require('./schema/editShortCode.json'));
      log.debug("patch shortCodeId %s, data: ", shortCodeId, data);
      await models.ShortCode.update({
        description: data.description,
        deviceNumber: data.deviceNumber,
        email: data.email
      }, {
        where: {
          id: shortCodeId
        }
      });
      await updateShortCodeDataToFirebase(shortCodeId, data);
      log.debug("put done");
      return {
        success: true,
        message: "success in shortCode creation"
      };
    },

    delete: async function (shortCodeId) {
      log.debug("delete shortCodeId: ", shortCodeId);
      await models.ShortCode.destroy({
        where: {
          id: shortCodeId //this will be your id that you want to delete
        }
      }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
        if(rowDeleted === 1){
          log.debug('Deleted successfully');
        }
      }, function(err){
        log.debug(err);
      });

      await deleteShortCodeDataFromFirebase(shortCodeId);

      return {
        success: true,
        message: "success in shortCode creation"
      };
    }
  };
}