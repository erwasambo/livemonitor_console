import Promise from 'bluebird';
import Log from 'logfilename';
import {Publisher} from 'rabbitmq-pubsub';

import PassportAuth from './PassportAuth';

import config from 'config';

// Jobs
import MailJob from './jobs/mail/MailJob';

import ShortCodeRouter from './shortcode/ShortCodeRouter';

let log = new Log(__filename);

const publisherOption = { exchange: "shortCode" };

export default function ShortCodePlugin(app){

  app.data.registerModelsFromDir(__dirname, './models');

  let publisher = createPublisher();
  setupAuthentication(app, publisher);

  setupRouter(app, publisher);

  let models = app.data.models();

  let mailJob = MailJob(config);

  let parts = [mailJob, publisher];

  return {
    publisher:publisher,
    async start(){
      try {
        for (let part of parts) {
          await part.start(app);
        }
      } catch(error){
        log.error(`cannot start: ${error}`);
      }
    },

    async stop(){
      await Promise.each(parts, obj => obj.stop(app));
    },

    seedDefault(){
/*      let seedDefaultFns = [
        models.Operator.seedDefault,
        models.ShortCode.seedDefault
      ];
      return Promise.each(seedDefaultFns, fn => fn());*/
    },

    async isSeeded() {
      let count = await models.ShortCode.count();
      log.debug("#shortCodes ", count);
      return count;
    }
  };
}

function setupRouter(app, publisherShortCode){
  //ShortCodes
  ShortCodeRouter(app, publisherShortCode);
}

function createPublisher(){
  let rabbitmq = config.rabbitmq;
  if(rabbitmq && rabbitmq.url){
    publisherOption.url = rabbitmq.url;
  }

  log.info("createPublisher: ", publisherOption);
  return new Publisher(publisherOption);
}

function setupAuthentication(app, publisherShortCode) {
  let auth = new PassportAuth(app, publisherShortCode);
  app.auth = auth;
  return auth;
}