import Promise from 'bluebird';
import Log from 'logfilename';
import {Publisher} from 'rabbitmq-pubsub';

import PassportAuth from './PassportAuth';

import config from 'config';

// Jobs
import MailJob from './jobs/mail/MailJob';

import SentMessageRouter from './sentMessage/SentMessageRouter';
import SentMessageApi from './sentMessage/SentMessageApi';

let log = new Log(__filename);

const publisherOption = { exchange: "sentMessage" };

export default function SentMessagePlugin(app){

  app.data.registerModelsFromDir(__dirname, './models');

  let publisher = createPublisher();
  setupAuthentication(app, publisher);

  setupRouter(app, publisher);

  let models = app.data.models();

  let mailJob = MailJob(config);

  let parts = [mailJob, publisher];

  return {
    publisher:publisher,
    async start(){
      try {
        for (let part of parts) {
          await part.start(app);
        };
      } catch(error){
        log.error(`cannot start: ${error}`);
      }
    },

    async stop(){
      await Promise.each(parts, obj => obj.stop(app));
    },

    seedDefault(){
/*      let seedDefaultFns = [
        models.ShortCode.seedDefault,
        models.Device.seedDefault,
        models.SentMessage.seedDefault
      ];
      return Promise.each(seedDefaultFns, fn => fn());*/
    },

    async isSeeded() {
      let count = await models.SentMessage.count();
      log.debug("#sentMessages ", count);
      return count;
    }
  };
}

function setupRouter(app, publisherSentMessage){
  //SentMessages
  let sentMessageApi = SentMessageApi(app);
  SentMessageRouter(app, sentMessageApi);
}

function createPublisher(){
  let rabbitmq = config.rabbitmq;
  if(rabbitmq && rabbitmq.url){
    publisherOption.url = rabbitmq.url;
  }

  log.info("createPublisher: ", publisherOption);
  return new Publisher(publisherOption);
}

function setupAuthentication(app, publisherSentMessage) {
  let auth = new PassportAuth(app, publisherSentMessage);
  app.auth = auth;
  return auth;
}