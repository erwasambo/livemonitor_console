import passport from 'koa-passport';
import registerLocal from './auth-strategy/LocalStrategy';
import registerJwt from './auth-strategy/JwtStrategy';

let config = require('config');

let log = require('logfilename')(__filename);

export default function(app, publisherUser) {

  let models = app.data.models();
  registerJwt(passport, models);
  registerLocal(passport, models);

  passport.serializeUser(function(user, done) {
    log.debug("serializeUser ", user);
    //TODO use redis
    done(null, user);
  });

  passport.deserializeUser(function(id, done) {
    log.debug("deserializeUser ", id);
    //TODO use redis
    done(null, id);
  });
};
