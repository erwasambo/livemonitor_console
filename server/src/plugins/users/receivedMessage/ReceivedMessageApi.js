import _ from 'lodash';
let log = require('logfilename')(__filename);

export default function ReceivedMessageApi(app) {
  let models = app.data.models();
  let validateJson = app.utils.api.validateJson;

  return {
    createReceivedMessage: async function (shortCodeIn) {
      validateJson(shortCodeIn, require('./schema/createReceivedMessage.json'));
      log.debug("createReceivedMessage: ", shortCodeIn);

      //Look out for duplicates
      let res = await models.ReceivedMessage.find({
        where: {receivedMessageId: shortCodeIn.receivedMessageId, timeStamp: shortCodeIn.timeStamp}
      });

      if (!res) {
        let result = await models.ReceivedMessage.create(shortCodeIn);
      }else {
        log.debug("duplicate not recorded: ", shortCodeIn);
      }
      //return savedShortCode.toJSON();
      return {
        success: true,
        message: "success in received message creation"
      };
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.ReceivedMessage.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let receivedMessages = _.map(result.rows, receivedMessage => receivedMessage.toJSON());
      return {
        count: result.count,
        receivedMessagesData: receivedMessages
      };
    },
    getOne: function (receivedMessageId) {
      log.debug("get receivedMessageId: ", receivedMessageId);
      return models.ReceivedMessage.findByReceivedMessageId(receivedMessageId);
    }
  };
}