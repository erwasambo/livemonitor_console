import Router from 'koa-66';
import Qs from 'qs';

let log = require('logfilename')(__filename);

export function ReceivedMessageHttpController(app, receivedMessageApi){
  log.debug("ReceivedMessageHttpController");

  let respond = app.utils.http.respond;
  return {
    async createReceivedMessage(context){
      return respond(context, receivedMessageApi, receivedMessageApi.createReceivedMessage, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        receivedMessageApi,
        receivedMessageApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let receivedMessageId = context.params.id;
      return respond(context, receivedMessageApi, receivedMessageApi.getOne, [receivedMessageId]);
    }
  };
}

export default function ReceivedMessageRouter(app, receivedMessageApi){
  let router = new Router();
  let receivedMessageHttpController = ReceivedMessageHttpController(app, receivedMessageApi);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', receivedMessageHttpController.createReceivedMessage);
  router.get('/', receivedMessageHttpController.getAll);
  router.get('/:id', receivedMessageHttpController.getOne);

  app.server.baseRouter().mount("/receivedMessages", router);
  return router;
}
