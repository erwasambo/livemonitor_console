import _ from 'lodash';

let log = require('logfilename')(__filename);

export default function MeApi(app) {
  let models = app.data.models();
  let validateJson = app.utils.api.validateJson;

  return {
    getByUserId: async function (userId) {
      log.debug("index userId: ", userId);
      let user = await models.User.findByUserId(userId);
      log.debug("index user: ", user.get());
      return user.toJSON();
    },

    getShortCodes: async function (userId) {
      log.debug("get userId: ", userId);
      return await models.User.getShortCodes(userId);
    },

    put: async function (userId, data) {
      validateJson(data, require('./schema/put.json'));
      log.debug("put userId %s, data: ", userId, data);
      await models.User.update({
        username: data.username
      }, {
        where: {
          id: userId
        }
      });
      log.debug("put done");
      return data;
    }
  };
}