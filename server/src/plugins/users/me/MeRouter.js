import Router from 'koa-66';

let log = require('logfilename')(__filename);

export function MeHttpController(app, meApi){
  log.debug("MeHttpController");
  let respond = app.utils.http.respond;
  return {
    async getShortCodes(context) {
      return respond(context, meApi, meApi.getShortCodes, [context.passport.user.id]);
    },
    async get(context) {
      return respond(context, meApi, meApi.getByUserId, [context.passport.user.id]);
    },
    async put(context) {
      return respond(context, meApi, meApi.put, [context.passport.user.id, context.request.body]);
    }
  };
}

export default function MeRouter(app,meApi){
  let router = new Router();
  let meHttpController = MeHttpController(app, meApi);
  router.get('/', meHttpController.get);
  router.get('/shortCodes', meHttpController.getShortCodes);
  router.put('/', meHttpController.put);
  app.server.baseRouter().mount("/me", router);
  return router;
}