import _ from 'lodash';
let log = require('logfilename')(__filename);

export default function DeviceApi(app) {
    let models = app.data.models();
    let validateJson = app.utils.api.validateJson;

    return {
        createDevice: async function (shortCodeIn) {
            validateJson(shortCodeIn, require('./schema/createDevice.json'));
            log.debug("createDevice: ", shortCodeIn);
            //TODO::See how you can implement already created later
            let result = await models.Device.create(shortCodeIn);
            //return savedShortCode.toJSON();
            return {
                success: true,
                message: "success in Device creation"
            };
        },
        getAll: async function (filter = {}) {
            _.defaults(filter, {
                order: 'DESC',
                limit: 10,
                offset: 0
            });
            log.info('getAll ', filter);
            let result = await models.Device.findAndCountAll({
                limit: Number(filter.limit),
                offset: Number(filter.offset)
            });
            log.info(`getAll count: ${result.count}`);
            let devices = _.map(result.rows, device => device.toJSON());
            return {
                count: result.count,
                deviceData: devices
            };
        },
        getOne: function (deviceId) {
            log.debug("get deviceId: ", deviceId);
            return models.Device.findByDeviceId(deviceId);
        }
    };
}