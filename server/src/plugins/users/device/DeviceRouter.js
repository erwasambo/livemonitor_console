import Router from 'koa-66';
import Qs from 'qs';

let log = require('logfilename')(__filename);

export function DeviceHttpController(app, deviceApi){
  log.debug("DeviceHttpController");

  let respond = app.utils.http.respond;
  return {
    async createDevice(context){
      return respond(context, deviceApi, deviceApi.createDevice, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        deviceApi,
        deviceApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let deviceId = context.params.id;
      return respond(context, deviceApi, deviceApi.getOne, [deviceId]);
    }
  };
}

export default function DeviceRouter(app, deviceApi){
  let router = new Router();
  let deviceHttpController = DeviceHttpController(app, deviceApi);
  router.post('/', deviceHttpController.createDevice);
  router.get('/', deviceHttpController.getAll);
  router.get('/:id', deviceHttpController.getOne);

  app.server.baseRouter().mount("/devices", router);

  return router;
}