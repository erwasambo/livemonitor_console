import Promise from 'bluebird';
import Log from 'logfilename';
import {Publisher} from 'rabbitmq-pubsub';

import PassportAuth from './PassportAuth';

import config from 'config';

// Jobs
import MailJob from './jobs/mail/MailJob';
import ReceivedMessageRouter from './receivedMessage/ReceivedMessageRouter';
import ReceivedMessageApi from './receivedMessage/ReceivedMessageApi';


let log = new Log(__filename);

const publisherOption = { exchange: "receivedMessage" };

export default function ReceivedMessagePlugin(app){

  app.data.registerModelsFromDir(__dirname, './models');

  let publisher = createPublisher();
  setupAuthentication(app, publisher);

  setupRouter(app, publisher);

  let models = app.data.models();

  let mailJob = MailJob(config);

  let parts = [mailJob, publisher];

  return {
    publisher:publisher,
    async start(){
      try {
        for (let part of parts) {
          await part.start(app);
        };
      } catch(error){
        log.error(`cannot start: ${error}`);
      }
    },

    async stop(){
      await Promise.each(parts, obj => obj.stop(app));
    },

    seedDefault(){
/*      let seedDefaultFns = [
        models.Device.seedDefault,
        models.ReceivedMessage.seedDefault
      ];
      return Promise.each(seedDefaultFns, fn => fn());*/
    },

    async isSeeded() {
      let count = await models.ReceivedMessage.count();
      log.debug("#receivedMessages ", count);
      return count;
    }
  };
}

function setupRouter(app, publisherReceivedMessage){
  //ReceivedMessages
  let receivedMessageApi = ReceivedMessageApi(app);
  ReceivedMessageRouter(app, receivedMessageApi);
}

function createPublisher(){
  let rabbitmq = config.rabbitmq;
  if(rabbitmq && rabbitmq.url){
    publisherOption.url = rabbitmq.url;
  }

  log.info("createPublisher: ", publisherOption);
  return new Publisher(publisherOption);
}

function setupAuthentication(app, publisherReceivedMessage) {
  let auth = new PassportAuth(app, publisherReceivedMessage);
  app.auth = auth;
  return auth;
}