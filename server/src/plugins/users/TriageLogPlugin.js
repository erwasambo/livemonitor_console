import Promise from 'bluebird';
import Log from 'logfilename';
import {Publisher} from 'rabbitmq-pubsub';

import PassportAuth from './PassportAuth';

import config from 'config';

// Jobs
import MailJob from './jobs/mail/MailJob';

import MeRouter from './me/MeRouter';

import TriageLogRouter from './triageLog/TriageLogRouter';
import TriageLogApi from './triageLog/TriageLogApi';

import AuthenticationRouter from './authentication/AuthenticationRouter';

let log = new Log(__filename);

const publisherOption = {
  exchange: "user",
  type: "topic",
  url: "amqp://livemonitorconsole.com"
};

export default function TriageLogPlugin(app){

  app.data.registerModelsFromDir(__dirname, './models');

  let publisher = createPublisher();
  setupAuthentication(app, publisher);

  setupRouter(app, publisher);

  let models = app.data.models();

  let mailJob = MailJob(config);

  let parts = [mailJob, publisher];

  return {
    publisher:publisher,
    async start(){
      try {
        for (let part of parts) {
          await part.start(app);
        }
      } catch(error){
        log.error(`cannot start: ${error}`);
      }
    },

    async stop(){
      await Promise.each(parts, obj => obj.stop(app));
    },

    seedDefault(){
/*      let seedDefaultFns = [
        models.SentMessage.seedDefault,
        models.TriageLog.seedDefault
      ];
      return Promise.each(seedDefaultFns, fn => fn());*/
    },

    async isSeeded() {
      let count = await models.TriageLog.count();
      log.debug("#triageLogs ", count);
      return count;
    }
  };
}

function setupRouter(app, publisherUser){
  //Authentication
  AuthenticationRouter(app, publisherUser);

  //Me
  MeRouter(app);
  //TriageLogs
  TriageLogRouter(app, publisherUser);
}

function createPublisher(){
  log.info("createPublisher: ", publisherOption);
  return new Publisher(publisherOption);
}

function setupAuthentication(app, publisherUser) {
  let auth = new PassportAuth(app, publisherUser);
  app.auth = auth;
  return auth;
}