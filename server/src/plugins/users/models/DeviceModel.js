'use strict';
import bcrypt from "bcrypt";
import Promise from 'bluebird';

module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);
  let models = sequelize.models;

  let Device = sequelize.define('Device', {
      deviceId: {
        type: DataTypes.STRING
      },
      phoneNumber: {
        type: DataTypes.STRING
      }
    },
    {
      tableName: "devices",
      classMethods: {

        seedDefault: async function () {
          let devicesJson = require('./fixtures/devices.json');
          log.debug('seedDefault: ', JSON.stringify(devicesJson, null, 4));
          return Device.bulkCreate(devicesJson);
        },

/*        associate: function(models) {
          Device.hasMany(models.SentMessage, { through : models.ShortCodeOperator, foreignKey: "device_id"});
          Device.hasMany(models.ReceivedMessage, { through : models.ShortCodeOperator, foreignKey: "device_id"});
          models.SentMessage.belongsTo(Device, { through : models.ShortCodeOperator, foreignKey: "shortCode_id"});
          models.ReceivedMessage.belongsTo(Device, { through : models.ShortCodeOperator, foreignKey: "shortCode_id"});
        },*/
        /**
         * Finds a device by its email
         * returns the model of the  device
         *
         * @param {String} email - the device's email address
         *
         * @returns {Promise} Promise device model
         */
        findByEmail: function (email) {
          return this.find({where: {email: email}});
        },
        /**
         * Finds a device by deviceid
         * returns the model of the  device
         *
         * @param {String} deviceid - deviceid of the device to find
         *
         * @returns {Promise} Promise device model
         */
        findByDeviceId: function (deviceid) {
          return this.find({where: {id: deviceid}});
        },

        getShortCodes: function (devicename) {
          return this.find({
            include: [
              {
                model: models.ReceivedMessage,
                include: [
                  {
                    model: models.ShortCode
                  }]
              }],
            where: {
              devicename: devicename
            }
          });
        },
        getSentMessages: function (devicename) {
          return this.find({
            include: [
              {
                model: models.ReceivedMessage,
                include: [
                  {
                    model: models.SentMessage
                  }]
              }],
            where: {
              devicename: devicename
            }
          });
        }
      },

      instanceMethods: {
        comparePassword: function (candidatePassword) {
          let me = this;
          return new Promise(function (resolve, reject) {
            let hashPassword = me.get('passwordHash') || '';

            bcrypt.compare(candidatePassword, hashPassword, function (err, isMatch) {
              if (err) {
                return reject(err);
              }
              resolve(isMatch);
            });
          });
        },

        /**
         * Removes password from the toJson
         *
         */
        toJSON: function () {
          let values = this.get({clone: true});
          delete values.password;
          delete values.passwordHash;
          return values;
        }
      }
    },
    {
      underscored: true
    });

  return Device;
};