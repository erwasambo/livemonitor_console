'use strict';
import bcrypt from "bcrypt";
import Promise from 'bluebird';

module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);
  let models = sequelize.models;

  let User = sequelize.define('User', {
      username: {
        type: DataTypes.STRING(64),
        unique: true,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING(64),
        unique: true,
        allowNull: false
      },
      firstName: {
        type: DataTypes.STRING(64)
      },
      lastName: {
        type: DataTypes.STRING(64)
      },
      location: {
        type: DataTypes.STRING(64)
      },
      password: DataTypes.STRING,
      passwordHash: DataTypes.STRING
    },
    {
      tableName: "users",
      classMethods: {
        seedDefault: async function () {
          let usersJson = require('./fixtures/users.json');
          log.debug('seedDefault: ', JSON.stringify(usersJson, null, 4));
          for (let userJson of usersJson) {
            await User.createUserInGroups(userJson, userJson.groups);
          }
        },
        /**
         * Finds a user by its email
         * returns the model of the  user
         *
         * @param {String} email - the user's email address
         *
         * @returns {Promise} Promise user model
         */
        findByEmail: function (email) {
          return this.find({where: {email: email}});
        },
        /**
         * Finds a user by userid
         * returns the model of the  user
         *
         * @param {String} userid - userid of the user to find
         *
         * @returns {Promise} Promise user model
         */
        findByUserId: function (userid) {
          return this.find({where: {id: userid}});
        },
        /**
         * Creates a user given a json representation and adds it to the group GroupName,
         * returns the model of the created user
         *
         * @param {Object} userJson  -   User in json format
         * @param {Array} groups - the groups to add the user in
         *
         * @returns {Promise}  Promise user created model
         */
        createUserInGroups: async function (userJson, groups) {
          log.debug("createUserInGroups user:%s, group: ", userJson, groups);
          return sequelize.transaction(async function (t) {
              log.info("create user");
              let userCreated = await models.User.create(userJson, {transaction: t});
              await models.UserGroup.addUserIdInGroups(groups, userCreated.get().id, t);
              return userCreated;
            })
            .catch(function (err) {
              log.error('createUserInGroups: rolling back', err);
              throw err;
            });
        },

        /**
         * Creates a user given a json representation and adds it to the shortCode shortCodeId,
         * returns the model of the created user
         *
         * @param {Object} userJson  -   User in json format
         * @param {Array} shortCodes - the shortCodes to add the user in
         *
         * @returns {Promise}  Promise user created model
         */
        createOrUpdateUserInShortCodes: async function (userId, shortCodes) {
          log.debug("createUserInShortCodes user:%s, userId: ", userId, shortCodes);
          return sequelize.transaction(async function (t) {
              log.info("create  user or update shortcodes");
              return await models.UserShortCode.addUserIdInShortCodes(shortCodes, userId, t);
            }).catch(function (err) {
              log.error('createUserInShortCodes: rolling back', err);
              throw err;
            });
        },

        updateUser: function updateUser(userId, data) {
          if(data.newAllocatedShortCodeIds) {
            this.createOrUpdateUserInShortCodes(userId, data.newAllocatedShortCodeIds);
          }

          return this.update({
            username: data.username,
            location: data.location,
            email:data.email,
            firstName:data.firstName,
            lastName:data.lastName
          }, {
            where: {
              id: userId
            }
          });
        },

        deleteUser: function deleteUser(userId) {
          return this.destroy({
            where: {
              id: userId //this will be your id that you want to delete
            }
          }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
            if(rowDeleted === 1){
              log.debug('Deleted successfully');
            }
          }, function(err){
            log.debug(err);
          });
        },

        /**
         * Checks whether a user is able to perform an action on a resource
         * Equivalent to: select name from permissions p join group_permissions g on p.id=g.permission_id where g.group_id=(select group_id from users where username='aliceab@example.com') AND p.resource='user' and p.create=true;
         * @param {String} userId  - The userId to search
         * @param {String} resource  -The resource name to search
         * @param {String} action  - The action , "create,read,update,delete"
         *
         * @returns {Boolean} True if the user can perform the action on the resource otherwise false
         */

        checkUserPermission: async function (userId, resource, action) {
          log.debug('Checking %s permission for %s on %s', action, userId, resource);
          let where = {
            resource: resource
          };
          where[action.toUpperCase()] = true;
          let res = await this.find({
            include: [
              {
                model: models.Group,
                include: [
                  {
                    model: models.Permission,
                    where: where
                  }]
              }],
            where: {
              id: userId
            }
          });
          let authorized = res ? true : false;
          return authorized;
        },

        checkUserAdminPermission: async function (userId, resource, action) {
          log.debug('Checking %s  admin permission for %s on %s', action, userId, resource);
          let where = {
            resource: resource,
          };
          where[action.toUpperCase()] = true;
          let res = await this.find({
            include: [
              {
                model: models.Group,
                include: [
                  {
                    model: models.Permission,
                    where: where
                  }]
              }],
            where: {
              id: userId,
              username: "admin"
            }
          });
          let adminAuthorized = res ? true : false;
          return adminAuthorized;
        },

        /**
         * Returns all permissions associated with a user
         *
         * @param {String} username - The username to search permissions for
         *
         * @returns {Promise} a Promise containing array of permission results
         */
        getPermissions: function (username) {
          return this.find({
            include: [
              {
                model: models.Group,
                include: [
                  {
                    model: models.Permission
                  }]
              }],
            where: {
              username: username
            }
          });
        },

        /**
         * Returns all short codes associated with a user
         *
         * @param {String} username - The username to search permissions for
         *
         * @returns {Promise} a Promise containing array of permission results
         */
        getShortCodes: function (userId) {
          return this.find({
            include: [
              {
                model: models.ShortCode,
                include: [
                  {
                    model: models.Operator
                  }]
              }],
            where: {
              id: userId
            }
          });
        }
      },

      instanceMethods: {
        comparePassword: function (candidatePassword) {
          let me = this;
          return new Promise(function (resolve, reject) {
            let hashPassword = me.get('passwordHash') || '';

            bcrypt.compare(candidatePassword, hashPassword, function (err, isMatch) {
              if (err) {
                return reject(err);
              }
              resolve(isMatch);
            });
          });
        },

        /**
         * Removes password from the toJson
         *
         */
        toJSON: function () {
          let values = this.get({clone: true});
          delete values.password;
          delete values.passwordHash;
          return values;
        }
      }
    },
    {
      underscored: true
    });

  let hashPasswordHook = function (instance, options, done) {
    if (!instance.changed('password')) {
      return done();
    }
    bcrypt.hash(instance.get('password'), 10, function (err, hash) {
      if (err) {
        return done(err);
      }
      instance.set('password');
      instance.set('passwordHash', hash);
      done();
    });
  };

  User.beforeCreate(hashPasswordHook);
  User.beforeUpdate(hashPasswordHook);

  return User;
};