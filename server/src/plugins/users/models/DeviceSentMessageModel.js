let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function(sequelize, DataTypes) {

  let models = sequelize.models;
  assert(models);

  let DeviceSentMessage = sequelize.define('DeviceSentMessage', {
    name: DataTypes.STRING
  }, {
    tableName:"device_sentMessages",
    classMethods: {
      addDeviceIdInSentMessage:addDeviceIdInSentMessage,
      addDeviceIdInSentMessages:addDeviceIdInSentMessages
    }
  });

  /**
    * Creates in the db deviceSentMessage association between sentMessage name and deviceId
    * @param {Array} sentMessages  - Name of the sentMessage for which we want to add the device
    * @param {String} deviceId   -   deviceId to be added to the sentMessage   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDeviceIdInSentMessages(sentMessages, deviceId, t) {
     log.debug(`addDeviceIdInSentMessages device:${deviceId}, #sentMessages ${sentMessages.length}`);
     for (let sentMessage of sentMessages) {
       await DeviceSentMessage.addDeviceIdInSentMessage(sentMessage, deviceId, t);
     }
   }
   /**
    * Creates in the db deviceSentMessage association between sentMessagename and deviceId
    * @param {String} sentMessageName  - Name of the sentMessage for which we want to add the device
    * @param {String} deviceId   -   deviceId to be added to the sentMessage   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDeviceIdInSentMessage(sentMessageName, deviceId, t) {
     log.debug(`addDeviceIdInSentMessage device:${deviceId}, sentMessage: ${sentMessageName}`);
     //let sentMessage = await models.SentMessage.findByName(sentMessageName);
     let sentMessage = await models.SentMessage.find({
       where: { name: sentMessageName }
     }, {transaction:t});
     if (!sentMessage) {
       let err = {name: 'SentMessageNotFound', message: sentMessageName};
       throw err;
     };
     //log.debug(`addDeviceIdInSentMessage upsert to device:${deviceId} sentMessage: ${sentMessage.get().id}`);
     return DeviceSentMessage.upsert({
       sentMessage_id: sentMessage.get().id,
       device_id: deviceId
     }, {transaction:t});
   }

  return DeviceSentMessage;
};
