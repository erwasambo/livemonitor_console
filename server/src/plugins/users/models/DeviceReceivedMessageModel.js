let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function(sequelize, DataTypes) {

  let models = sequelize.models;
  assert(models);

  let DeviceReceivedMessage = sequelize.define('DeviceReceivedMessage', {
    name: DataTypes.STRING
  }, {
    tableName:"device_receivedMessages",
    classMethods: {
      addDeviceIdInReceivedMessage:addDeviceIdInReceivedMessage,
      addDeviceIdInReceivedMessages:addDeviceIdInReceivedMessages
    }
  });

  /**
    * Creates in the db deviceReceivedMessage association between receivedMessage name and deviceId
    * @param {Array} receivedMessages  - Name of the receivedMessage for which we want to add the device
    * @param {String} deviceId   -   deviceId to be added to the receivedMessage   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDeviceIdInReceivedMessages(receivedMessages, deviceId, t) {
     log.debug(`addDeviceIdInReceivedMessages device:${deviceId}, #receivedMessages ${receivedMessages.length}`);
     for (let receivedMessage of receivedMessages) {
       await DeviceReceivedMessage.addDeviceIdInReceivedMessage(receivedMessage, deviceId, t);
     }
   }
   /**
    * Creates in the db deviceReceivedMessage association between receivedMessagename and deviceId
    * @param {String} receivedMessageName  - Name of the receivedMessage for which we want to add the device
    * @param {String} deviceId   -   deviceId to be added to the receivedMessage   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDeviceIdInReceivedMessage(receivedMessageName, deviceId, t) {
     log.debug(`addDeviceIdInReceivedMessage device:${deviceId}, receivedMessage: ${receivedMessageName}`);
     //let receivedMessage = await models.ReceivedMessage.findByName(receivedMessageName);
     let receivedMessage = await models.ReceivedMessage.find({
       where: { name: receivedMessageName }
     }, {transaction:t});
     if (!receivedMessage) {
       let err = {name: 'ReceivedMessageNotFound', message: receivedMessageName};
       throw err;
     };
     //log.debug(`addDeviceIdInReceivedMessage upsert to device:${deviceId} receivedMessage: ${receivedMessage.get().id}`);
     return DeviceReceivedMessage.upsert({
       receivedMessage_id: receivedMessage.get().id,
       device_id: deviceId
     }, {transaction:t});
   }

  return DeviceReceivedMessage;
};
