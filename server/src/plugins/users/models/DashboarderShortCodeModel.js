let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function(sequelize, DataTypes) {

  let models = sequelize.models;
  assert(models);

  let DashboarderShortCode = sequelize.define('DashboarderShortCode', {
    name: DataTypes.STRING
  }, {
    tableName:"dashboarder_shortCodes",
    classMethods: {
      addDashboarderIdInShortCode:addDashboarderIdInShortCode,
      addDashboarderIdInShortCodes:addDashboarderIdInShortCodes
    }
  });

  /**
    * Creates in the db dashboarderShortCode association between shortCode id and dashboarderId
    * @param {Array} shortCodes  - id of the shortCode for which we want to add the dashboarder
    * @param {String} dashboarderId   -   dashboarderId to be added to the shortCode   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDashboarderIdInShortCodes(shortCodes, dashboarderId, t) {
     log.debug(`addDashboarderIdInShortCodes dashboarder:${dashboarderId}, #shortCodes ${shortCodes.length}`);
     for (let shortCode of shortCodes) {
       await DashboarderShortCode.addDashboarderIdInShortCode(shortCode, dashboarderId, t);
     }
   }
   /**
    * Creates in the db dashboarderShortCode association between shortCodeId and dashboarderId
    * @param {String} shortCodeId  - Id of the shortCode for which we want to add the dashboarder
    * @param {String} dashboarderId   -   dashboarderId to be added to the shortCode   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDashboarderIdInShortCode(shortCodeId, dashboarderId, t) {
     log.debug(`addDashboarderIdInShortCode dashboarder:${dashboarderId}, shortCode(Id: ${shortCodeId}`);
     let shortCode = await models.ShortCode.find({where: {id: shortCodeId}}, {transaction:t});
     if (!shortCode) {
       let err = {name: 'ShortCodeNotFound', message: shortCodeId};
       throw err;
     };

     return DashboarderShortCode.upsert({
       shortCode_id: shortCode.get().id,
       dashboarder_id: dashboarderId
     }, {transaction:t});
   }

  return DashboarderShortCode;
};
