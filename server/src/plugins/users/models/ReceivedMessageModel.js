'use strict';
module.exports = function(sequelize, DataTypes) {
  let log = require('logfilename')(__filename);

  let ReceivedMessage = sequelize.define('ReceivedMessage', {
    receivedMessageId: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    body: {
      type: DataTypes.STRING
    },
    device: {
      type: DataTypes.STRING
    },
    timeStamp: {
      type: DataTypes.STRING
    }
  }, {
    tableName: "receivedMessages",
    classMethods: {
      seedDefault:async function () {
        let receivedMessagesJson = require('./fixtures/received_messages.json');
        log.debug('seedDefault: ', JSON.stringify(receivedMessagesJson, null, 4));
        return ReceivedMessage.bulkCreate(receivedMessagesJson);
      },
      findByName: findByName,
      /*associate: function(models) {
        models.Device.belongsTo(ReceivedMessage,{ through : models.DeviceReceivedMessage, foreignKey: "device_id" });
        ReceivedMessage.belongsTo(models.Device,{ through : models.DeviceReceivedMessage, foreignKey: "receivedMessage_id"   });
      }*/
    }
  });

  let models = sequelize.models;

  function findByName(receivedMessageName) {
    return models.ReceivedMessage.find({where: { name: receivedMessageName } });
  }

  return ReceivedMessage;
};
