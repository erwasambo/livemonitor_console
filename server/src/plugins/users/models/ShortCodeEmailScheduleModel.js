'use strict';
import bcrypt from "bcrypt";
import Promise from 'bluebird';

module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);
  let models = sequelize.models;

  let ShortCodeEmailSchedule = sequelize.define('ShortCodeEmailSchedule', {
      shortCodeId: {
        type: DataTypes.STRING(64),
        allowNull: false
      },
      date: {
        type: DataTypes.STRING(64)
      },
      timeStamp: {
        type: DataTypes.STRING(64)
      }
    },
    {
      tableName: "shortCodeEmailSchedule",
      classMethods: {
        associate: function(models) {},
        seedDefault: async function () {},
        /**
         * Finds a shortCode by its email
         * returns the model of the  shortCode
         *
         * @param {String} email - the shortCode's email address
         *
         * @returns {Promise} Promise shortCode model
         */
        createShortCodeEmailSchedule: async function (shortCodeEmailScheduleJson) {
          return sequelize.transaction(async function (t) {
              log.info("create createShortCodeEmailSchedule" + JSON.stringify(shortCodeEmailScheduleJson));
              return await models.ShortCodeEmailSchedule.create(shortCodeEmailScheduleJson, {transaction: t});
            })
            .catch(function (err) {
              log.error('createShortCodeEmailSchedule: rolling back', err);
              throw err;
            })
        },

        findByShortCodeEmailScheduleId: function (shortCodeEmailScheduleId) {
          return this.find({where: {id: shortCodeEmailScheduleId}});
        },

        findByDateAndShortCodeId: function (shortCodeId, date) {
          return this.find({where: {shortCodeId: shortCodeId, date: date}});
        }
      }
    },
    {
      underscored: true
    });

  return ShortCodeEmailSchedule;
};