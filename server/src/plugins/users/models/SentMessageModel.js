'use strict';
module.exports = function(sequelize, DataTypes) {
  let log = require('logfilename')(__filename);

  let SentMessage = sequelize.define('SentMessage', {
    sentMessageId: {
      type: DataTypes.STRING
    },
    device: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    body: {
      type: DataTypes.STRING
    },
    shortCode: {
      type: DataTypes.STRING
    },
    timeStamp: {
      type: DataTypes.STRING
    }
  }, {
    tableName: "sentMessages",
    classMethods: {
      seedDefault:async function () {
        let sentMessagesJson = require('./fixtures/sent_messages.json');
        log.debug('seedDefault: ', JSON.stringify(sentMessagesJson, null, 4));
        return SentMessage.bulkCreate(sentMessagesJson);
      },
/*      associate: function(models) {
        models.Device.belongsTo(SentMessage,{ through : models.DeviceSentMessage, foreignKey: "device_id" });
        SentMessage.belongsTo(models.Device,{ through : models.DeviceSentMessage, foreignKey: "sentMessage_id"   });

        models.ShortCode.belongsTo(SentMessage,{ through : models.ShortCodeSentMessage, foreignKey: "device_id" });
        SentMessage.belongsTo(models.ShortCode,{ through : models.ShortCodeSentMessage, foreignKey: "sentMessage_id"   });
      },*/
      findBySentMessageId: findBySentMessageId
    }
  });

  let models = sequelize.models;

  function findBySentMessageId(sentMessageId) {
    return models.SentMessage.find({where: { sentMessageId: sentMessageId } });
  }

  return SentMessage;
};
