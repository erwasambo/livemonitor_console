'use strict';
module.exports = function(sequelize, DataTypes) {
  let log = require('logfilename')(__filename);

  let TriageLog = sequelize.define('TriageLog', {
    sentMessageId: {
      type: DataTypes.STRING
    },
    triageStatus: {
      type: DataTypes.STRING
    },
    timeStamp: {
      type: DataTypes.STRING
    }
  }, {
    tableName: "triageLog",
    classMethods: {
      seedDefault:async function () {
        let triageLogsJson = require('./fixtures/triage_logs.json');
        log.debug('seedDefault: ', JSON.stringify(triageLogsJson, null, 4));
        return TriageLog.bulkCreate(triageLogsJson);
      },
      findByName: findByName
      /*associate: function(models) {
        models.SentMessage.belongsTo(TriageLog,{ through : models.SentMessageTriageLog, foreignKey: "sentMessage_id" });
        TriageLog.belongsTo(models.SentMessage,{ through : models.SentMessageTriageLog, foreignKey: "sentMessage_id"   });
      }*/
    }
  });

  let models = sequelize.models;

  function findByName(sentMessageName) {
    return models.TriageLog.find({where: { name: sentMessageName } });
  }

  return TriageLog;
};