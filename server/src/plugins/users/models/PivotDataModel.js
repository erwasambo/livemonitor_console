'use strict';
module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);

  let PivotData = sequelize.define('PivotData', {
    sentMessageId: {
      type: DataTypes.STRING
    },
    shortCodeId: {
      type: DataTypes.STRING
    },
    shortCodeName: {
      type: DataTypes.STRING
    },
    date: {
      type: DataTypes.STRING
    },
    delayedAmount: {
      type: DataTypes.STRING
    },
    failedAmount: {
      type: DataTypes.STRING
    },
    successfulAmount: {
      type: DataTypes.STRING
    },
    operatorName: {
      type: DataTypes.STRING
    },
    operatorId: {
      type: DataTypes.STRING
    }
  }, {
    tableName: "pivotData",
    classMethods: {
      seedDefault: async function () {
        /*        let pivotDatasJson = require('./fixtures/triage_logs.json');
         log.debug('seedDefault: ', JSON.stringify(pivotDatasJson, null, 4));*/
        return "Wewe";// PivotData.bulkCreate(pivotDatasJson);
      },
      findByPivotDataId: findByPivotDataId
      /*associate: function(models) {
       models.SentMessage.belongsTo(PivotData,{ through : models.SentMessagePivotData, foreignKey: "sentMessage_id" });
       PivotData.belongsTo(models.SentMessage,{ through : models.SentMessagePivotData, foreignKey: "sentMessage_id"   });
       }*/
    }
  });

  let models = sequelize.models;

  function findByPivotDataId(shortCodeId) {
    return models.PivotData.find({where: {shortCodeId: shortCodeId}});
  }

  return PivotData;
};