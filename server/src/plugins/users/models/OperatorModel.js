'use strict';
module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);
  let models = sequelize.models;

  let Operator = sequelize.define('Operator', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    }
  }, {
    tableName: "operators",
    classMethods: {
      associate: function (models) {
        models.Operator.hasMany(models.ShortCode, {as: 'ShortCodes'});
        models.ShortCode.belongsTo(models.Operator);
      },

      createOperator: async function (operatorIn) {
        log.debug("createOperator operator:%s, operatorId: ",operatorIn);

        return sequelize.transaction(async function (t) {
            log.info("create operator");
            let operatorCreated = await models.Operator.create(operatorIn, {transaction: t});
            return operatorCreated;
          })
          .catch(function (err) {
            log.error('createOperator: rolling back', err);
            throw err;
          });
      },

      findByOperatorId: function (operatorId) {
        return Operator.find({where: {id: operatorId}});
      },
      findByName: function (name) {
        return Operator.find({where: {name: name}});
      }
    }
  });

  return Operator;
};