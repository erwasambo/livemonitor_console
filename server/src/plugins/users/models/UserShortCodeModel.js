let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function(sequelize, DataTypes) {

  let models = sequelize.models;

  let UserShortCode = sequelize.define('UserShortCode', {}, {
    tableName:"user_shortCodes",
    classMethods: {
      addUserIdInShortCode:addUserIdInShortCode,
      addUserIdInShortCodes:addUserIdInShortCodes
    }
  });

  /**
    * Creates in the db userShortCode association between shortCode id and userId
    * @param {Array} shortCodes  - Id of the shortCode for which we want to add the user
    * @param {String} userId   -   userId to be added to the shortCode   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addUserIdInShortCodes(shortCodes, userId, t) {
     log.debug(`addUserIdInShortCodes user:${userId}, #shortCodes ${shortCodes.length}`);

    //Delete all pre-existing selections first
     await models.UserShortCode.destroy({where: { user_id: userId }}, {transaction:t});
    log.debug(`Delete all pre-existing selections first:${userId},`);

     for (let shortCode of shortCodes) {
       await UserShortCode.addUserIdInShortCode(shortCode, userId, t);
     }
   }
   /**
    * Creates in the db userShortCode association between shortCodename and userId
    * @param {String} shortCodeId  - Id of the shortCode for which we want to add the user
    * @param {String} userId   -   userId to be added to the shortCode   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addUserIdInShortCode(shortCodeId, userId, t) {
     log.debug(`addUserIdInShortCode user:${userId}, shortCode: ${shortCodeId}`);
     //let shortCode = await models.ShortCode.findByName(shortCodeId);
     let shortCode = await models.ShortCode.find({
       where: { id: shortCodeId }
     }, {transaction:t});
     if (!shortCode) {
       let err = {name: 'ShortCodeNotFound', message: shortCodeId};
       throw err;
     }
     //log.debug(`addUserIdInShortCode upsert to user:${userId} shortCode: ${shortCode.get().id}`);
     return UserShortCode.upsert({
       shortCode_id: shortCode.get().id,
       user_id: userId
     }, {transaction:t});
   }

  return UserShortCode;
};
