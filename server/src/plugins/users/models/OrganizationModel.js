'use strict';
module.exports = function(sequelize, DataTypes) {
  let log = require('logfilename')(__filename);

  let Organization = sequelize.define('Organization', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: "organizations",
    classMethods: {
      seedDefault:async function () {
        let organizationsJson = require('./fixtures/organizations.json');
        log.debug('seedDefault: ', JSON.stringify(organizationsJson, null, 4));
        return Organization.bulkCreate(organizationsJson);
      },
      findByName: findByName,
      /*associate: function(models) {
        models.User.belongsToMany(Organization,{ through : models.UserOrganization, foreignKey: "user_id" });
        Organization.belongsToMany(models.User,{ through : models.UserOrganization, foreignKey: "organization_id"});

        models.Dashboarder.belongsToMany(Organization,{ through : models.DashboarderOrganization, foreignKey: "dashboarder_id" });
        Organization.belongsToMany(models.Dashboarder,{ through : models.DashboarderOrganization, foreignKey: "organization_id"});
      }*/
    }
  });

  let models = sequelize.models;

  function findByName(organizationName) {
    return models.Organization.find({where: { name: organizationName } });
  }

  return Organization;
};