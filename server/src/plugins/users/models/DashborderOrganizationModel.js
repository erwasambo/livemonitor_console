let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function(sequelize, DataTypes) {

  let models = sequelize.models;
  assert(models);

  let DashboarderOrganization = sequelize.define('DashboarderOrganization', {
    name: DataTypes.STRING
  }, {
    tableName:"dashboarder_organizations",
    classMethods: {
      addDashboarderIdInOrganization:addDashboarderIdInOrganization,
      addDashboarderIdInOrganizations:addDashboarderIdInOrganizations
    }
  });

  /**
    * Creates in the db dashboarderOrganization association between organization name and dashboarderId
    * @param {Array} organizations  - Name of the organization for which we want to add the dashboarder
    * @param {String} dashboarderId   -   dashboarderId to be added to the organization   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDashboarderIdInOrganizations(organizations, dashboarderId, t) {
     log.debug(`addDashboarderIdInOrganizations dashboarder:${dashboarderId}, #organizations ${organizations.length}`);
     for (let organization of organizations) {
       await DashboarderOrganization.addDashboarderIdInOrganization(organization, dashboarderId, t);
     }
   }
   /**
    * Creates in the db dashboarderOrganization association between organizationname and dashboarderId
    * @param {String} organizationName  - Name of the organization for which we want to add the dashboarder
    * @param {String} dashboarderId   -   dashboarderId to be added to the organization   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addDashboarderIdInOrganization(organizationName, dashboarderId, t) {
     log.debug(`addDashboarderIdInOrganization dashboarder:${dashboarderId}, organization: ${organizationName}`);
     let organization = await models.Organization.find({
       where: { name: organizationName }
     }, {transaction:t});
     if (!organization) {
       let err = {name: 'OrganizationNotFound', message: organizationName};
       throw err;
     };

     return DashboarderOrganization.upsert({
       organization_id: organization.get().id,
       dashboarder_id: dashboarderId
     }, {transaction:t});
   }

  return DashboarderOrganization;
};