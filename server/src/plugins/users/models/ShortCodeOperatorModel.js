let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function (sequelize, DataTypes) {

  let models = sequelize.models;
  let ShortCodeOperator = sequelize.define('ShortCodeOperator', {}, {
    tableName: "shortCode_operator",
    classMethods: {
      addShortCodeIdInOperator: addShortCodeIdInOperator
    }
  });

  /**
   * Creates in the db all the git pshortCodeOperator within operatorId associated with the shortCodeId
   *
   * @param {String} shortCodeId  - Id of the shortCode for which we want to add operator
   * @param {String} operatorId - Operator id linked with the shortCode
   *
   * @returns {Promise} returns a list of Promises results
   */
  async function addShortCodeIdInOperator(operatorId, shortCodeId, t) {
    log.debug(`add: shortCode ${shortCodeId}, operatorId ${operatorId}`);
    let operator = await models.Operator.find({
      where: { id: operatorId }
    }, {transaction:t});
    if (!operator) {
      let err = {name: 'OperatorNotFound', message: operatorId};
      throw err;
    }
    return ShortCodeOperator.create({
      operator_id: operator.get().id,
      shortCode_id: shortCodeId
    }, {transaction:t});
  }

  return ShortCodeOperator;
};