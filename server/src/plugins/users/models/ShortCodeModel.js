'use strict';
import bcrypt from "bcrypt";
import Promise from 'bluebird';

module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);
  let models = sequelize.models;

  let ShortCode = sequelize.define('ShortCode', {
      name: {
        type: DataTypes.STRING(64),
        allowNull: false
      },
      description: {
        type: DataTypes.STRING(64)
      },
      deviceNumber: {
        type: DataTypes.STRING(64)
      },
      operator: {
        type: DataTypes.STRING(64)
      },
      email: {
        type: DataTypes.STRING(64)
      }
    },
    {
      tableName: "shortCodes",
      classMethods: {
        associate: function(models) {
          models.User.belongsToMany(models.ShortCode,{ through : models.UserShortCode, foreignKey: "user_id"});
          models.ShortCode.belongsToMany(models.User,{ through : models.UserShortCode, foreignKey: "shortCode_id"});
        },

        seedDefault: async function () {
          /*let shortCodesJson = require('./fixtures/short_codes.json');
          log.debug('seedDefault: ', JSON.stringify(shortCodesJson, null, 4));
          for (let shortCodeJson of shortCodesJson) {
            await ShortCode.createShortCodeInOperators(shortCodeJson);
          }*/
        },
        /**
         * Finds a shortCode by its email
         * returns the model of the  shortCode
         *
         * @param {String} email - the shortCode's email address
         *
         * @returns {Promise} Promise shortCode model
         */
        findByEmail: function (email) {
          return this.find({where: {email: email}});
        },
        /**
         * Finds a shortCode by shortCodeid
         * returns the model of the  shortCode
         *
         * @param {String} shortCodeid - shortCodeid of the shortCode to find
         *
         * @returns {Promise} Promise shortCode model
         */
        findByShortCodeId: function (shortCodeId) {
          return this.find({where: {id: shortCodeId}});
        },
        /**
         * Creates a shortCode given a json representation and adds it to the group GroupName,
         * returns the model of the created shortCode
         *
         * @param {Object} shortCodeJson  -   ShortCode in json format
         * @param {Array} groups - the groups to add the shortCode in
         * @param {Array} shortCodes - the shortCodes to add the shortCode in
         * @param {Array} operators - the operators to add the shortCode in
         *
         * @returns {Promise}  Promise shortCode created model
         */
        createShortCodeInOperator: async function (shortCodeJson) {

          let operatorId = shortCodeJson.operatorId;

          log.debug("createShortCodeInOperator shortCode:%s, operatorId: ", shortCodeJson, operatorId);

          return sequelize.transaction(async function (t) {
              log.info("create shortCode");
              let shortCodeCreated = await models.ShortCode.create(shortCodeJson, {transaction: t});
              await models.ShortCodeOperator.addShortCodeIdInOperator(operatorId, shortCodeCreated.get().id, t);
              //Add Shortcode to admin by default
              await models.UserShortCode.create({shortCode_id: shortCodeCreated.get().id,user_id: 1}, {transaction:t});
              return shortCodeCreated;
            })
            .catch(function (err) {
              log.error('createShortCodeInOperator: rolling back', err);
              throw err;
            });
        },
        /**
         * Return operator associated with a shortCode
         *
         * @param {String} shortCodeId - The shortCodeId to search operator for
         *
         * @returns {Promise} a Promise containing operator result
         */

        getOperator: function (shortCodeId) {
          return this.find({
            include:[
              {
                model: models.Operator
              }],
            where:{
              id: shortCodeId
            }
          });
        },

        getUsers: function (shortCodeId) {
          return this.find({
            include: [
              {
                model: models.ShortCode,
                include: [
                  {
                    model: models.User
                  }]
              }],
            where: {
              id: shortCodeId
            }
          });
        }
      },

      instanceMethods: {
        comparePassword: function (candidatePassword) {
          let me = this;
          return new Promise(function (resolve, reject) {
            let hashPassword = me.get('passwordHash') || '';

            bcrypt.compare(candidatePassword, hashPassword, function (err, isMatch) {
              if (err) {
                return reject(err);
              }
              resolve(isMatch);
            });
          });
        },

        /**
         * Removes password from the toJson
         *
         */
        toJSON: function () {
          let values = this.get({clone: true});
          delete values.password;
          delete values.passwordHash;
          return values;
        }
      }
    },
    {
      underscored: true
    });

  return ShortCode;
};