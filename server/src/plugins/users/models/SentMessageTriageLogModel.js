let assert = require('assert');
let log = require('logfilename')(__filename);

module.exports = function(sequelize, DataTypes) {

  let models = sequelize.models;
  assert(models);

  let SentMessageTriageLog = sequelize.define('SentMessageTriageLog', {
    name: DataTypes.STRING
  }, {
    tableName:"sentMessage_triageLog",
    classMethods: {
      addSentMessageIdInTriageLog:addSentMessageIdInTriageLog,
      addSentMessageIdInTriageLogs:addSentMessageIdInTriageLogs
    }
  });

  /**
    * Creates in the db sentMessageTriageLog association between sentMessage name and sentMessageId
    * @param {Array} triageLog  - Name of the sentMessage for which we want to add the sentMessage
    * @param {String} sentMessageId   -   sentMessageId to be added to the sentMessage   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addSentMessageIdInTriageLogs(triageLog, sentMessageId, t) {
     log.debug(`addSentMessageIdInTriageLogs sentMessage:${sentMessageId}, #triageLog ${triageLog.length}`);
     for (let sentMessage of triageLog) {
       await SentMessageTriageLog.addSentMessageIdInTriageLog(sentMessage, sentMessageId, t);
     }
   }
   /**
    * Creates in the db sentMessageTriageLog association between sentMessagename and sentMessageId
    * @param {String} sentMessageName  - Name of the sentMessage for which we want to add the sentMessage
    * @param {String} sentMessageId   -   sentMessageId to be added to the sentMessage   *
    * @returns {Promise} returns a  Promise containing the results of the upsert
    */
   async function addSentMessageIdInTriageLog(sentMessageName, sentMessageId, t) {
     log.debug(`addSentMessageIdInTriageLog sentMessage:${sentMessageId}, sentMessage: ${sentMessageName}`);
     let sentMessage = await models.TriageLog.find({
       where: { name: sentMessageName }
     }, {transaction:t});
     if (!sentMessage) {
       let err = {name: 'TriageLogNotFound', message: sentMessageName};
       throw err;
     };
     //log.debug(`addSentMessageIdInTriageLog upsert to sentMessage:${sentMessageId} sentMessage: ${sentMessage.get().id}`);
     return SentMessageTriageLog.upsert({
       sentMessage_id: sentMessage.get().id,
       sentMessage_id: sentMessageId
     }, {transaction:t});
   }

  return SentMessageTriageLog;
};
