'use strict';
import bcrypt from "bcrypt";
import Promise from 'bluebird';

module.exports = function (sequelize, DataTypes) {
  let log = require('logfilename')(__filename);
  let models = sequelize.models;

  let Dashboarder = sequelize.define('Dashboarder', {
      lastUpdated: {
        type: DataTypes.STRING(64)
      },
      status: {
        type: DataTypes.STRING(64)
      }
    },
    {
      tableName: "dashboarders",
      classMethods: {

        seedDefault: async function () {
          let dashboardersJson = require('./fixtures/dashboarder.json');
          log.debug('seedDefault: ', JSON.stringify(dashboardersJson, null, 4));
          await Dashboarder.bulkCreate(dashboardersJson);
        },




       /* associate: function(models) {

          Dashboarder.belongsTo(models.User, {
            foreignKey: {
              name: "user_id",
              allowNull: true
            }
          });

          models.User.hasMany(models.Dashboarder,{
            foreignKey: {
              name: "user_id",
              allowNull: true
            }
          });



          models.Dashboarder.belongsTo(Dashboarder,{ through : models.DashboarderShortCode, foreignKey: "dashboarder_id"});
          models.ShortCode.belongsTo(Dashboarder,{ through : models.DashboarderShortCode, foreignKey: "shortCode_id"});
          models.Organization.belongsToMany(models.Organization,{ through : models.DashboarderOrganization, foreignKey: "dashboarder_id" });
        },*/
  /**
         * Finds a dashboarder by its email
         * returns the model of the  dashboarder
         *
         * @param {String} email - the dashboarder's email address
         *
         * @returns {Promise} Promise dashboarder model
         */
        findByEmail: function (email) {
          return this.find({where: {email: email}});
        },
        /**
         * Finds a dashboarder by dashboarderid
         * returns the model of the  dashboarder
         *
         * @param {String} dashboarderid - dashboarderid of the dashboarder to find
         *
         * @returns {Promise} Promise dashboarder model
         */
        findByDashboarderId: function (dashboarderid) {
          return this.find({where: {id: dashboarderid}});
        },
        /**
         * Creates a dashboarder given a json representation and adds it to the shortCode ShortCodeName,
         * returns the model of the created dashboarder
         *
         * @param {Object} dashboarderJson  -   Dashboarder in json format
         * @param {Array} shortCodes - the shortCodes to add the dashboarder in
         * @param {Array} shortCodes - the shortCodes to add the dashboarder in
         * @param {Array} organizations - the organizations to add the dashboarder in
         *
         * @returns {Promise}  Promise dashboarder created model
         */
        createDashboarderInShortCodesOrganizations: async function (dashboarderJson) {

          let shortCode = dashboarderJson.shortCode;
          let organizations = dashboarderJson.organizations;

          log.debug("createDashboarderInShortCodesOrganizations dashboarder:%s, shortCode: ", dashboarderJson, shortCode);
          log.debug("createDashboarderInShortCodesOrganizations dashboarder:%s, organizations: ", dashboarderJson, organizations);

          return sequelize.transaction(async function (t) {
              log.info("create dashboarder");
              let dashboarderCreated = await models.Dashboarder.create(dashboarderJson, {transaction: t});
              await models.DashboarderShortCode.addDashboarderIdInShortCode(shortCode, dashboarderCreated.get().id, t);
              await models.DashboarderOrganization.addDashboarderIdInOrganizations(organizations, dashboarderCreated.get().id, t);
              return dashboarderCreated;
            })
            .catch(function (err) {
              log.error('createDashboarderInShortCodesOrganizations: rolling back', err);
              throw err;
            });
        },

        /**
         * Finds a dashboarder by dashboardername
         * returns the model of the  dashboarder
         *
         * @param {String} dashboarderName - Dashboardername of the dashboarder to find
         *
         * @returns {Promise} Promise dashboarder model
         */
        findByDashboardername: function findByDashboardername(dashboarderName) {
          return this.find({where: {dashboardername: dashboarderName}});
        },

        /**
         * Checks whether a dashboarder is able to perform an action on a resource
         * Equivalent to: select name from permissions p join shortCode_permissions g on p.id=g.permission_id where g.shortCode_id=(select shortCode_id from dashboarders where dashboardername='aliceab@example.com') AND p.resource='dashboarder' and p.create=true;
         * @param {String} dashboarderId  - The dashboarderId to search
         * @param {String} resource  -The resource name to search
         * @param {String} action  - The action , "create,read,update,delete"
         *
         * @returns {Boolean} True if the dashboarder can perform the action on the resource otherwise false
         */


        /**
         * Returns all permissions associated with a dashboarder
         *
         * @param {String} dashboardername - The dashboardername to search permissions for
         *
         * @returns {Promise} a Promise containing array of permission results
         */
        getPermissions: function (dashboardername) {
          return this.find({
            include: [
              {
                model: models.ShortCode,
                include: [
                  {
                    model: models.Permission
                  }]
              }],
            where: {
              dashboardername: dashboardername
            }
          });
        },
        getShortCodes: function (dashboardername) {
          return this.find({
            include: [
              {
                model: models.ShortCode,
                include: [
                  {
                    model: models.ShortCode
                  }]
              }],
            where: {
              dashboardername: dashboardername
            }
          });
        },
        getOrganizations: function (dashboardername) {
          return this.find({
            include: [
              {
                model: models.ShortCode,
                include: [
                  {
                    model: models.Organization
                  }]
              }],
            where: {
              dashboardername: dashboardername
            }
          });
        }
      },

      instanceMethods: {
        comparePassword: function (candidatePassword) {
          let me = this;
          return new Promise(function (resolve, reject) {
            let hashPassword = me.get('passwordHash') || '';

            bcrypt.compare(candidatePassword, hashPassword, function (err, isMatch) {
              if (err) {
                return reject(err);
              }
              resolve(isMatch);
            });
          });
        },

        /**
         * Removes password from the toJson
         *
         */
        toJSON: function () {
          let values = this.get({clone: true});
          delete values.password;
          delete values.passwordHash;
          return values;
        }
      }
    },
    {
      underscored: true
    });

  return Dashboarder;
};
