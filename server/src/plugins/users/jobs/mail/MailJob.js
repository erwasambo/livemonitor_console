import {Subscriber} from 'rabbitmq-pubsub';
import nodemailer from 'nodemailer';
import firebase from 'firebase';
import ejs from 'ejs';
import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import mg from 'nodemailer-mailgun-transport';
let log = require('logfilename')(__filename);

export default function MailJob (config){
  var emailTemplate = "";
  let subscriber = createSubscriber(config);
  // This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
  let auth = {
    auth: {
      api_key: 'key-28dcd1d22f7db9b41c8ddfce69befd33',
      domain: 'livemonitorconsole.com'
    }
  };

  let transporter = nodemailer.createTransport(mg(auth));
  log.debug("MailJob nodemailer createTransport options: ", JSON.stringify(auth));

  return {
    async start() {
      log.info('start');
      try {
        await subscriber.start(this._onIncomingMessage.bind(this));

        firebase.database().ref('emailTemplate').on("value", function (snapshot) {
          emailTemplate = snapshot.val();
        });

        log.info('started');
      } catch(error){
        log.error(`cannot start: ${error}, is RabbitMq running ?`);
      }
    },

    async stop() {
      log.info('stop');
      await subscriber.stop();
      log.info('stopped');
    },

    async getTemplate(type){
      let filename = path.join(path.dirname(__filename), 'templates', type + '.html');
      //log.debug("filename", filename);
      return new Promise((resolve, reject) => {
        fs.readFile(filename, "utf8", (error, data) => {
          if(error){
            reject(error);
          } else {
            resolve(data);
          }
        });
      });
    },

    async _sendShortCodeDownEmail(type, user) {
      log.info("sendEmail %s to user ", type, user);
      if(!user.shortCodeAdminEmail){
        log.error("shortCodeAdminEmail not set");
        throw {name:"shortCodeAdminEmail not set"};
      }

      if(!user.shortCodeName){
        log.error("shortCodeName not set");
        throw {name:"shortCodeName not set"};
      }

      if(!user.shortCodeOperator){
        log.error("shortCodeOperator not set");
        throw {name:"shortCodeOperator not set"};
      }

      if(!user.timeStamp){
        log.error("timeStamp not set");
        throw {name:"timeStamp not set"};
      }

      if(!transporter){
        log.error("mail config not set");
        throw {name:"mail config not set"};
      }

      let locals = {
        shortCodeName: user.shortCodeName,
        shortCodeOperator: user.shortCodeOperator,
        shortCodeAdminEmail: user.shortCodeAdminEmail,
        description: user.description,
        timeStamp: user.timeStamp,
        websiteUrl: config.websiteUrl,
        emailTemplate:emailTemplate,
        signature: config.mail.signature
      };

      let template = await this.getTemplate(type);
      let html = ejs.render(template, locals);
      let lines = html.split('\n');
      let subject = lines[0];
      let body = lines.slice(1).join('\n');

      let mailOptions = {
        from: config.mail.from,
        to: user.shortCodeAdminEmail,
        subject: subject,
        html: body
      };

      log.debug("_sendShortCodeDownEmail: ", _.omit(mailOptions, 'html'));

      return new Promise( (resolve, reject) => {
        transporter.sendMail(mailOptions, function(error, info){
          if(error){
            log.error("cannot send mail: ", error);
            reject(error);
          } else {
            delete info.html;
            log.debug("mail sent: ", info);
            resolve(info);
          }
        });
      });
    },

    async _sendEmail(type, user) {
      log.info("sendEmail %s to user ", type, user);
      if(!user.email){
        log.error("email not set");
        throw {name:"email not set"};
      }

      if(!user.code){
        log.error("token not set");
        throw {name:"token not set"};
      }

      if(!transporter){
        log.error("mail config not set");
        throw {name:"mail config not set"};
      }

      let locals = {
        code: user.code,
        websiteUrl: config.websiteUrl,
        signature: config.mail.signature
      };

      let template = await this.getTemplate(type);
      let html = ejs.render(template, locals);
      let lines = html.split('\n');
      let subject = lines[0];
      let body = lines.slice(1).join('\n');

      let mailOptions = {
        from: config.mail.from,
        to: user.email,
        subject: subject,
        html: body
      };

      log.debug("_sendEmail: ", _.omit(mailOptions, 'html'));

      return new Promise( (resolve, reject) => {
        transporter.sendMail(mailOptions, function(error, info){
          if(error){
            log.error("cannot send mail: ", error);
            reject(error);
          } else {
            delete info.html;
            log.debug("mail sent: ", info);
            resolve(info);
          }
        });
      });
    },

    async _onIncomingMessage(message) {
      log.info("onIncomingMessage content: ", message.content.toString());
      log.info("onIncomingMessage fields: ", JSON.stringify(message.fields));
      let user;

      if(!transporter){
        log.error("not configured");
        subscriber.ack(message);
        return;
      }

      try {
        user = JSON.parse(message.content.toString());
      } catch (error) {
        log.error("cannot convert message");
        subscriber.ack(message);
        return;
      }

      try {
        if(message.fields.routingKey==="user.shortcodedown") {
          await this._sendShortCodeDownEmail(message.fields.routingKey, user);
        }else{
          await this._sendEmail(message.fields.routingKey, user);
        }
        log.info("email sent");
        subscriber.ack(message);
      } catch (error) {
        log.error("error sending mail: ", error);
        // TODO nack or ack ?
        subscriber.ack(message);
        return;
      }
    }
  };
}

const subscriberOptions = {
  url:"amqp://livemonitorconsole.com",
  exchange: 'user',
  queueName: 'mail',
  routingKeys:['user.registering', 'user.resetpassword','user.shortcodedown']
};

function createSubscriber(config){
  log.info("createSubscriber: ", subscriberOptions);
  return new Subscriber(subscriberOptions);
}