import Router from 'koa-66';
import Qs from 'qs';

let log = require('logfilename')(__filename);

export function ShortCodeEmailScheduleHttpController(app, shortCodeEmailScheduleApi){
  log.debug("ShortCodeEmailScheduleHttpController");

  let respond = app.utils.http.respond;
  return {
    async createShortCodeEmailSchedule(context){
      return respond(context, shortCodeEmailScheduleApi, shortCodeEmailScheduleApi.createShortCodeEmailSchedule, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        shortCodeEmailScheduleApi,
        shortCodeEmailScheduleApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let shortCodeEmailScheduleId = context.params.id;
      return respond(context, shortCodeEmailScheduleApi, shortCodeEmailScheduleApi.getOne, [shortCodeEmailScheduleId]);
    }
  };
}

export default function ShortCodeEmailScheduleRouter(app, shortCodeEmailScheduleApi){
  let router = new Router();
  let shortCodeEmailScheduleHttpController = ShortCodeEmailScheduleHttpController(app, shortCodeEmailScheduleApi);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/
  router.get('/', shortCodeEmailScheduleHttpController.getAll);
  router.get('/:id', shortCodeEmailScheduleHttpController.getOne);

  app.server.baseRouter().mount("/shortCodeEmailSchedules", router);
  return router;
}