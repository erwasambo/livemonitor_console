import _ from 'lodash';
let log = require('logfilename')(__filename);

export default function ShortCodeEmailScheduleApi(app) {
  let models = app.data.models();
  let validateJson = app.utils.api.validateJson;

  return {
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.ShortCodeEmailSchedule.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let shortCodeEmailSchedules = _.map(result.rows, shortCodeEmailSchedule => shortCodeEmailSchedule.toJSON());
      return {
        count: result.count,
        shortCodeEmailSchedulesData: shortCodeEmailSchedules
      };
    },
    getOne: function (shortCodeEmailScheduleId) {
      log.debug("get shortCodeEmailScheduleId: ", shortCodeEmailScheduleId);
      return models.ShortCodeEmailSchedule.findByShortCodeEmailScheduleId(shortCodeEmailScheduleId);
    }
  };
}