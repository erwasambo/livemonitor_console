import Router from 'koa-66';
import Qs from 'qs';
import TriageLogApi from './TriageLogApi';

let log = require('logfilename')(__filename);

export function TriageLogHttpController(app, publisherUser){
  log.debug("TriageLogHttpController");
  let triageLogApi = TriageLogApi(app, publisherUser);
  let respond = app.utils.http.respond;
  return {
    async createTriageLog(context){
      return respond(context, triageLogApi, triageLogApi.createTriageLog, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        triageLogApi,
        triageLogApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let triageLogId = context.params.id;
      return respond(context, triageLogApi, triageLogApi.getOne, [triageLogId]);
    }
  };
}

export default function TriageLogRouter(app, publisherUser){
  let router = new Router();
  let triageLogHttpController = TriageLogHttpController(app, publisherUser);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', triageLogHttpController.createTriageLog);
  router.get('/', triageLogHttpController.getAll);
  router.get('/:id', triageLogHttpController.getOne);

  app.server.baseRouter().mount("/triageLogs", router);
  return router;
}