import _ from 'lodash';
import moment from 'moment';
let log = require('logfilename')(__filename);
import {Publisher} from 'rabbitmq-pubsub';

export default function TriageLogApi(app, publisherUser) {
  let models = app.data.models();
  let validateJson = app.utils.api.validateJson;

  return {
    createTriageLog: async function (shortCodeIn) {
      validateJson(shortCodeIn, require('./schema/createTriageLog.json'));
      //Check for duplicates
      let triageLog = await models.TriageLog.find({
        where: {sentMessageId: shortCodeIn.sentMessageId, triageStatus: shortCodeIn.triageStatus}
      });

      if (!triageLog) {
        let result = await models.TriageLog.create(shortCodeIn);
        log.debug("if (!triageLog) { createTriageLog: ", JSON.stringify(result));
        if (shortCodeIn.triageStatus == "INCOMING SMS: ERROR IN RECEIVING") {
          log.debug(" if (shortCodeIn.triageStatus shortCodeIn.triageStatus : ", shortCodeIn.triageStatus );

          let sentMessage =  await models.SentMessage.findBySentMessageId(shortCodeIn.sentMessageId);
          log.debug("let sentMessage = SentMessage.findBySentMessageId("+shortCodeIn.sentMessageId+")", JSON.stringify(sentMessage));
          if (sentMessage) {
            let shortCode = await models.ShortCode.find({where: {id: sentMessage.shortCode}});
            if (shortCode) {
              log.debug(" if (shortCode) { let shortCode = await models.ShortCode : ",  JSON.stringify(shortCode));

              //Timestamp is 2016-07-12 05:00:10
              let timestampArray = shortCodeIn.timeStamp.split(/(\s+)/);
              let date = timestampArray[0];

              log.debug(" timestampArray date: ",  JSON.stringify(date));

              //Now be in the look for Short Code Email Schedule
              let shortCodeEmailSchedules = await models.ShortCodeEmailSchedule.findAll({where: {shortCodeId: JSON.stringify(shortCode.id),date: date}});
              if(shortCodeEmailSchedules) {
                log.debug(" if(shortCodeEmailSchedules)  let shortCodeEmailSchedules = await models.ShortCodeEmailSchedule : ", JSON.stringify(shortCodeEmailSchedules));
                if(shortCodeEmailSchedules.length>0){
                  log.debug("shortCodeEmailSchedules.length>0");
                  for (let shortCodeEmailSchedule of shortCodeEmailSchedules) {
                    let SHORT_CODE_IN_TIMESTAMP_REFERENCE = moment(shortCodeIn.timeStamp).unix();   //REFERENCE IS 1636158779454
                    let ADD_EMAIL_SCHEDULE_REFERENCE = moment(shortCodeEmailSchedule.timeStamp).add(3, 'hours');
                    let NEXT_THREE_HOURS = ADD_EMAIL_SCHEDULE_REFERENCE.unix();// NEXT_THREE_HOURS is 1846110800000
                    log.debug("shortCodeIn.timeStamp" + shortCodeIn.timeStamp +"SHORT_CODE_IN_TIMESTAMP_REFERENCE",SHORT_CODE_IN_TIMESTAMP_REFERENCE);
                    log.debug("shortCodeEmailSchedule.timeStamp" + shortCodeEmailSchedule.timeStamp +"NEXT_THREE_HOURS",NEXT_THREE_HOURS);
                    //Check if time has elapsed by more than 3hours
                    if(NEXT_THREE_HOURS<=SHORT_CODE_IN_TIMESTAMP_REFERENCE){
                      log.debug("NEXT_THREE_HOURS<=SHORT_CODE_IN_TIMESTAMP_REFERENCE yes! its more than three hours please send this");
                      //send email
                        //publisherUser.start();
                        //find users who have been allocated this shortcode
                      let userIds = await models.UserShortCode.findAll({where: {shortCode_id: shortCode.id}});
                      if(userIds) {
                        log.debug("let userIds = await models.UserShortCode : ", JSON.stringify(userIds));
                        if(this.isIterable(userIds)){
                          for (let userId of userIds) {
                            this.publishSendEmails(shortCode, userId.user_id, shortCodeIn.timeStamp);
                          }
                        }else{
                          //only one userId has been found
                          this.publishSendEmails(shortCode, userIds.user_id,shortCodeIn.timeStamp);
                        }
                      }

                      //record in email schedule table
                      await models.ShortCodeEmailSchedule.createShortCodeEmailSchedule({shortCodeId: shortCode.id,date: date, timeStamp: shortCodeIn.timeStamp});
                      //Just send one email
                      break;
                    }else{
                      //dont send email
                      log.debug("Email not sent because it is not yet 3hours");
                    }

                  }
                }else{
                  log.debug("shortCodeEmailSchedules.length==0 there is no one");
                  //only one userId has been found
                  //publisherUser.start();
                  //find users who have been allocated this shortcode
                  let userIds = await models.UserShortCode.findAll({where: {shortCode_id: shortCode.id}});
                  if(userIds) {
                    log.debug("let userIds = await models.UserShortCode : ", JSON.stringify(userIds));
                    if(this.isIterable(userIds)){
                      for (let userId of userIds) {
                        this.publishSendEmails(shortCode, userId.user_id, shortCodeIn.timeStamp);
                      }
                    }else{
                      //only one userId has been found
                      this.publishSendEmails(shortCode, userIds.user_id,shortCodeIn.timeStamp);
                    }
                  }

                  //record in email schedule table
                  await models.ShortCodeEmailSchedule.createShortCodeEmailSchedule({shortCodeId: shortCode.id,date: date, timeStamp: shortCodeIn.timeStamp});
                }
              }
            }
          }
        }
      } else {
        log.debug("duplicate not recorded: ", shortCodeIn);
      }
      //return savedShortCode.toJSON();
      return {
        success: true,
        message: "success in triage log creation"
      };
    },
    publishSendEmails: async function (shortCode, userId, timeStamp) {
      let users = await models.User.findAll({where: {id: userId}});
      //Now get all users who have been allocated this shortcode and send
      //each individual an email
      if (users) {
        log.debug("let users = await models.User. : ", JSON.stringify(users));
        if (this.isIterable) {
          publisherUser.start()
            .then(function() {
              for (let user of users) {
                //send shortcode down email
                let userPendingOut = {
                  shortCodeName: shortCode.name,
                  shortCodeOperator: shortCode.operator,
                  emailTemplate: "Hello World",
                  shortCodeAdminEmail: user.email,
                  description: shortCode.description,
                  timeStamp: timeStamp
                };
                publisherUser.publish("user.shortcodedown", JSON.stringify(userPendingOut));
                log.debug("publisherUser.publish(user.shortcodedown", JSON.stringify(userPendingOut));
              }
            })
        }else{
          //only one user found
          //send shortcode down email
          let userPendingOut = {
            shortCodeName: shortCode.name,
            shortCodeOperator: shortCode.operator,
            emailTemplate: "Hello World",
            shortCodeAdminEmail: users.email,
            description: shortCode.description,
            timeStamp: timeStamp
          };

          publisherUser.start()
            .then(function() {
              publisherUser.publish("user.shortcodedown", JSON.stringify(userPendingOut));
              log.debug("publisherUser.publish(user.shortcodedown", JSON.stringify(userPendingOut));
            })
        }
      }
    },
    isIterable: async function (obj) {
      // checks for null and undefined
      if (obj == null) {
        return false;
      }
      return typeof obj[Symbol.iterator] === 'function';
    },
    getShortCodes: async function (userId) {
      log.debug("get userId: ", userId);
      return await models.User.getShortCodes(userId);
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.TriageLog.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let triageLogs = _.map(result.rows, triageLog => triageLog.toJSON());
      return {
        count: result.count,
        triageLogData: triageLogs
      };
    },
    getOne: function (triageLogId) {
      log.debug("get triageLogId: ", triageLogId);
      return models.TriageLog.findByTriageLogId(triageLogId);
    }
  };
}