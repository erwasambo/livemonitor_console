import _ from 'lodash';
let log = require('logfilename')(__filename);

export default function SentMessageApi(app) {
  let models = app.data.models();
  let validateJson = app.utils.api.validateJson;

  return {
    createSentMessage: async function (shortCodeIn) {
      validateJson(shortCodeIn, require('./schema/createSentMessage.json'));
      log.debug("createSentMessage: ", shortCodeIn);

      //Look out for duplicates
      let res = await models.SentMessage.find({
        where: {sentMessageId: shortCodeIn.sentMessageId, timeStamp: shortCodeIn.timeStamp}
      });

      if (!res) {
        let result = await models.SentMessage.create(shortCodeIn);
      }else {
        log.debug("duplicate not recorded: ", shortCodeIn);
      }
      //return savedShortCode.toJSON();
      return {
        success: true,
        message: "success in sent message creation"
      };
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.SentMessage.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let sentMessages = _.map(result.rows, sentMessage => sentMessage.toJSON());
      return {
        count: result.count,
        sentMessagesData: sentMessages
      };
    },
    getOne: function (sentMessageId) {
      log.debug("get sentMessageId: ", sentMessageId);
      return models.SentMessage.findBySentMessageId(sentMessageId);
    }
  };
}