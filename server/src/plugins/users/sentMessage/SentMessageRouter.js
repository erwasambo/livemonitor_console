import Router from 'koa-66';
import Qs from 'qs';

let log = require('logfilename')(__filename);

export function SentMessageHttpController(app, sentMessageApi){
  log.debug("SentMessageHttpController");

  let respond = app.utils.http.respond;
  return {
    async createSentMessage(context){
      return respond(context, sentMessageApi, sentMessageApi.createSentMessage, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        sentMessageApi,
        sentMessageApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let sentMessageId = context.params.id;
      return respond(context, sentMessageApi, sentMessageApi.getOne, [sentMessageId]);
    }
  };
}

export default function SentMessageRouter(app, sentMessageApi){
  let router = new Router();
  let sentMessageHttpController = SentMessageHttpController(app, sentMessageApi);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', sentMessageHttpController.createSentMessage);
  router.get('/', sentMessageHttpController.getAll);
  router.get('/:id', sentMessageHttpController.getOne);

  app.server.baseRouter().mount("/sentMessages", router);
  return router;
}