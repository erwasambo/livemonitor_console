import Router from 'koa-66';
import Qs from 'qs';

let log = require('logfilename')(__filename);

export function DashboarderHttpController(app, dashboarderApi){
  log.debug("DashboarderHttpController");

  let respond = app.utils.http.respond;
  return {
    async getAll(context) {
      return respond(
        context,
        dashboarderApi,
        dashboarderApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let dashboarderId = context.params.id;
      return respond(context, dashboarderApi, dashboarderApi.getOne, [dashboarderId]);
    }
  };
}

export default function DashboarderRouter(app, dashboarderApi){
  let router = new Router();
  let dashboarderHttpController = DashboarderHttpController(app, dashboarderApi);

  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);

  router.get('/', dashboarderHttpController.getAll);
  router.get('/:id', dashboarderHttpController.getOne);

  app.server.baseRouter().mount("/dashboarders", router);
  return router;
}
