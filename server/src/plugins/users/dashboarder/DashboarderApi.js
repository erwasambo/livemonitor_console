import _ from 'lodash';
let log = require('logfilename')(__filename);

export default function DashboarderApi(app) {
  let models = app.data.models();

  return {
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.Dashboarder.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let dashboarders = _.map(result.rows, dashboarder => dashboarder.toJSON());
      return {
        count: result.count,
        dashboarderData: dashboarders
      };
    },
    getOne: function (dashboarderId) {
      log.debug("get dashboarderId: ", dashboarderId);
      return models.Dashboarder.findByDashboarderId(dashboarderId);
    }
  };
}
