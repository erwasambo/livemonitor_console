import Router from 'koa-66';
import Qs from 'qs';
import OrganizationApi from './OrganizationApi';

let log = require('logfilename')(__filename);

export function OrganizationHttpController(app, publisherOrganization){
  log.debug("OrganizationHttpController");

  let organizationApi = OrganizationApi(app, publisherOrganization);
  let respond = app.utils.http.respond;
  return {
    async addOrganization(context){
      return respond(context, organizationApi, organizationApi.createOrganization, [context.request.body]);
    },
    async getAll(context) {
      return respond(
        context,
        organizationApi,
        organizationApi.getAll,
        [Qs.parse(context.request.querystring)]);
    },
    async getOne(context) {
      let organizationId = context.params.id;
      return respond(context, organizationApi, organizationApi.getOne, [organizationId]);
    }
  };
}

export default function OrganizationRouter(app, publisherOrganization){
  let router = new Router();
  let organizationHttpController = OrganizationHttpController(app, publisherOrganization);

/*  router.use(app.server.auth.isAuthenticated);
  router.use(app.server.auth.isAuthorized);*/

  router.post('/', organizationHttpController.addOrganization);
  router.get('/', organizationHttpController.getAll);
  router.get('/:id', organizationHttpController.getOne);

  app.server.baseRouter().mount("/organizations", router);
  return router;
}