import _ from 'lodash';
import Log from 'logfilename';

export default function OrganizationApi(app, publisherOperator) {
  let models = app.data.models();
  let log = new Log(__filename);
  let validateJson = app.utils.api.validateJson;

  return {
    async createOrganization(organizationIn) {
      validateJson(organizationIn, require('./schema/createOrganization.json'));
      log.debug("createOrganization: ", organizationIn);
      let organization = await models.Organization.findByName(organizationIn.name);
      if (!organization) {
        let organizationOut = {name: organizationIn.name};
        log.info("createOrganization name ", organizationOut.name);
        let savedOrganization = await models.Organization.create(organizationOut);
        return savedOrganization.toJSON();
      } else {
        log.info("already created", organizationIn.name);
      }
      return {
        success: true,
        message: "success in organization creation"
      };
    },
    getAll: async function (filter = {}) {
      _.defaults(filter, {
        order: 'DESC',
        limit: 10,
        offset: 0
      });
      log.info('getAll ', filter);
      let result = await models.Organization.findAndCountAll({
        limit: Number(filter.limit),
        offset: Number(filter.offset)
      });
      log.info(`getAll count: ${result.count}`);
      let organizations = _.map(result.rows, organization => organization.toJSON());
      return {
        count: result.count,
        organizationData: organizations
      };
    },
    getOne: function (organizationId) {
      log.debug("get organizationId: ", organizationId);
      return models.Organization.findByOrganizationId(organizationId);
    }
  };
}
